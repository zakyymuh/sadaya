$( document ).ready(function() {
 	
 	//toggle menu
 	$('.btn-toggle').on('click', function(){	 
 	  if($('.nav-menu').is(":visible")){
 	    $('.nav-menu').slideUp();
 	    $(this).removeClass('active');
 	  }else{
 	    $('.nav-menu').slideDown(); 
 	    $(this).addClass('active');
 	  } 
 	});

 	$('.back-top').click(function() {
 	    $("html, body").animate({scrollTop: 0}, 500);
 	}); 
 	if($(this).scrollTop() > 700){  //inittial
 		$('.back-top').show();
 	}else{ 
 		$('.back-top').hide();
 	} 
 	$(window).scroll(function() { 
 	    if($(this).scrollTop() > 700){
 	    	$('.back-top').fadeIn(200);
 	    }else{ 
 	    	$('.back-top').fadeOut(200);
 	    }
 	 });


 	var height_menu = 0;
	if(($(window).width() > 1160) && !$('.nav').hasClass('fixed')) { 
		height_menu = 70;
 		
 		//initila stiky menu function
		var lastScrollTop = 0;
		$(window).scroll(function(event){
	     	if ($(window).scrollTop() > 240){
			   $('.nav-top').addClass( "fixed"); 
			}
			if ($(window).scrollTop() > 240){
			   $('.nav-top').addClass( "fixed"); 
			}
			else {
			   $('.nav-top').removeClass("fixed");  
			} 


		}); 
	} 

	//smooth scrool
    $(".nav-menu li a:not(.btn-href), .btn-link").click(function(e) {
        e.preventDefault();
        $("html, body").animate({ 
        	scrollTop: $($(this).attr("href")).offset().top - height_menu 
        }, 500);
    }); 

});