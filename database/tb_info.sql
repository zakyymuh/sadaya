-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 28 Sep 2018 pada 15.45
-- Versi server: 10.1.35-MariaDB
-- Versi PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sadaya_baru`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_info`
--

CREATE TABLE `tb_info` (
  `id_info` int(11) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `tanggal_update` datetime NOT NULL,
  `tag` varchar(30) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_info`
--

INSERT INTO `tb_info` (`id_info`, `judul`, `foto`, `tanggal_update`, `tag`, `deleted`) VALUES
(1, '123456', '<p>You did not select a file to upload.</p>', '2018-09-27 22:28:22', '1123', 1),
(2, '23333', '<p>You did not select a file to upload.</p>', '2018-09-27 22:28:55', '333', 1),
(3, '12311232132AAAA', '908c9a564a86426585b29f5335b619bc3.jpg', '2018-09-27 22:30:01', '132', 1),
(4, '123123', '820a8f5c40c91fbd63f19519314ca2772.PNG', '2018-09-27 22:53:18', '1231232', 0),
(5, 'Ini adalah judul nyaa owowowowo', 'a8240cb8235e9c493a0c30607586166c3.png', '2018-09-28 20:11:57', '123123', 0),
(6, 'Ini adalah judul nyaa owowowowo', '3210ddbeaa16948a702b6049b8d9a2023.png', '2018-09-28 20:12:26', '123123', 0),
(7, 'Ini adalah judul nyaa owowowowo', 'e6d8545daa42d5ced125a4bf747b36884.png', '2018-09-28 20:12:47', '123123', 0),
(8, 'Selamat malam semua', '674bfc5f6b72706fb769f5e93667bd234.PNG', '2018-09-28 20:13:43', '#ucapanmalam', 0),
(9, 'Selamat malam semua', '3bbfdde8842a5c44a0323518eec97cbe4.PNG', '2018-09-28 20:14:54', '#ucapanmalam', 0),
(10, 'Selamat malam semua', '7b7a53e239400a13bd6be6c91c4f6c4e3.PNG', '2018-09-28 20:20:20', '#ucapanmalam', 0);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_info`
--
ALTER TABLE `tb_info`
  ADD PRIMARY KEY (`id_info`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_info`
--
ALTER TABLE `tb_info`
  MODIFY `id_info` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
