-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 28 Sep 2018 pada 15.44
-- Versi server: 10.1.35-MariaDB
-- Versi PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sadaya_baru`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id_admin` varchar(3) NOT NULL,
  `nama` varchar(15) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(204) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_admin`
--

INSERT INTO `tb_admin` (`id_admin`, `nama`, `username`, `password`) VALUES
('AD1', 'Admin KUY', 'mosimosi', 'a9355c23d71381b7caabeaa60ee26d7d79efb2ac749bfc26736e465f0f4529f8604d57ed3bf6fd2db67a565371c73d7acb9b0d182d5382e7fb45b1ceae2fff28e5YTeP3m2u1qHkTIfPsMN4u7MJvDSSIalT0cTUgUp+w='),
('AD2', 'Admin Meong', 'mosimosi2', 'eb398fbb66a41c1540d94537ce3a02da637066b27f1db33b51484d984a10d135134a18e610403cdb5e579deeb6817e95c178fc09f2742172c9b197877ab4e128t79wQnp+CWmZWZ6qWCgAE0o1MiqzotxcvOz59mQEjwo=');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_anggota`
--

CREATE TABLE `tb_anggota` (
  `NTA` char(10) NOT NULL,
  `password` varchar(172) NOT NULL,
  `NIM` char(8) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `jenis_kelamin` enum('L','P') DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `agama` varchar(12) DEFAULT NULL,
  `tempat_lahir` varchar(30) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `kontak` varchar(15) DEFAULT NULL,
  `kontak_lain` varchar(15) DEFAULT NULL,
  `alamat` varchar(130) DEFAULT NULL,
  `sosial_media` varchar(25) DEFAULT NULL,
  `jurusan` varchar(25) DEFAULT NULL,
  `foto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_anggota`
--

INSERT INTO `tb_anggota` (`NTA`, `password`, `NIM`, `nama`, `jenis_kelamin`, `email`, `agama`, `tempat_lahir`, `tanggal_lahir`, `kontak`, `kontak_lain`, `alamat`, `sosial_media`, `jurusan`, `foto`) VALUES
('252.10.001', 'a87c9aac3b03e4ef7e9eebd4706e6a08d8f52bc66c2ed1bbd034d16810a846a16a64549bce5348418cc5c990a24db7fa06239475fc0b28936da722352b70989cK2cf0NoTC9hL5O7BJdZZ6X5kFhf/Re7A7eogkraMz2s=', '-', 'Ade Gura Permana', '', '-', '-', '-', '0000-00-00', '-', '-', '-', '-', '-', '-'),
('252.10.002', '5dce6f80a743b4f23dfdb6845fa15f7aa2c3f106094489220685dd1aa4df2d0d1aa40fad83078b0adf84f4c18278ef60424e65aca704876a00d5de46386e2dffEsdhMTiFXHXsGd4J9jc43Euy6DNyw97/pJuiz4RsUMM=', '', 'Aditya Mokhamad', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.003', '8bb1b4b1a396ca3818430b7b1ae4cc381fccba802ae260445a302f3c89ebd8d15c1a3bf9640e720e46256a2a5d7ef0844175dd3fd583acdfd56d4dabd02e6670dlxSCDW6htZ06ygQKNiz1KRlJqzNBaD3vbdJie1xP4Y=', '', 'Adrian Vivaldi', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.004', 'f567ff7ae9aefb52f88bea6a3fd463ab0c7d03f607a661d0b5c29a2c89a6307948b50b2e62569763e2c3507f9a2bff92a21c324431f861818f6ed341b588dab1m62se3FI6b2eMfN55vAX0M9A0heRvGliyVVb0QtvZuQ=', '', 'Adzwin Aznan', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.005', '67eed3079bf0f0fe437b91f6cc91fc352d640e0a083e3145cf3681be62fd46a1d5ef034577815eb4b2483f59ac2f502b62afd8c68abd861395cc673c79ad241dA65QlHMwMRqmU3KfvGfai31+2zJMdWMqEFc2Xwc/1+o=', '', 'Afrida Ajeng', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.006', '5291cd15155d7cf46f41a0d2f4374f2af363c34c15dad14af7cec3a4304fdaaacef0e251465fe88e9db996d0b4156b1ed5897c86f2e26b4d044b50dabfe0c9c9vpd9v6Pd/T3izdtuWcRl9Ze++hbBGfDdWeByuHVW95M=', '', 'Ahmad Fauzi', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.007', '78a8930beb1907329f3b5d671016006b11594ce4381784df3786637047e7a91990d5d6a81ebd556dec432f239daf7a02536efd6736ebbbe5c3b2f1f0e50d830audtzAADv9uJrEwPiZJrDEM/UWkrK//AML8MkCnGO1tM=', '', 'Ananty H', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.008', '74cc365cc0685e6466292cf9ae2ef7ce6454e50379bc784cfd153570e40508f785344d9db32f68ebf26359f0e830ea1cb17a8c68c1a00fbc3440274ff2b09527xXpM44Tx+UjlnVZHcwur8F2v3qI/l+7gfPv9atIrWHE=', '', 'Angga Akbar', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.009', '9704440bf70c6ce6ba1fe305453753d8d3bfc9bd5b1aef7b62c542f1680fc7fd0922654c364d4cfafdad946824ec3d445376446dfc2829f8a906f821b23f8bc960jQBcNOKXthIUfxEBPfCbzG7QL5YqNe/re2SVEb8gE=', '', 'Angga Febrian H', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.010', 'd92228118d79346cd6fb8789b9919e9a07414408939e043d08006d400bc23978f520dbb706e85ad4d889aa1a97b7379a67c897cc4d41f4e38deb67d2376fe235wUtMfpkxO9EvXUz8Xv7V4MYPmrqDDoOO+hnyQG40uoA=', '', 'Anisa Auliyana', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.011', '2f39cf8e429b0cdba8cdde04a319aa924e735f1d0590e42adb286ed8b71091630dbe6def69ba25018f31ecdc5fa37087754e570d5131ed02bbb1d59f2c04d5ae5xngzfbR56rqijGYnIk1Xz32TkD/O3HrFzWot5hyqDs=', '', 'Anita Rachmawati', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.012', 'd2c51dd3d98d5a1f897fb10eade08f8f8372b80a8f90c4a113a979b079e111a62a048f0e0f6af23ba804a918b4dc110a31a9a00cab1116896f173378750bcf01v+t8eYvL8zZNXSVkdmJvyPTkkHXqqC1mW6hmTzA/H+A=', '', 'Aprilia Dwi Purwanti', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.013', '59ed06134652b0e3bd23578c4df0e76da97d0e63a9df56fe0af8962f5b3d58d276009f99aa8ff8365d824d25b1f45aef83f2e4248154eb27b40dace8eb3680e5qHcbFsP1zxXx0HyR8TchmJ+LQc+nPmMjyA4q9Cr7QDk=', '', 'Aprilia Putri Nadhilah', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.014', '2b431f84b62c8f95bdd48ddfafc7be889b9a51ffffc2a49cefa8446f2e868cf31136a82061d7c62b0beb0ad5078ab63a2a1ec1bf10e29b09396ff82f8397586a0+g4OztFNLrI0H6h7a0gFOs6rtF84eG5xprg8bqQwRc=', '', 'Arief Septian Wijayanto', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.015', '53b9c670c5f0efb43f18ddf5848a51f4909416c7afdc72af38203f472e84792187199b1925ab67e9a699535e41c10607486775c56f7ef47574a96a4b65ae8bb6jwQ9hl8nOcXU7sr3jpUhpMwEFkm0Db7zRGApU1TVZ6c=', '', 'Ayu Novitasyari', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.016', 'ca489c062388742f626e43ec44294be17b1cdd066fce9822491cef0decffa8c9c885114cf00e79e3b966cbd314637e374353075abb297d35fd14cabf0d317eb0O+gDKIaq8imlPdLBR9TFGFTAHwEgmP5VISf7aE6iyjQ=', '', 'Bagas Harfianto', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.017', '016fc60bfc2b371726275ce8ec18ccb4e978b397d3f5d0ffd3e95222f2bffc9ad0e829c18326db4f0aabd2e0348b50a61c1d88f90d4facbfcda555487acbc3bayG8PaBQ90HJ/4XUzpw0MTkZZhBnYOmGFZik2idHhseU=', '', 'Bella Damayanti', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.018', 'd69ad31a69a7efd01197cac652aa6aab539d3a9508c6e457a6b018356d48a02977f7f20a45bc9e65414798353bd2842aaad0def91be7cdbeb47407a018087fb1UrzCLfqi9GAORNxlhYmF+Pfvl2I1l85UPWN4e6p8H5k=', '', 'Bella Monica', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.019', 'ce71c10d30d982bdeaf0824abe8d5b2cfe79412ecc15177ef903ba91f26e2b1cc22ab503522f98ee56d936869e9a1f1f21e369e9b178181b0af2985fbc3d20deQTlRlTHAE6q1yqAXhST+NYWCBIWh2qN5KATnuOX4qG0=', '', 'Bunga Triamanda Putri', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.020', '16e15fc02843e7e73297f61577f1de1ee46d4d31389dac6e572b77b5b5a11b2b6a48b33d0aacf96cc00c3f53899b04bd9964286dfaed9104925bc15e1a37279aSeZ7RVuZaeEQKrSums3zjhGlwRyvlcF3aQluPS5i8zU=', '', 'Candra Sobar A', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.021', 'c91d5a382475b1ad1753f1f8057cc487bffe346030f76aeeaf0be490c297e1297ec07f58b41c16fc6d6161861f1a540eec17424b5b86cfb124ab57405acfdf4fZph4wzoHtPCUZCfo5QQKpJoIXejRsm6KW0sR1m8ALDw=', '', 'Cecep Arief Habibudin', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.022', 'b19e32d3475270e5d1b9c091f1f271911dc6155adb9d3998622de4495608ccf47cc4a9880b2b86dda50bdbdd0475e0b1f1ec7c3b6b82fe9e0b65dec7a1a191f7tbp4pIhrVJ2qBjVDUq+IERq0jefAcaB6hgUO8je0wCo=', '', 'Charisa Cahya Dewi', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.023', 'dbd9480a1a3011bb0be4bd247ca75359dedbb29835d32ff2615fed8d21fe700b0cb3532c4a891a4fa0426126e376268f4899dbaa4a19fd87cfe0718cb8d5f7fd5yjZ1oUOiglQeQ05GeZEgmpEEde8hsylmu955j5BBDw=', '', 'Chintya Dewi P', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.024', '9980359d7b6709275cfc6a06dd3b2449cb6257c754d116ec96bc49b16b941df04b3216bde6aada35ae18c0fd78d6824c44a1854ccc7ce89bcc02757ea32c07c4b8+BO1cvdGg3LBlgxdj+/yEsClnpUeQk3nhq7sm8FCY=', '', 'Cindy Desinta Ramadhani', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.025', '2abfea9bab8d4a7db1dba26277fa1676efc82844fc04104dacb61382c92a8854bbe8aa6990e6a2bc10940d4fb1c76bb385dd539f08e16a878f196bcc993e38a5xGTgvArKQBDoUA0DslrwSVuM7aXZLph6NIo2f6900ag=', '', 'Cintami Amalia', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.026', '06ba3b6ecbe4ff5486844999ef4698ddbe1e8689af37334c201aef9bdf357640393cc2f6f7ac13773189cd964b5f044ba9d0f36caee11a500d4fee71cedf6d96hKTj1bn12WwYv0iXP7kR9rjgV4yphEhgOYsguLjuCrU=', '', 'Cinthia Merita Ap', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.027', 'cc7e5b00c11f44f0089ad1ebeeedf80576f2399c6631e0806d6003dbe9b96053c69952f201a53b68a3c6279ee2bcb8a3895f92927dcd8a498aa5824b0dbcb38dd+9VXcg598sIgdUX18TrSOL8YSXnVV2XekC7RpY/wJA=', '', 'Dalih Rusmana', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.028', '281984ce85684987f957df1591702e5ab37d936f9c2ff953d21f54501b553ba4053163298135c240f3e990e2d4613d0fcf369a0c84f78fc89beaae52e2f24fbeAMCKhP92Di9KB8H8H8PxLftRc1LAUGjMcmOfUxJMsGM=', '', 'Danil Ibrahim', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.029', 'fab3c5b3496fe7f610418184a2d60b0b085b25995c295f6dd0c6af86a1c49a298a751846cd7bda100fd8e3d4d17cfa9b472fde461b95ebbf747508567ea59158yB8mk8cMCFWxICexqYBQPK3qRSNxZlNnphKPa6g+raQ=', '', 'Dede Yayat', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.030', '7e0ade9857ad8175f48bc0e8abea2ce6a9e34d45eb151ff3cd4c31a5080866a0ea1324c1636b33c71aae4214cb04b794c1742bd5a77e5c34017071c9edf6db24m3dY2tMW50SdL3T+NkZHhZedNwGlqUQgAwoZu5LPq/4=', '', 'Defriansyah', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.031', '76ce722bc2d21450fa87d6736aedbc719bf60b1a2d5dce0eaab4b91d433ff658bbc99266705aa513d1b21739dc3d35c5ba66480267352a4cce442232e62ca057oDkFO+oJDF0nN0qzsjEIbcLTk26hQK1gXh+T6JFiMZY=', '', 'Dennis Ardiansyah Saputra', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.032', 'a254b5b69e94077c813dc9808286cda13e43eb73e84ef5d4f284bc39ff98a2fb26e53a34e23eea8cf9ac17e84aa1fe6a5f9e51b7f03b0c93e6a530035950482ek6qP+hM8m6J3+iAUr9m0QJxKUVA/8hWRY2fkDHdhI3M=', '', 'Depi Anggraeni', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.033', '05806dd63174e1ebb619c461fef33ec6e3b1582dc69f446ee00615ab814d6cedd5c356aff997ff2e5a1b715ae2657f00c7619ed7e5db2d3edb7206d96be5511fwz/36+EGGj/ucQ6pejJZ2x+eLiAfOOY7uHXy2lLl/K8=', '', 'Desy Novita Sari', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.034', '9b6b5f4685e6d4c529cdc8187ac40beea5019b97dc36f238289ba2c1e054a439629ac88938802cea22032d2ffce40fea56b21beae3cab8595668ff8dc703a26fdNYqBnno3m9lxT0UBb+ZsjhnyLakLQWw4+T9Vp9opIA=', '', 'Deva Ayuni Akbari', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.035', 'c4f7b3c8c7b3c03af8b59d1a63656b8b395ac5bd79b2dc58e3fef132e9460a7b3499e62f27ac28dce79ca52230e171d6ddcf43b04fff6199e550dade2764f725O9RiO2ZYgfTQxMmPo059o36ZQtIKlc2ZaR93ZwW3lww=', '', 'Deva Hervayani', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.036', '500f7a6319906c5e6b4607613bf0555ffc1c090a89aa9efbfb5339f5438487fedb1f2c0307cc2db3335f3055bcc32bf4aa3ece70204f2b636f12c977924cedf7H4WTXhoMEqQ1vGhTC5KkmR+GMJB8cS8p5Tn38vPclYM=', '', 'Devi Herpiyani', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.037', '1de32e08e8ddba2a9804a2e7a4a62aecf797bc8ab8d16bc53147995ce4c927783fba6633b117a072597ec5f76193f3af24b27604842cb36861c10fd5eb5b3215d+nowRMnNNfyapfxRg8suLNVbDdrCOODCKqSH6KtBJU=', '', 'Devia Aprilliani', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.038', '60742c78b12af177e778fd79991a6c4fcf0c4d88dd8c8131685c8ba68d581b883b844a91d774a68ae47fc85dd2c0cacaab98d7ff530f3d01087b082705f7f23473Rr9VxZ8ruGWNDuUz13gZwGCK1fmb2YrVOhZ5RQmuE=', '', 'Devy Putri Raman Dhani', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.039', '577f073d1bab262315f24c7f0ae2868b820380555af4d37b357c9acb7c07335cd37b148c8b0f7e34132e4fe983bc2485b36e188cf722db8fdcf57296c7d07a77BgRLytn73JN325+t8Ky8KumwMJH4JaQ1YKxwm8SO4Ls=', '', 'Dewi Sri Juniarti', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.040', 'ab54287b08ffd24d9c1279753716225e47dc3375170fb74c986465cdb4735dda14a478045845db89fdf9b11b52027fad43ab8401f4673282bb34b35b7aaedd82+ZbWlFFK3Kof61dXV4xQap9YQ3bU9NxctllI0fUWevQ=', '', 'Diana Putri Irawati', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.041', '382e6e8082a593ddac86880ff14110cd183345197a1b0d685f5c2994599608f619b05c675db05fcfa00096b491a5b164890244979566fb4286b94a6f0bad7524mwHzUPISwVWUmdBadrSbaFLKSI4R0IehRsArcMyDQeI=', '', 'Dikdik Subagja', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.042', '4a6adf1c35aad62ddf70cdda30afdd10486344e124e154337891537e35258659e4ea67a099ecef54e1b88cb0a0d7d16469d67c4777949aa933dba7abbc3cbf92wRpVMmEVr31z8yeF063wX2dDB9vd6khr3YK0IgkyYmY=', '', 'Diki Supriadi', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.043', 'c49303724a9dfad5c73d00bfe423e243faeecbc47251c162ea7b0c84f8f9896b7e6691e675307fcd6e878635514bd700563400ea435bfab2cc46b0229ba4e927jcMEPjt/roQZs4FdeHnkSayyWOqZd2KZTQEq1Wd3xRc=', '', 'Dimas Khoirudin Ahmad', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.044', '7c0f1eca05b8eee6104f132a907f52b79064987c7615a7ae2ac6d64907074feb87b70ade9e9692646039cdb2dc924c7a0a44f4ba6ab79597d841c747ff92ca20dfL4NgZTFRMQ6MzkCP/hAtQm2TWPSP7e1B+PKJnPwTQ=', '', 'Dimas Mohammad Makdus', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.045', '6888bfaf5b594f17724033ffe6746ce2db7bb6614c2ce41b3fca0acb9b16d280716c30036512880b30443b3e5a112fae6ff31323488d618b9456ab323e8ebf0bh9+aJv50EgJw/CjbBQg+jRNaMQWDcfN7MNZ3PNx/Oi8=', '', 'Dina Maysarah', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.046', '9ebd2542abb524d7d208a27d387fdb4561d6d3f1574fe43792399865d90ec7bd5f2742880e23e48026662f2e281f99f686a21d5227095a9ee19474435f51126cFlAm+n2MS9CdKyPsTefbQfo3Bxa+VIZowjBgqMvgt+0=', '', 'Dinda Yunita Franshela Rosandy', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.047', 'c758d92fccb7dbcbd2968eda743086b89305cbaffa7f366f752368eb62832c7e0fda50090c6e287c76392f6f7e9286defdb9b343e34595ce2c9ed14514fc3e186fgjruhKR0F5Mgd2KTaA5GRb2ja3vnuszWZKtVGJZ60=', '', 'Dinna Fauziyyah', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.048', '14387fe754bceda4fba539e092b19b87554fe8a8da4a865035ddfaacaf5101c5ac1b528735ce446544bfcb70fefad844e63b05d80395443ad859d8da2af7bca2i9VROXgWRDImN6x7+A3i5tuQYn1vOVd5QUBw0jbghyI=', '', 'Dinna Febriyani', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.049', 'd05b97e3b4ccda29ee0dc858591d95dd71ad8c5a9b55f1475f57399c3eb64c582dfc133d0e6399a2ececbb568d5d1350c50ceac35d5c6815dbd79f3e7620b1abIQ02QsIxUwZdJksx26robAl62eo4mtJ064SX5Rm5eV4=', '', 'Dinna Shaffitri', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.050', '8a8609eeed2ee5aeb9174b9c18e53434266367d256d8edfa5359c0c017d816584e8319001398fe8acdd38f587b41fb171aa5b37135f11460c8c5b97600d55540jB6GGfiZJy/jkcH5oRJebuRVDYNdQV2H9uhDR7P74BU=', '', 'Drina Sukmaning Rahayu', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.051', '5e3b69c9d00c702eeba221bd9218691189f191615c1fb80c96a7663816191eb1bc9408e619f09264de67389b99cb274b6a3c3a441deb92bc85cde74015231c8bebuDHzDAAUGhf30GCzRGcys39fQNTUXqDuhNv0xOMYI=', '', 'Dwijo Topo W', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.052', 'b019bba73829bce90a68557686c775e8fb1f0bc4451c7828ae4e8d4b3491896c614ea3e24c8482bb3782245ffa24f0abd73fe1ff29484ab1be636ecffb8157ebOPk6O5pyzyMnt8LrFlRvrgDF0l4mk19W+Xs8bJIq2us=', '', 'Eki Agustian', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.053', '1d5d3578e301eef9225e617b0a846a3291b91dd227960b9a3df98b73b797d8db1e9c9a928b1f07c5cdf0a4d49a9f2f5fae00e5039f02322874d9d1c6123bc7c9iZn9c9KZsZ4u4qhNoexb69PMf/sAiBBLRRis2VELkbo=', '', 'Eli Chandra Ika', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.054', '7ee966654ad5973137cdacf58410b52b7101b87cc4a2ee3cdb8de85528564bd301edb049e1537f30c6aca85da4b61652c1f26cc11a15e124540a9d452abec1ebG2sehpSY+Cs0lAFC+ABTDAABVbRVpF5iNrYsgcZOIt8=', '', 'Eliyana', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.055', '3a835f1a2f4fdff9c909b78f620cac99473194a7ab2e9c113f8d753547d6e076de8fe210d24eb493651ae64441fcb096bd5c5a69bd08ab88caaa9993a3d70bfeWZxTfFfeD0ELesQsci/UeL+n5De3Uc0rLlFt3l9oN6I=', '', 'Endang Maulana', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.056', '08be7529159b32833589aa18e1b6862ec9f92ec74c4d14ebcb93733e78ce31eaf43a548ad0ff9981869ff86660a1f91a3ab0a30f1b9f3b0e2d7981b36cc4941fPoIHqmbdAFRo5w3cKCAsRMwtyS1iV9FThYmP6TBWcuE=', '', 'Endang Retnani Arumsari', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.057', 'efba40d07e7dfc71a502ec0491d4116b405e2cc4d316b8848f24e23e412a6eb01172231d929f5bc079f28ff1e031ea326a880e44b6f9227cad42a59b441dc6a0F5NZRqWJlXgqH92B+2PqFIwfTDWaYbGGnlAVjwYJp54=', '', 'Erin Marlian', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.058', 'c3eb7eeff933b61c992bb4188069f34988c8b6e5a6cbbd961bb46aca3784f0b0effd4fa65079c872e96fb9878fc002ef976d5617b8a61ff5e0bca048e56503ffBfX0Tm2oqW/YqqDvx1cYPX7SAB5EAkOiR1rNSxCEMoI=', '', 'Erlinda Zumarnis', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.059', '868e25316f734649753d2972ce8063dccadaba680fc874cd7b059bd818f09c7f3e1ee2e7640fae348e125a6cfcd039b61ba857082b621355fffec88a652b6d47vV8OXj0lyQp7UyQsVPlQzZaaXTQJf9ctCmy0R6wNJaE=', '', 'Erry Nurhadiansyah', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.060', 'd8f1d5cac9d11f5e610346ccef0e5226672fb3b4dceaae57cf3cfafc9100f8816959b7cb30c5cbd06528262ac5bcd1552c2078c9d40c2af3976dbbd92dafbcbbvpHVKwfZ3wnxQ0XFv2aus6j8sJ9+0NremZKNu3IHtww=', '', 'Evy Rahmawati', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.061', 'd9e6093f4dd72d8709a8bae74ab1c2520ea68c0702cc9c03dd5b845c47b670d183e7638538cbbbbba4939b10ff496333ce6ddfd03d98a9e21139322776f198adXZ4I8yS9JqKqSaHgliREEKws8bomN+U5abhmsBL1bkc=', '', 'Fadilla Rizky', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.062', 'a080dcb6ac9334f58152b6e25676a6313e0cc3f56db5bbff9ba389bfbfc35e68be1dc082a1b8b2bf6ef8914ece4f7b53d648878ae8e4e4eb1e966dfc9b1b063eZkMC9xjQikJ3B3rD95IuoB1YVVqpOWXarKjB3V5xoHk=', '', 'Fahru Roji Wijayanto', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.063', '591237f07e73d75452ef583ab7ca8b6b9177f762056ff7d5aaf5a0f4c69b172514c7b79d6ef481aa993dd0dd211899cf4055b9a7a9cbe16fb4fb5be712152a94p8hgYglQ0bw/dOb3/ULr8uAtq9UQ1nhVrbcIuWXCftM=', '', 'Fajarini Hanisa Devi', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.064', '1ae03c9dacad8f6cbda26a3fe0d2ca3ff2b9f49b8557dc665d77df17b06c2b8f704e11266ea0414ca5f1ec81c68211b792c0ee9d5cdcac30cf08ffd144826f22lUlbJ6ktm5dfR4Lsl0nednSuOISGHv45M2H3njxlMKU=', '', 'Faldi Favian', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.065', 'ccc96208a69b121c809cacfeb42d1f51124f62b6e59d9fe51c77628436936857455fbd829e898b3c2a5032a794b48bf5a55aa9e8d8bd3ca5cffd5a2e235be5e9xZLdKcmpeSyL85uLOuH8koYsCwfmTK5mUrCx2Idenbc=', '', 'Fany Rachmasari Choerunnisa', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.066', '995fc8e4d64a78e4af354e04aa5971d5edcb94f334b940c007e068a1db8621a741b4bc20e2983faa82f8a6b2b74537946732231c1ca5322770538195a1f891b8/gE2euXTQOqEafw/+9hbnyLvmh0sMJY5f99+iV+qmK4=', '', 'Farhan Mubarok', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.067', 'af9748e42e4b64c10549d1afb52af36512b8777e78af40600e07bb5710d3b63d8d3c8f8d5be7193fca952ef50077e2b05a2ef3895d3c90c9207ea2208c7e3c51JvBXWAmHLKXrMPzvap5AfwRkfthocU3jR7zQWWhjylA=', '', 'Febri Alfiana', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.068', 'be1c2818e6d9ed41047379337b01af9821b607dee3fa7c4563bf88a0d170e0e64db4591052d07d7408b2e54eb0eda64233ddde4e7ec0865e78ce0303aa49d8a3trcx71sed4RF3PP1hlsLww7M9igsMCGLFfDkZprUuxA=', '', 'Febri Noptageri', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.069', '3894ef62f9dfb3a379f171817e9a28796fb5e46d3c403728c30bcffe63d971aed94c142b08441513c87edfea7b27cb821fe3054bdcb70a921d137b06698232fcOhMRw+wlvARta2OQd01N3DdiYhlpsDU2df4SkitZZfo=', '', 'Ferry Sofyan Rohyandy', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.070', '97fcdd90e11d706822a7328b66237ada2daf37d0bc41da427441e9bc788350d3b606c792af09e934381566f43343050becd1c12ce8c7e9f67da49199d76f0d287FPR/M/ODOLX6qkjVisw/+gehImeb3xnUty8FdcyTIQ=', '', 'Fikri Tawaf Al Imsa', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.071', '9fde62ed431a612fcac8f6b9c81e0399b9f0a14a5bd5f39c2418dd55db7a383b6c71436c0bd0953a90180c1675a3705b08ca7c269e811d6a7c67c554c7778ef2e/tg1reoLPJXxsI2DeYWwT2q984gXooyaQye2B4M9zI=', '', 'Fitri Nopianti', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.072', 'b481aa77beb577808eab9e3481bd41304ddf47c516d3b2e589f6af49dc08670613182d497f4c450254a089f91043a093147c4310ef2664501494072c44b57120TnPlTEE3XU65KRpcpBFKR+6YqEf6clOX7cbAwiH4z5w=', '', 'Ghifarin Algani', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.073', 'a1e63f2665f98b3610025380cd59ba224e1c38ed6482196be397dc5893427064ba20557e38b591e9b131d40d207a618a04d721f0d507458efb78ea95522c7975o7PVnXhDzLNiRYInVVy4PUGHo4sMZS3wk42uEYu2RSs=', '', 'Gina Aulia Khoirun Nissa', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.074', '1feb4a9ef07b2b59e235e96fc2b9e686a2aee36a5529ace1388fc8b08d28afcb4d77cea9e6ba060906858ebc6b51587b1bea199973264705b5627e420fb557caAqyTTZo52iGTONmpdA0yyUXGPW5vfsBMAGbgFVMiKwQ=', '', 'Gina Sahla Arozak', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.075', '12df0278cd78c0eafd9516551f7cc843244791a65ce09f62be1eae64d3266f6dfe620f14daabb14733f3a298bf7b9b941d4e2bce5ca7b792561969d29617d8141qrDcsvYzKEG0VopvbLNu0qG1UK7SwFROM/k8sRB5P4=', '', 'Gita Hana Pertiwi', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.076', '560527e83a1beb1274cdd8d8daf439bbaa553d403ef345fd0119a5df172f75f0e26af1bfdbcedaad9db3bfcd5f093dd2137370d5bce9149eecac2b46c979efc3IKf/dx3a+LpIBAoR+BntUheqx7VPAFvVJs++pzDpFq8=', '', 'Gloria Stevani Hanna', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.077', '5961436ab54bdb56e49cc0c231d0bf0e4f4dd9a5c44b85d520b982afc3c1ea29614995e496c1ce2f099ecd7ee1045bd83a2af8b907da5c400ac19a04e33632a4W+yVBWX3G6nrJqrqJ4PFaQlP/5jpFY93eVaZeORHPws=', '', 'Harri Hidayatuloh', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.078', 'fc06bbf13c77b9687ed737ef870e2860cd77dbb754398be87e42223cf3d49c54897d104e0e8f1825b67b6293f4e5fea42f452af3c373571b42187507951d5b014+ah9mKbyYP5+tUGgeZAG5bORg8g43Akz0j1UtXc7e0=', '', 'Hilda Nurhelfitrianingsih', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.079', 'ec76ae706f968d1d7024257b4604d51d22281c75a56b11aa7e3e4e08789cd6812b0ec67b66bf1167cfa8d0e3c007e1bea9b1c30657c0ab0b3cb93719f18f10c36jGcQyRk/qkZHO1xxWPfgQy0WMHhbBFNrW/Ap8rWBv4=', '', 'Hilmi Nadiyah', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.080', 'daa08c9d5c3d1e69f5486582768e0504616b5cd1289259b46d66dd622b52db7462f8490b2bae87a70148876c71a1f897715d2be30bda6338e2cb75483c76126fJECwNbtjtfTgClrug7ThfqmR2vrXiBYi2rWQYxDw0js=', '', 'Honesty Audi', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.081', 'e4c8089101662a5813a5d45549dbf1d2a489363373328f990541dc7a3d7779474c963e0c2b215aa291db4d2928110a598f937cc85c9561c3bc74e03fe40f2f5bz4C4k8B4wN8N5DSfJpSu/sV9myFNi+8XZk3fQOt5Y3Y=', '', 'Husna Kusuma W', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.082', '4dd358c3cc50ba684816a43f384ac6a5047e53f9af67909f08735e71fab1ef94731f368964a92c3a521c019e9845eb41bd5e61a1b679c14ca3c0dc9bf2c5cd81jAs49evGJ3Npc7fbQGJIy/HKK6mfeOr/zBpSFGUntn8=', '', 'Husnie Fathu R', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.083', '7651f77ab765c1dc41346d6d0283bc859524099752650b7bec7a7df3732fb6618c31b5f3d2257765250c46cc1c936c3ded5c20d34749174f1ab1d751fbd5aab7vv/0f6PnpymbaMdnHzvskhU7SakdBPA22+cHUT3DToU=', '', 'Ibnu Maulana Hasan', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.084', 'e68e88dcc1b9a4a468dcd96e941462b130415d846cc7f1cb9450871e767b3dac721a3f7c595403c82b9ae3de8023dd31d287656ce906836606b328590b2a84casRSrlCqukkRDDlWdmP+IZMiTC3p9uoPSF5f685U6G8U=', '', 'Ibnu Munzi', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.085', '53efae14298649f998a34440e5b2cc469685ba53b48ed804eed3897b87d057ea4d94434d7e865ca421f01b9cc1e9e34515a93b5ce962194f7730c08ef8842e995W5fsnP/gl3WesQa09edOJcSnyNvTIZt1xgl9P/WsGk=', '', 'Idea Mujhida', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.086', '719501601129f4da4e16ecf671ad2de1717da536d221b741860e995da76d693081b8cfe6ee3323ba83e2ac6068c3e9fce5178dec4220b5ec81e7b81a0f0cf4b28yS+WkcYcbfYHqAXIhdeiYw8ODa+8dBkC3nsBP/b098=', '', 'Iman Hilman Firmansyah', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.087', 'c9ffd5db4f942b484c65779eca511278ec36efc0fa461651ff3507dc7f1b08f277e6e16f632816c82381caccb0b18d226cb9300d9f2d9be6ca2388970a3f4729ojjaB0VwEUpbALsR/G55+btGaeGrot1MFoyE8qz/RRY=', '', 'Indah Hasan', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.088', 'b1c733e9623f9544b80595c73438463a5e78cc984063ee37ec6c36518f15aae362e09efb46c5bee1401ee4fe9a80cdc34cffbb217f10a22f24ab7c542d553950Lb7YibfjKkbGPoPE3pRNxKigXYosMMzSELSNO2iR6Q0=', '', 'Indah Huerul Nisa', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.089', '1bea163c93c538980e949f7d567bcfe8636b599e7310537e1d43f853f6d35027dbea063c3f0662115550e32b22e82c3da75093dab7c139964a1eae68074293bbhl38KAtp8W5mXvU8sdPGnLe7Eee1SYXfQaiPXHgCUeI=', '', 'Indra Gunawan Wibiksana', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.090', '88d8acea9dddfd677722a6dffbdb0ddd7d7d443c2a12ac58e2c8d79d1432470c460756ffe52f0d8e00bf7ea731b081c988a952cd2fc4881bf83adf3b4fe239bah7JPniSPAqhMC6m3CsPGNTQ3YNfV9Qpug4N3TzVW3vs=', '', 'Indrayana Yusf M.', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.091', '3e2d2d93b0655de881187ac54951f63e7424fb16b5f02dff8007472158e8310386346530c0ed28e283e99344385e4ce4fe01ef6cad18000dfa9b4a2417d90a66X5TKKQUYYvh6JR8sk1VBAVtHuBGjmQBL6osXlxs7Gio=', '', 'Intan Augustine A S', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.092', '54242b9743189baca0cc85ced8386f516320d93f83c9d5dd9dee50cfad29cef14baea206838c32ad1e44e41762731d6331a2a440b3d90dc41303291db8f09370fNCmIyWPISeZ1KNNaz6kVCi3+rftazIEPnYKEOq9PaU=', '', 'Iqbal Ilhamdhani', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.093', 'fcf104c8fe9b3425e92d6abf7602946bd65b90061d95345c6a89222f18d3d053207992a607b9f6ff51f364eb4cca51efc9b243be2a7d868caac74ee6238235a8JYGXx0prtKIq/e4k0EEddQprp1cdJVTC3orWPLswv4k=', '', 'Iqbal Putra Ramadhan', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.094', '4b38602cd31a650fd4ba36781d48737731817a204b135ffbbd6eacca0c2e75a54493a778e0d428dd150fb71395113d30b0e2fb5bdf5028196ae043b9ad176bd1pGtAysbleEmSShb+b7Wq0dL9+vSyiMgq6Ss5kFekIbM=', '', 'Ira Maulia Nurkusumah', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.095', '8be98af74a3ff5cf51f22c14e59926c24f196b0a1c8071d421a4ae6eeeffb404d67bc22e2434438330abd1b61f84d8fc16cd4bc0ed95e89a610ebe482f65a297HeG7GUGhLpctRK7zNZ7qGrDIqWcAQlgHI92Wm03GyuI=', '', 'Isnaeni Ainun S', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.096', 'a18a8b761ecad7c77a13cda7ad32b312de45a25eea6b2b54d6a5370bd437d20e8ebb2ffbda4792c7ae4ed4d21c730aac52cd05b3763ad47b7f7ac9901fa94f5eb9Qj8/PKxAhAvqLQJg9eo/ZwrjDyxDxC0uTMKg9RZ5Y=', '', 'Ita Puspitasari', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.097', '243c0a93c8da79b878bf2a9f881c798b2ea7d6a56bb3d4faa171053545c0dcb3660fc6e730eda773c620b25f3806c4ecdb1b410e7a11d70c605f9a7a38f1c5f32/J/9T5oFnKpWlkitWUMDI5SACwxzYk2Y1KeNPRlRDk=', '', 'Jehan Fathiyah Azhari', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.098', 'd2b3b8ec534ec59849932bdfaf8c11a8ee86248d6eff6ebdefed9e2a92e25982df32a42626641d5464f8112b08b37e8b98a92d4e495f70734fa20b3f8cb64a32xAxjpEHlnL8THCyryw3VW8CfgS5Wy0vXZtru6ES2OiE=', '', 'Jessy Wiona Vionita', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.099', 'cc61f984e72ded9d4c5764accc133b752275810339a4894b77968ca594c3cb7e443d1502675c996f0b01725b44faa19269ae3d67f993c298bf3b32a0e7c9eabac1jQ8zdAOXirlMJ5W+BhOMuWbtxAoPlF2BnbIXyydBo=', '', 'Kartika', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.100', 'd6e67c77b9aad30616110f15b421a7f198be18a66bdc65482bbd4a9ddc812944141727de53ee7884ed68faf9f715243bd0f9a9286b6e17c61f06350413712a1czBe1SxbFn06bz5P1TLOcIXctdIVaoDgOfIMZs8BlOWc=', '', 'Labib Fadhlurrahman', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.101', 'b84ab996b83518a0eb20495f34568180ba279b9c09ec7d4dca9156aa1677a30c035467dc2aeba48843d63e6dcde73472bff310464c99406ad7abadb009a2a14c14t4++IksU9cDuO7RBS5xQU4NI/hy/Bbie9vsebznLc=', '', 'Layla Alfa Afifah', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.102', 'c1db005a1ace78c203e92a35a6b9d033dd27570c129ab54d9a0adeb398bb74a6dd36b3944f3c9efeeec1fd693782b7287df5a4b3f8a11a2d0396779f5bc13904iT11J9setRxD0ZqV6BytWcXKl672/Sfvq7NAKc7Al3s=', '', 'Lia Yuliani', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.103', 'd39709640c861594a9930db3205705bc0a1dd068a693758895024dbefa976419e8f593c1290044b56740433ab5b32c0fee6c3f91cf2bf56084f243ebc6c2f618jKZGf4IfwRX/yCsnPqe887AFC+GEcoQlHdwiUC+6890=', '', 'Livia Putri Ramadhan', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.104', '3460f9275aee08651ea4103055864ce67364cdf9347409b8ba15ca59a264998391850c90c27d2246044f690800a380823732875e13783a3dae0d703535007bb7QUzVwdVIxd9KtGFFTda+cMOztAo6ah6GX342l8egLn4=', '', 'Luthfi Baihaqy', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.105', '5bd3f2b481053a0021d0120c15b3b567be3830784dda128d1c3569a4e401399b943ac5dbf41737b40a73fb72c7bd37c34958dd8609085a80d6e37cc62d32ca76gudN95HJpPfttvZcBSjssFyZLOfW7Kz4mDYtc8d+NbE=', '', 'M. Aldy Rahadiyansyah', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.106', '2d50fe2128f56cd0d908324b46ece20d0613e5b78af7b81a6c4e089ce564fbb169868e948ff626d82173111e347b0d0879d5c9452283a0cff04fdc5ed7e889c1Vm/Erto+l/Laoxp43slmoKVeIBiVGHdUQ7JnQhU0hOM=', '', 'M. Riyandi Ludyansyah', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.107', '468ec4bc29806713db99d3c6463d6334e077ad3d785927e755aee934fa0dcdef87f0a91848bb90d93f6ade8c3ad317deb5b5e1ac24cabc5d679be4bec2c4af8f6msIAGbu5mQn1Y7Y/3ufWB95BnnGCwpV+MHbl9i/Ugs=', '', 'M. Albi Trismandani', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.108', '31b86cda7e4fd483204f2e8c38e6d66c40ef75e278654d27854aec9bd18b439a1d9829ab4678990909e5fd9f24ef35f8f7592c00aff03dd9ec3dbd5f849cf4c8WsPoyGkE3qdZiM8WRAHqH82Zehj9koj6ySRdRScv4PM=', '', 'M. Indra Noor.S', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.109', '61bce86067fcce038a911c945a3f457c2f4019bbf27643057fc0ebe8a40c572fb55bdd5d0fe509909d71d9c9d3854cfd40b68538917eeea62f624e4f2496b06bPg3/S860LBUbKHHRcOodg909WQvP2yxbYi2UEgJpsk0=', '', 'M. Sholehudin Mawardi', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.110', '21c831805d8c2d2296e9b0088863ae3bbf967b9275e0d6817297433f1c0a21f86e72c2883165df418c284b81bd73c2851b120c387d718c25c3520123b1aab0b6EClTBWvEu8o0OehAl84LXo7TLAFlNscg7U5WDA4z4DQ=', '', 'Maria Yovintha Yoshep', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.111', '2a2b9327affd96cb267d36ed1e14f925415305a8230a5593e6caf295ae53c6db62436a6dbe6879cd0811ba840c86cb921015839e68f789c1a6b8bae5a6e3be77+u3oGjGvfy7Uj6qqFcc3QjozscQnmzwzsADRDmT6q+E=', '', 'Masaya Petrinda Olia', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.112', '90de94dc0d6eff3afb4613b85dc08a19aac9c16e83d6cdb4fe6e567a71ec40d5ec8840e6bac38472fb74187510857f8a87187fa6839c68d6a9d1df5780dd459aVP9YFkxrX1KuncVMJ3E/hk9ctWqVv6m1618VeiJ3RhM=', '', 'Mega Tiya G', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.113', 'd966e18b1edea9908c116eed322120fe9fa399073c05084a7d2da3392dee32cc0e46d0a768dc830bc080fe5bdf266e7ab3b31d6f158b2b130524797440e9f3bduhQLMqualkNu13EKB+w9P+YiDwWbiDUjy0r2waMAKYI=', '', 'Meissa Eka Wardana', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.114', 'd354c61b9344acb7e9458cdd862fee4a6efe6c5605e446a87eb42f8f9bc6df8852d98d5b98d090af83dcb55932f75fcaa3598511a65ba51389be6fda2142eaf2S2twEKXPriOOXTVww306NyBrKEXMMp6c26TNUWoLehQ=', '', 'Melia Eka Yuniar', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.115', '12317d0e5209c342356a8777b2793aba26aa970a83c557201cf74928f7ba0ec41503d78ba052d49d70972fc7a7d8608c0bc292f61033382c601986dd2fdaac76R/rYvEegG8v0za5D1L+Lz4jnGblRPB5IT6dK/Dyhxd4=', '', 'Mitha Khoerunnissa', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.116', 'fab96eb2d79a1b12f2f5575ad82cec0c7d8a6ab3189d3a2bffd39ea34e51c5109fbc882fcf41f50534f2ab9688daad3c3abb92d0f0efbc9369d83c5f885ac5dbeyIy7A80fBvB+IjV/2LFeDkqUd1KLwqQ1eF+3WX/bOA=', '', 'Mohammad Nanda Rizky Dhiya\'ulhaq', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.117', 'f0666b449b25382145f4e6c68f0759128b8800a8c2b9c505aa738eb4531b0abaabcfab02bcaca20f34c10e8675b3a9acec2278e80989d63cb8ef8aea7aa12d0f8B+k2JmHq+9bBQcB9zUYBYPxQZ2QeoGSe58EayVtMtg=', '', 'Monica Claudia', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.118', '7870441a6f0fd442027c89ef361aa125bfe525232f06eb9111c29b1e9c2a75ac5ca8d64ab25f7c05fdd118e69e004f6504956fc2d48e69701688f066e43866bdbOOdXjUZdjd53ztt1n+OhS9CXLZzkmGouk8kKOxSB7Q=', '', 'Monica Romauly Weu', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.119', 'e927dec89d51ad47820807a065778eabeb033ee474f31a352b707a87c49e14567a5b52ae34840d448db69ce21b0883db7aed6e5d0d32a0e667c270754069ac187GY8iXLsQqI1UAwHdzy6SnusqGr9P2meUcHA3nvWtiI=', '', 'Muchammad Yaasiin Asshobry', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.120', 'd13f522fdf100d0f8f12f3d73f477db62f3f71b3848caddba2d44ea3f1c37482101e885f468ae5032553d35d62e5e03bfe51df01b395e7dbe94e04985054c494LUcBpiQeec4smvy/xr7Y39LHyvQVemMOOJgmy+3faAc=', '', 'Muhamad Iyad Muayyad', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.121', '317176146bb2d15f49824b000174126567bf4e9405beb3440f9fc5bc9424e10fbcfca8694936c57ebe0536188becb4dc53832fe87968e0963d8825879123f90bYmBFDNQuRk1CGZ3YayI9+uN3+HXp/bwe7uFIuh1TepY=', '', 'Muhamad Rizki Ramadhan', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.122', '7139e4d43deddac68da6aeb0897a9017e8388e2cf87c0cb993440f04be640cf3f22050cca75a4e0ee8cb0290dab6d3f75ed0543b936cbf05c60c81ae31028bbaMfVMmSaKkIbLgTj+ZCone3By6Ty7g0NSzRgAjg2ASFk=', '', 'Muhammad Afif Pratama', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.123', '2a14ee456417a0462dda2ac52bdd21233ba277e9e689696a9fcf53305e699334e314e681746a090cc41e7c1f01ce756a8109694aa7388d8a184570d7dc54cdc2MDblhwmBW/V0wF63yI9krkXVXQqTgUtmhmeYPHY8RXQ=', '', 'Muhammad Fuad Salam', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.124', '2cfc25a75a730ba1b73a934cc96386628486d9a9d84ffea8529681a95c81ce222ceca3fd38abc2b345cb9c0e4eeda2739d2aa75c701f63d65f39ee56903d72f3g752vxC4fWCOP0V2YkEtXIZhnA8/VrrYNdcKwgU72CY=', '', 'Muhammad Hafiz Hidayatullah', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.125', '3d7979a5b32f872fe5559d8d6ca8dea206d295dcbc718abb651abd4c67bf2486c3933b6b28eeb34dba5f1d383260ed826ea0a5dac0d8170c34dff63a9972044dvh2frSsM3CT6Fqw3d1Eso4NepFwP772G0eYxCkkXN3Y=', '', 'Muhammad Mugnisalam Maghfira', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.126', '37c6de5143c38955389810e6375440937fdc9cac168dc011cb4226b575d6c32e8229a0aeacbed537ff3905554250ed984daf771be4f8a7ba122b070169346c82hYeiE1dJXY0e59Ufac/r3qBIaAKYyNjcoY2HpimtzyI=', '', 'Muhammad Ramadhan', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.127', '8537cc8fd619f3f73300999abf8639c2d84ae3f961bf4f6c278d342863ee1cdecb5420c59db7d22f405edc4292f3dcd77094b31e54679c11ca11b1a0471666f74H3P+NVXN6iNngLfsQ3idq3EBqRY6bteA7lTl70WaZ8=', '', 'Muhammad Riofanny Maulid Akbar', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.128', '852ff97ab22c03864272807fdfde31428cc9995ecbcf49864b026d68fcc170b75dd84707dab4095e41f4de23f54e28a232960f23902e4ca248c934511cebb84a4QUtkZdIrbPhsdqGAPkxl0ox/em6Dvj+Vn29CYWjRA0=', '', 'Muhtar Mahmud Kahpi', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.129', '9d59fb99e24a0bbee273cf7ecf13bbc173f173d2f37b15e00c7df776ba937efe52e9883e66e0b0d489c1a12ca7e3e1cf585af078a673053be1e845779b7f7894M35lIzP/4d6BWhir4beXX9d6wjbv5IxQ+rQjq4tb+4o=', '', 'Nada Helfika Sofviani', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.130', 'acd594f86e9196c2303d164e00a2db5100a6318481f5c35f4235900fe06354dbc2ad012dac614cb1e60bbac532e75fb789e7d7033399f5896f02c940dd59c0075Kk9GkGl1HVreiScyQxbxYqWpVJBElYNeXLGjANvd5Y=', '', 'Nadia Salsabilla', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.131', '60fb8f902ace378ec19580a10745cfe8c6144e136622dd3e7bb38508d80f45ed215582ae9a118865e30b12515987f2c734004a82f2545c2bf8380fb77d3193ccfKDbKbk8Hvx8OJJp4gKCxOvpJa6RJlf9SleB6po3NDo=', '', 'Nadia Soliha', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.132', 'fed37fa4173e4068dbc2d4235064c94405bff2c36d17493b0bb6e91e65d0f6f64f56b519ef890d96668162bc06df757a8eb3855ffe7c1b23f8b748b4f059f897pbkqoYfuhYqFmoZUakAkMZ+QgH8q6aoqRQnq5Kt0Xk8=', '', 'Nadila Laila Adha Ridwan', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.133', 'd93a70bfaeea81979ec59b644f021ef465d40a02cd6bc65a3bafc61eef79cf032dfdff00bd3847e6f4c0167e9f8da73c521ff2beac3890b16694ca6435d5b042kH7X22knrKGUv/bgPvWGN5o6XG2WTscmCIAwGwznqpM=', '', 'Nadila Luthfiantini', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.134', '8a8282e6c05f0aad3edadc0b101f21b85f3a883159e2c8a42bb1379507b23ad1087970e96841045fe51185ba7932b0c887eb4ec7f18ffdc30f7d75bdc4ac537af3EC4NlUHNQLVCP1ZCzpDWAjkMlQ1X5/5ij3evMTgpI=', '', 'Nadya Nur Faiza', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.135', '034dbd7ff1d8114a1cec84704e569629007d411a608f5780ae5ca8eb7355a56da1019cb36887f33c2b80cbd085f5ed2a419c87d845e8e255059ce6bc0adb8e54fd9i533+12UCoNGecO8P1G6hbj/CUTpkfxF2pid7KQ0=', '', 'Nanda Silviana Azhar', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.136', 'cd70d3a0fd707f1031e14c687ce411a18b9e0f4e584325c5ec250c06d04bc9f57099fdf12ac4c0d5ce443bb9ba44d675055cdef407acb58671b0cff84fe350a1sP1AWanJNm5Ow39wO/G4CJaDJ56jM+flOGFHrYA7+l8=', '', 'Narsih', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.137', '66fffd8f7b93eae89f18f9057e7eef8d71e22e2ed3093b333ffe3d3b8a1e48722f65fc8ee4cb3a8ff3f0e5989f6c435ecef94fe07c777225f05abb7dc627bfb78XrW5bNddGIAVbBenBHxowpdMpPA0Liz7hOR8HUkwuU=', '', 'Negrita Tikasari Siti Aisyah', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.138', '9481e13bfa4e1b4d0c0e17170ddea4bc0e802a6d58e3949309f05f164dccaa0f6c1f0c659691e3d4d71ff1525a40e46eba4665c7454835c8d843a1e59b7fa8e6i0dyayjUzgwoBNGf0Diii2AIl+jrhn84DoV/ZBDi77k=', '', 'Neneng Putri Utami Haq', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.139', 'f07d2e0731733690e7fd5ac7825ecea7745997b861a6a95a2f5c9249a74119afd51570836ebc2f50f23c4545c324a89093d1535ac7352d43ff556044976311d4UXlvaljXSl4yi97WEiEJAFk2OcYO2uYrfoeaYMid4p0=', '', 'Nida Nurmalia', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.140', '84d83ffbec580e05421ab31d36c54e5de02228feae0b98bbf0f6d406a7ac3d5656eb3d1ef3237ceca2908d6175b87a9d4c6038e8589b2ad094522ee0ac51d74bgGPKhjLq87MIy7CICy9aCoMamex8aqAZFHNa+YlKiaw=', '', 'Niddy Ambar Katresna', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.141', '83d9ddf21e601051683e4464522723058363f5b0cec6f848de2de2246444eb641d6cf79d99a34e529e9d3e64289857b94b150f8382d75f0b8c7569f25b9bed90lWHftrKUkM3raJnsxBXU6NxfdgAmE95KdW3H1Ip5bKk=', '', 'Nita Natasya Aditya', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.142', '105246652809f814b8aa8d8fc7c042e11ffc43cdeb6c4a24427b7e94b30d10a6803f066c8bed7a6d637107304b532f79bbe84c2b3cf6c231919e02e76568b3ad8Bcc9lOGKjJGOYN3HBTwfMK8GFr8DGtlT9tiFoJPKiM=', '', 'Nizar Abdurahman Firdaus', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.143', '7a7585fce68d17eabd830490b9e2c0a779ab27a99ecf04e8b0eddf28f924ff0be5527fb9443e5bc0afaa56459a69ccdea144a21e5ee330dae75c0de36abc22195ndXOcFPR7ORlxVuQ8GAy3Udz9H2aAjb5NvyiUzqd7A=', '', 'Novianita Siti S', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.144', 'cc4a7b654218a7e7bfd39097da53f4bc07aece9056aae28a8c66664ca767762d82839967e4e69cf3f482c8524392998ca5c66a413d19585704e9ff57f3ca22e9XfHueL5hDat3EB9RJmeSIHvI2OccuiDBCrjUxCwmQXw=', '', 'Noviyan Prayoga', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.145', '43e39e6c636040e6cf3d21dcd31d8fcda483ae3b6a2d6389e02c5dea48b093a87d43f629a2ae77a2945a1567334f8b77feafaab95821eb7acb768b4afbb4ba15QalzDlR4VDv3bbcWZdRs4h9t3+tDoZ34sxHkdvrx1Ic=', '', 'Nur Fadillah', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.146', '0d33a096500c71995de8a8350301fdef87adf2321e4a8b931026ee229ab9045b3daa9dd75454a3d549f9f7fe0de9dfc035313fd41d7bc28dfda3874d47cdbc1e3Y1N/JWlDB1j6+T6ETErY97JgjBiS6JcUITQBGBfcDI=', '', 'Nurani Fauziyah Gusman', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.147', 'aa6e4bfffcbe2f23e248cfdf3a1cb49569fa7198b860e74be1797acba23a5d96258b7e795be7d7eb6302af54897e2a49ccf10e98e21d4d7041cbe1b5ce34be32wh89eaKTghkDKOQjLYeS3mVjBdj1m4ayHbiR55GRGpE=', '', 'Nurroby Abyanul Haq', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.148', '51bb644e6d971b5667f0eae43cea1defca06f1700560e5cc47bd61ba1647af522f07432bb275869fe55d1c0d5bdaa7a4da8fa909b2466c5b0b373851f1081f3dAVBbXtnXszD+w9nUEZNUxWEdEbuoCRX5QF+0nxl5SsQ=', '', 'Nurul Afiyani', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.149', 'b5ed2ee0c5b53da50fbf423511b5d8f7454990e9cf1869dcdd85e462a1046d2edb0769365b8c9ccfbe3b58b267f6519376f417a0985d4c15dd12945c0da79daeisZBPZv41doiaWdeEk1T6cxG9UWpVe7WDhwLiwChujA=', '', 'Panji Prasetya', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.150', 'bed25b12d23c6b4997fa07be438231b7e672b8f3b98910638dbcdf0de92d7fe64dea030dfb43ed9e3cc23a7ab19d00e77ac5d5079237b6b345129ace3377704c/V67OooUi1j72Gg1rHJcFUHaKPKj4/rHWNKQG1fzkNI=', '', 'Paramyta Ovestiani', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.151', 'f6b9892887df962f5b5b564030eee37a56b5876f7c6eb7030f5e2f2ac826c97a68c7d132058e513c06b2bcc4b3b5c5f36eb57903c1d294d6fa8798c4e9c8d1917LspTaWSntKkmFECeQdhUJxpI/2yXBga81aJEF4CXCk=', '', 'Pevi Nurahmad S', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.152', '4f0849d688f19da1e47a499075f42742d56c3c5b4f6d2275983c8ff29e5b2796efc01d0db8d28a87d0ca356a5fa38f3de104b0e6f8b2d83ccef370b201b2ee15/r9bsolLP3dO1RYznsqTAQ7+Jq4QqHJ+bTHnHJOuxWY=', '', 'Pinky Aprita Sari', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.153', 'b28b3fea6e7fa8b406d30d033319ca21f2a82c6ea0b9b16c280075ff4a53afb0711300eede0efc270e0af1f138b067b2c2c9c60b534829c04c0dc236185c85686En9w1jxDKorFgCUfcraICeZXFG7ImNvKbk+uZfUkOk=', '', 'Pradita Galih Syaraga', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.154', '7d411e790fc3d2385383cddbef0e8ba30778ab5d7be24d19e4d8d66f30837a33dd77e7fedbeabdc65d130b2aa05af38e9a6fa228b289dac71552e65994fd022b6wARz2jcfnq97G4f00mDnt2G/vJcVHX12OnhV6DcgjM=', '', 'Putri Ayu Amelia', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.155', 'b140c87569837e6ed0fe9c73e731cd004da118d394e9c406f4782826c13556d695cb2b160f4923e00ba611f7ea73564b62e26744d0bdeaa893f18895277caa2dDZ30mBNqNCaRHA8rY2rsS5aa3vuFT/pGa+SbhEj+DHg=', '', 'Qidan Zola', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.156', 'd0ac8d7d3372e8beedb77e107783d72b6e077ed9ea743643055da72a19f9068f36a916b97c5d05bbc06a27fec1bf8ddd2dcf2bfae02846edebacd11f930eb9acH8zT+zqw8FCP1SanhXeOfGFKAOQ8f2LtUHJv7dlzWFQ=', '', 'Rafif Muhammad Fadhil', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.157', 'dcaaba0e297274b03eda4a897224e6b809502f9f3475537237cf601150656bb95c90396dd905b07f12762d371d7183e5eddbb5c41ad2a5feaeec2c2f3de9a42dtRF8TpuIGV86UffiRrwkdZWexJ47bDXK1pGS2T7SBpw=', '', 'Rahadian Dwi Putra', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.158', '55cf9b844f0e9bb5c5b795b553a4e2a3ccf73f621cc02b93f03b6a65aefa586afb1b747b53270892576c59a41586fadb177eac8391cb1b528698397c11428bdaVq+k0nR+Kp6bjcZBCkPLgHc5pTEdadvhM4rpew1IVXc=', '', 'Rahmat Nurul Ramdani', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.159', 'b99892bdd2da150e0162ea6ebf2b473eca1101738b3c9c8e35a74d349d4994580ca04e3fd1c03b384738ed9790102f5077f215d3d3fc927662df76d530d2ff6c6t2XbpprPDSY9YxocXYbN9rQr5NGEd6dh59V+hK6rus=', '', 'Rahmi Haqiah Malik', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.160', 'fa86deacc99ebaa1bba3b9f066a7652835b679031648137275266b0dae4b4c66ceac9a1b8ccf19919fa696591b78d0e1e0af0d4db1fcceb041a493ed8fae4f23KM8rwxG7bDp/E1ey+1WoqG4o8+wE13XVf5xvHMUL9JU=', '', 'Raihan Irham Ramadhan', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.161', '58955319f5bc0cb8ea9f68edbd260b7dbb68b1f19b574369f63739cf541a605cd5d2ac0f8916d1361ce89114d2360b808977f45bf39dad2d457a4b28f46f59dc8XMHN38KgxKFdUp6aaq+fG1Xc5clxvgY6HL+qSoouDM=', '', 'Rajif Ramzani', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.162', 'ff90a8ded5b068fedd02e5b8e10e29b35530972aaec2b5a5c59eb114306193136a3265da7236490ead4fa8f664df8f3e0dee8b2bc14bbaefa39addf3f92c300cIfQKCs66Y6UAUReBE4JnwshACjfBMMV4NOByK3FlDkc=', '', 'Ratih Febrianti', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.163', '533d3447f26b2cf58e2fb4f839163c322b5bf9d53c161530767018bb8fafcc38a717df763adcb9db9fea3da588afe8ad65dbad6f3f488869d1e0bb89f95fa4f49wwHic+EGMdM3C6LLpqGQNnlm9sRqmqJlJMzNdFk/eY=', '', 'Reka Resti Fauzia', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.164', '8a0329bf5d47d6221be4a48e29b237378ac548822a7e551947c3e8f292b0fbc1582bbd77cc1a37cf3b415f529e1844ff4a4d9a487f53664ad83458e2ae52b1c5xKmqv4MyRfl+9jlMA6Tyok1h0MEJZP0bFvLKaUjvVmk=', '', 'Rella Leerais', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.165', '589b5d206585cf9d2ef89569972fba6501defd5536b6aef0eacbf7d29151f75f9c2e5a78203907f0548a4c794122414e9a4a3fe1c9b0b90606033a5c9a10e2a2GLd5GFnNhDWPeLh8lU9buwkaVwxyG7HS8PR7Gf2i4K4=', '', 'Reni Hayatun Nufus Surya', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.166', '92dfdac013858f71449aee3464b9ec3a31cc4e9bf03b3f026644d5d45a90b9550cd071835439d452dea1cec07c012eb70292c1072cfb332db6bbf0b7f48f8d44+8fHWJG0KqBUavv1mDRFM1GCPHWQZW0tDPBspGkcjOc=', '', 'Rica Rahmawati', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.167', '1e748a5c5af4687c8cd611f2484138eb2ea5dbd01d152e6ed7be325e5886653a3182b9eb87b3e267de5b2abb53c9b9c2d3172e783c0d0ba4f630116ae7ae6fcd7bQqwF48V1hAbl80FZ67OInNnhr5ikR3GtTLzxN23/Q=', '', 'Ridha Triningtyas', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.168', '293cbf2647e93d0c47b7e6e4db0bad52c9da0bf81d5c5f3fa75dc6bf6b9f947d7c2198c38aad611cb3f35929bd4959e89ca0c4bcaf15c26063349e124a1cd8dbYnHcuE6j4S4XPZFSAsjEJHT/c/L2fjhDe5RkcDugObY=', '', 'Ridwan Maulana', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.169', 'ec64e1af7db7fc3b817d31faea5a0b0d57bb84d5e7c96b5d9303663352dfd351f2af9d96186ae34b5a35e672b860e73e28c611e043200803f8b8ca079eb70827rG7vMj2lJnLcZ7Xl4IfoeBU8CCP8lKEcrVcPh+Zj3KU=', '', 'Rieska Alya A', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.170', 'a649fef96891f797c4d7ce970ca4b307aa646fe4b8f87e263c56ba671bfe620713e6a215486a398e942061d5a1873f9460e838c7911c68caa0c3a875b462efd3tsgXmwlHkwcFMKUrVx2S6q2Ue+I41cqZLhLkw738um4=', '', 'Riki Permadi', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.171', '7f17a8b25d80c87c14fab42e071509fbf9df8f7e5a36fa461385ae09b6116789fb8adfbdddadc7f55a0027ad32c5f7d777b105e49848d14917102c61bfd940f7iO8FpXXcrOjQeEzQ7poOEMqqwNxmmtQbDF3EABpioWg=', '', 'Rima Yuliani Hidayat', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.172', '9f5b58817a06aa5ac26200fc558ff4ab4c0677965528581ea4dcef17dc26a77f11fccc0415133e3808407578646dbbe24230d79607b54b521bd82ae7df4d9426Id3SQ0tEMlXrjUOqyhQZ1ezwE+a3HwIPvMqxX8I6NoQ=', '', 'Rina Rahayu', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.173', '3623c03e8eac95f999dbddcfddf55438ada6f4de34f2bd2e56ce916b781d43e7b1b1d999724199cf0e52e05e2b935ee0566fd1a5b99a7571c8db71a6b8342e58Xl4KX+ztanT4qIo4eV2FBh9hG91dlQwT5ga3Wpih5Sg=', '', 'Rio Nugraha Jaya Saputra', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.174', '19510587ff669f0cb0804b7e744e939063cf0bc0f8885069966054d7e621ad8014d45ed085cb7005349a3f8cab92d52c3ef83551a1b84e077bf3af57546680beltkfEdXKkUFuG++IXL80aO89wL5/ekORhe/iDqyKV3c=', '', 'Rio Prasetio', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.175', '01ef2756866d27740590c14bedfabeec4270ac73a18d8523d6e764f1f9899b58393f883eebecf25fd147cf35f275059bce8803e81a073b7ef3a88b4edec91dd37fXMQQ/Ox48nwgD3OjCWwjLYYZUPEUa8HW18LQR0fec=', '', 'Risa Delia Putri', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.176', '72b6c073c237cbfd8c536eadef70fe7e5c844599795b651b0a3250884fbb118a7edbe904e2fcd557997d5328750b835949fce1b41030be228a5b6776ab478dd5oYkfOEsuWOJg8rXLoS6uZaAgVFi59+YEATouB/Ci+DU=', '', 'Rival Viana', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.177', 'da06f61b75e680492a0cda7bf4f19e2796ea53af3a564dc01dff98ca0aa813e9217f01831bbde23d0c9e4f5d8b2604624e4d54e0e01b5e57d811c52bf88201627G+UGTHLmwaX9zXTUlsdU6lVAghxtcqxg5my+U3Cjw4=', '', 'Rizka Hidayatusolihah', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.178', 'ea9bbdfb1875773346eb700c98d8ccd7b5bf317dd891adac8ae07625b79798debfdec80853656bf662f1d572862aea38b31d5e262a7d32435857f865bd7f3431QKVyYFUX5t1EkPvI0+axeQ3bdS9bHOoUN2mEyd1QMwk=', '', 'Rizka Indriani', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.179', '3ab55d8191eca514b3aea6b13897a85cccfdf95da4509257a7a88a8cc3ed0a7e0a5ae9ec082b01e69b8846e816ee89179695658ecf9671466d53643842d66d39jU1ZblOhWIC+3Wmum2e75woQkx44xIuc6EvOm7RJtt0=', '', 'Rizki Herviana', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.180', '6de2bad2340713e52f1a437eaac0f0df4373b78cccf789bee214e718ac5fc64e985e3104035b3fe4d344aadadc398ee034a62d3836edfb73bf1f1e62c4fb57d5WrtuN4rOjyz3ntMcKWsbtbMbD6SW21xDoLqanRnx89M=', '', 'Rizkia Affiah', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.181', 'b3a6d31fa64eb5bae42e9edf7cb494cd627a57e9cefb85a43c668faa47e9215d782d28128e25bcdc31cf512f6427b06a43885c51a1b8c33abeca2535f721fb10fk9qMB/NOj10Nwx7pGHRTtOzSee1La92q4NgDoWx4w4=', '', 'Rusli Gustiana', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.182', 'f3cc4461be0d79cb734045e37438d900682dc714f83a5261bed091635043b260e535e032498b96966c91dbbc2348aed220b3d8da8ff9fd24f68648697c7cea2ef1YF9l0UnnJnKcsW6dpES/MGhUd6g9zuI3LoMBb+MAc=', '', 'Ryanti Nilam Siadari', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.183', '60808d102a3b71bb8c4cbb829310bcb79ac43ff378c7bed619ab152c1ae3e8c4d76187586509b4b8d1c58b1a68a1471e98d57c578d63971e1eba04ddd45f73f06b1u2f4mZU/Jb+MTRDNxWrbf6HE9AqNWVw65aW7JCNE=', '', 'Sakti Saputra', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.184', '3c797bb41f7db34d874680be744e6ff0a07511f9f84605cc574072814923872c2f3954af0d6d58133028da96508f08eec0509c8cbf1313f4c80c954e46252847CmKHqnryy6Z/gxDBg+9RTpN0Scj0hWBB9ZrTzAJb2aw=', '', 'Salsabila Syifa', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.185', '3a967174b3c1cac6e993c7216d4e3d5290b69c984914ace8da048542fe93fb8e61a01b3980d302f6c462139dfbb4ee7fbd00b577a41ba05208cbb3f5407f4b5dYB/ZySYyXq9enYGsQaXvoun7W0jFFg2/5v8FVPEMtbk=', '', 'Sandi Ginanjar', '', '', '', '', '0000-00-00', '', '', '', '', '', '');
INSERT INTO `tb_anggota` (`NTA`, `password`, `NIM`, `nama`, `jenis_kelamin`, `email`, `agama`, `tempat_lahir`, `tanggal_lahir`, `kontak`, `kontak_lain`, `alamat`, `sosial_media`, `jurusan`, `foto`) VALUES
('252.10.186', '76ee2acf5e63474b6685129730a259a1ae1dbd112988eae52f405e4610df61cb1beee438a73fcf05a75e9d33c57fe7bbf82c14c7f6ce3d9489d21c8fe9203b90ib1qi5/iaTy5Ar2MdBSQfKM1ieyZ+J5tO8iJtPXHeD8=', '', 'Sarah Agnelia Indriani', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.187', 'cb0badc7c4b13a15f44320c0b6f3b2b111a52bf0a833f4ed34262d034248cf6330493cefee30bbe097ebcc12a70de4c6f911294c6ff8439737563f4b26426511btj8F9TpiozsIdxo4icyyjk6y6uqcADfWwai4g/pKRc=', '', 'Sarah Damanik', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.188', 'e91eee09b292dc1a545016abdd10ca37fa47947b39fd447b0cfa98fb229d25be186367cd527dcb1c1e1b4f461d486fa0fa50139eeeee69a0bf8db79ebff1d453zNR3bc1FXVmxRAN/fi+FdoIuq1MJzK/RrTAOdF29CnA=', '', 'Satriyo Hutama Putra', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.189', '967339298f2f49c78aa50e0a603a33c99e9bebf04efd7b4d2073d922cc88f1e87c64984ccd4306178307cb4e5088246dfea086ddbb7f3fa62b2dd45549fd55a7/cmQUXE4DEdEyKEBs8oBtTdEJTqJa2nULyqdJxm/wvY=', '', 'Sekar Ayu Aprilyani', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.190', 'b649376634ade61f679b15f634708606e280ed76f35d0525d190b47509adfc90231b76a9a800d63dc6a19b20ed1bab720516f98e7f60a183ae7213488b93d63fgzQLXiAMd6BxNt+ZXCoKVT4RNawJrCgjrIpDcPAHnXA=', '', 'Sela Herdiyanti', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.191', 'e93e2c0833f7fe190f59dad36efcd2af8c9c4fa2f697e7f5bb6ef3ca64648664c87f3b110a3e9861e28874049ada5b8a7b5b4e06815778eca2dded70a56c2a71fdWmv+3KajsoQHuK9ANdl/2Ul7+puMwejpIhLcjcesg=', '', 'Senia Sucipratiwi', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.192', 'cd97159d6db959fed5f8f8783c93286c6d292c08364022d824aeb0ebe19fc7035eb34204b991a9b8d08f3f0e8f1ce0e2633bb8f63b8adc858aa3bd9409ae5308xSqSngptmSi6vncmGs1YNDdqDumHsY7YApYW5AC4QTY=', '', 'Septiana Lestari Harahap', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.193', '09008e466439b90503152984a60dbcf48227c3c1986c1aea8256eba8f6cda074a2269c079f237769aa29895c19d22e96a999e12cd087a49419e473927a3aeb73L9uep2SYoaWabzgvNcKbv/iUw5mVHy2UxkCWvG3Ao+s=', '', 'Septiana Nur Saprida', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.194', 'da5499d1942d1f74093fcaef46375529ed215c2eccfac9dace3b57bae7a4a3544d442d921a763d851bab2bb66c1e9088251fd54ad42b4d6cfa55047eb72052c2dAwZ4sfDlcPc81F1I0qJ124Q6/33WZCpLwpZUNKb4Ys=', '', 'Sesilia Sastriani Laoli', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.195', 'bd7a5c4d00d2051d50576b21aa0dc90f9693e4b1135374262bc4a0b7b6f3a037fa9199f1eb1727b47053cd966744917c36e7d58d63bfad4daab68811567595b8HjusXqawEIth47NMWe3/D7/6IuEz+xrmSRYyOkeO4L0=', '', 'Shafaa Marwah Ghaisani', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.196', 'aac282a53c761a35e162454af0be2270d9063f4f0272ddecd4ea93c229ea4f0669375a907e8f20ce0a685a43f4bd6b00125b2ebc9cd2c91e7fdb0a2f266db3c15Q21aG3JnShWNFcLGqEDNUSElIYFBQxduf2SAnKZIHE=', '', 'Shafira Andriani', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.197', 'dc0dc7db2e492ee3b035ccb637d5d84a556336fadce20b7b29c42a3e4ebf1c4e3c25acad94181ba403b1587e0e26193009698fda7cd2632d1024d7585452b76cZrH5Xer4/0duqGR+Ec5RfooAAR2WFgQQZ8l8CMVWWKM=', '', 'Shakira Ayu Lestari Taman', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.198', '4585cace54430b6a30bf0c674cd0f7dad754ddd0a92f08927338e0247f6dffb7cfe0ca0dfe88da2d7a9037fc9f3c898df6dc66560d0cc5affbe9a35382b30b59YUPg9tupIwPJpbevlYer2+OLFsuiy/gj4vwloUjWgcs=', '', 'Shanti Oktaviani', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.199', 'f9afdbf0106c2458acb17782feebc3bcf0a0330f6c5fa1367f56bde3c4b4ee9e908f86c7da9a7fcef8bfab52f20662b60289516089061b5cbf164066f51932f7sGDBMAK8uVtfMQwjWmtnaVOdviP3/w8dxDztL3+sIyU=', '', 'Shavira Welhelmina A', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.200', '110227c5464da68c48306bf4adb54aaa33eb26c65f0a6466b7e0a3b1787be8c4eecb827d1818ac531d6be55e71ee68f8c4f657934e1683c83e64c70a7bfa4e91fkiHKBok+Gx8elZUOEmTUiMrXxcvsA9OGNETy95ynsw=', '', 'Shella Fitrias Putri', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.201', 'e5e48d0b4f9c99a63ad708357aff5ba85f2d5188d58f86650bf4cd29fd33839fe0de4a60c8e8786463349fac09e433883660b44aa05c880de5119ecd9b03aa32v0YW+lIXBoAnBB6lpbhjfQcLB2OVzOKTqDT3NdrTbIE=', '', 'Shindy Ivvo Claodya Febri', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.202', '87fa0253e57bb204c9ed443a9a2bcde6aeab076b48d8013a94c898f03692a8b4dc0726d052a2529e22428e80703e4b7de86ec621e353300d47398ada45de5edfolOtg8fpTGmksHgvDlNYE9IvSVnmM1JCtzwyaglKWLs=', '', 'Shinta Oktaviana', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.203', 'c1e0c4758bcee2d907c68fcff6fa09a35f46202ae0c12dcd4e0b0af1745c163eb4b219e927559c0c351dcec77d04d8ff5d18b49c0b13f6bc9a3e75190bfd62b65jv1/ZUO35HcJvlfmmMsi0105FpgaZwT5s9qrG67YXA=', '', 'Siaga Parlindungan Harahap', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.204', '48a6b6a1cfb7bab5c0d79e0c92b2b7a80be64f9c0fcc592cd1ae726b1c37c25b1fc8138c31a3d2e0c7bac99e0fe0e6180d2a71e0ddc8bdc4451497926ca7ce05mIkvXqC4vZZvV/IKIVBROEre8LDTZZi0Od8S02WlLkg=', '', 'Sintya Fitri Wahyuni', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.205', 'd072f13e87d50b01e2ab275144c68aa160d18141ee8056348942f3800f4e8cf9e5d713cf651bc167f13ba36f6bc1ab28c50977064dc3e52a25ecd2715a1ff1031dYKXWQ9uDuwj4jrIpclaPUi5RhMl8ej5MOtTJ/1700=', '', 'Sisci Andriyani', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.206', '9a049b29815ed07b5bc52e11329228d452185473e35431f586245bc7a0559b23b40ddc3e6a5bd902e382c249cc9857eee76f4ce8e89c5c26fc55ea204eaeafdf0OD96d/7zg/w3JEwsKf8TVXpgi6DFU5RT7PoQi1aSOg=', '', 'Sissy Octavia Shelyna', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.207', '37accb81972d6fd1db85132bc8372bf832764954468a100ec3f6d79706a2784c9d7cd92e1ca1253c1bff164fea0d07e0072a5416dec3472a1037723ff9d8d81bqheULgxi430V3AlsQ03WApU1ZzrecR0WrqFSPgrRWqA=', '', 'Siti Halimah', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.208', '99f4f92d9c067332f9e340964bea8e4f6beaa098c104db94c78c96018fdf35ca37f79f19152bcb748c4a81cc0aace177337bee0d89140570007b1ebfa99f5a39JLYCFRKQ+JygNFXeBlPyiZ1GFkHIpXRtNuI3mdZ83tA=', '', 'Siti Juniyanti', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.209', '9637ab109e07f9b22c8974584e29f98afabbafe0d4c1221a294b5f1dddcef0d5e03b81108a613ee3280c9f516f0789eef2d46fb3800260018f24abf5dcb2b863jpk8TfaNo/n5Mumk/qPwPtCxsiqLB/SuQU1+HC6R8d8=', '', 'Siti Oktaviani Nursyamsyiah', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.210', '959ee468be75d19620836d020ac3afd93d2fd437c269457ac41331105793dab6bba7020fe49ff1fcec547ea3f4a2f220a8e7beafe417ea9fa0723ab5e24c4cacEtitgKImCDd9Oo84gC++EM/zDNxiKDD/CNW27DYYtmM=', '', 'Sonia Rahayu Islamiaty', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.211', '5f643077e4a5961eec887d2c8f909c51468ff3c1a008aa2c0a6e6957f1448f694cda328e02cadb8805380da1268984db8d20bb74857815b5404f2460b8b8cb43cgMJs9y62Q3oDiLwdOS2Aio//TGVNeYIYd7HZJyZ5lA=', '', 'Suci Rahayu Rahmatillah', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.212', 'a186c56ea12af8f2a5291e298ee5a3a882af728cf41e7b238ab9599dee51259b4aa82d337e9e4dfc27461ffe96522192438b25d6cb408159843cb5d11378d566bQTq6f3uciKC9zdM6+bVbzozMIQhcNGlVziWHl1a8lc=', '', 'Suhendar M', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.213', '1a0268badb1c352a25226b2a48d65931a5423f2c4abc4302d854f1fac28effb678d57fc5eebc0987ec2870b848c3dca81e2e93bff26accdc7c9c580dae6ac405kRlRl7U4PQxCKcSLCel2hJ0v4iui04lbx3QBHFwySOg=', '', 'Syifa Anida Sholihat', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.214', '8bcf432661594c4424465c3ba0d874470f1f8aca307b11e8e3bacdfa5c890c0ee984b2d893558722e64ab231508dfb40b5a37c43c2e3cf8830a2e3aa712eea74V9AGG/RKDkuYyBymYHVb43F0y2i/Gcg9eXYTN/T8GlQ=', '', 'Teguh Bayu Erlangga', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.215', '6e31c5a8c89cd6789e65dfef5fde235916439d5b6e1e9761c24aed9d6dd3213aa262a231c6447e477c7c1db48bc48a6d2dcb02e333578e45365acaae4e0dc41ehFRxwJZp61NQpDmDYi8moq4VUBs75A6jYTHzhN946Hc=', '', 'Tika cyntia', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.216', 'feaf430a410eb7a744fe7baef8444f84110ae120817a6dd1a97db5c808024327c423169963b797efb49dcecc7fa81f856f59082426486b87919739270b200f878gjBPysEhXttaojH94qz/ivlFmamtxO5Qqr97AghhX8=', '', 'Verrel Davendra Praditya', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.217', '2bdeafec033ea4cc64e548e8dd0db85fc1226ba8bd2924a9370f16109e801c8867b6a5d4bd881cfea340759bbc2a73145fb66ca487029749436a5b161e9ccf14BUDPuZCuT5+I2DkswlMF3gOgyq6L07JybMaUIZO3P98=', '', 'Vinny Sakinah An\'nur', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.218', '507d2fddd231a3b66209238441237f9430e14410a22d72669d1cd56e06e2372687c433052174479bdfc3d3dfbc664943e7e6fc65ff59a99689c4b5d0bf5757eeI+8eLkYwdXzyr6J6o2+9S4kF10ecCEbnmyeUThH9NV0=', '', 'Vira Mutiara Subekti', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.219', '75bfd91706d66dbe6f2a65a74427d61d13e6aed92a5976454f623cc930e9fa5ba8346af157e2965a8a9dd6339afe4467e2affce330e7b9cd9e676bdce1bc4d1cr3BatU19EtOnp65uEBpEb/7f10CK1/NUmG4J8Vdtp4I=', '', 'Wanda Adiati', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.220', 'c09da7f7990e3cc4169ea007a4249733a1345e44d87308ed565e6d9ae66d8abd1a0c9fe71e89c492610cee04143e504cb10b6edad3ad364aace7f6f8f3ba3088G7v5OiHXBagqeOgJVc6aEv4c70qT6o6AFLhrj2IztsI=', '', 'Wandar', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.221', '8c1a4da3e0e5e1c85180267cdfdf7f139cdb91bca66001b819a188332569c2d40c8e63441ad9f7929a46ffa9e2027c91caecdf3bdb66cc5144a01124e451360fFPVYsJ12l16AgKFja0iQjxhUgIcynUCTaYozPFFqakA=', '', 'Wilky Lorenca', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.222', '1efc33e9f5aaa9b80f74490936723648e8f6d571eb30c7322ea75271626a6ceac42cf8bd1c255428f0ded874d8c47da4d8f4b068aac9b29f1f75ec5504adb9a30iLCv9hH/SbX+L3ER4fnqScZCzU/3pcxYVFVIYpUKjU=', '', 'Wulan Sukmawati', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.223', 'bff481f024a477c8654713712f8ac50de02480098f5a5e5a1899798faa571c78b32ef24403831b5e2e89fed0d0d8e37f135fd725590a475914a07a9db62404abTy40aVeTP6qY9jfwDOpdN5WXTIxgz9lKeuZI0CjffiM=', '', 'Yulia Nuraini', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.224', 'f55cecf39509157644d0371136d3a5617595f6dab98377eb0be498568f0bd0e882fe92edd0cdbb43ca46573e50a5e5ac66e70b4cb41f09ac02781c447eabd9b3Y5OOkwlHjT8/U8Unl1VfwJZrwDWist8cuny4XTmrNJE=', '', 'Yuliana Eka Purnama Sari', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.225', 'a885612cbc7ac14423ce85c614d9bddd9230dc25623a5e41f0c00745ebd7d211b3c6491b054ada670e3bc4c03d5b243b2d72b280fe7ab278f281c7dbe8fcfc8dqkGj3id0zds3RqnQHMVWlOIbZKetH0Ob3VkiE0XDujQ=', '', 'Yuliani Dwi Purnama Sari', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.226', '6b0f3305d73106177dfae22a9b38be9eac219e54892a2ac5af5a0937f526691e6b5c8ae5cec1b093d7927d0b671a3f0768dca2e0e008484400da2785e8a63259HbCLPyuAWwv6JRwXcyPoRCNm3Tjhoa0vjRtphnJMguY=', '', 'Yuliawati ST N', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.227', '8ddf76f7a5ebb1aac829822cac462ed914eedcf7f38dd3f73dfa4f59d323eb4892b71e67c512e64eb46f960c49fab3d34b2ec67bc9a51a99236f73c0ffdf4629NzXAxyzlPWl6JuJtwH5BfgKadaoMGiXxXBKltjVKHqY=', '', 'Yusep Sopyan A', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.228', '1cfbe3d98ceff451dc868d4b925521c106f04a3345521f8d221d1209b0c2e3297c88c9422fa4b7ed0f300ada0abb14ae80f0cfe334c65ee4ec92a0b33001d724AWmvS6vuSZsrUpPt4/XjahCqb4EdfEedyLxfga4clEI=', '', 'Yusilawati', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.229', '1849263f303e8010539cde76fe4a0c873ab468440490ef5a01444f50f2ca1be6aed04f76fd5e2833c51b9ce9fad2517a499fbd582e84cd465e66043c6566d6edOuKHSxIs0Sj3cxndK/5mDzRu0OsJ/BqPsvSDez5Fk70=', '', 'Zaky Muhammad Yopi Rusyana', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.10.330', '57ef8b0dece8a6a82b0fd6b10198c932820ba4188ef20411631b9dc3ad977c0a890125383ccecbf0c887ef8bb1d5da1e24235234c03b90cbdfb08ab1e978556b1AtfiizTYgMTZ1fI+FWkYX1Ly3n8hjLCx3KmmvSFTLw=', '', 'Zidan Supriadi', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.001', '064fcf18389149f1f72711db385a1853fc9ec2953025404862d51e5b69ba4ce5a6c1fa8b0018419c089f7db720c595678429b3d9bf858569989919860d83be84JJxNdaVDo1TNtUwv7KVuF20CFht06og+VBfKydLQgFc=', '-', 'Abeyola Palupi', '', '-', '-', '-', '0000-00-00', '-', '-', '-', '-', '-', '-'),
('252.11.002', '6863020765146bc4329d818e5da0770cc6a4178ce5ae27529541bfd1cc023d0d7787ece33fb5de496694a81a0d04438fefd6c1fd44dc8e24cb9ed7c5ad308cd2iBHoSn/V7eGWPrae43cNYy0uRJnIZhjdQ4yWQP9+SaY=', '', 'Afina Nuraini Nadhirah', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.003', 'e56c39d69b1a4ff8d3b6d94c7f8e1180bd1c268c0627d5e0d381d2071bc77d3957b942b8c1c7250e72f1985a97e2b72c90f72d2e2386e959bf14ee65d64793a8UWYofz0Ft1+E6sp1hUykOxW8w7/9ktPKwcJjIueown0=', '', 'Afiva Harsini Zahra', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.004', '407adef91b99aea96f9991b021f282e9b60b073cb37c01639151c22af369612c77a38927d8c7180ac5f3250f5b6249443e770d6902c7ad1b89f69f9a0d4b669bQBZq5rTFDKiCGWUk3KGIm1qERziX4oCKRbv4ZCJIZlM=', '', 'Agung Irawan', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.005', '34198d6304fa8e89ca4a6435277a3eb16e1ab1f832a3f88a956d1facc3cf519a25ceb4ee1f4fea0ba549d6b96051eff59bc669d7ba85cb410820a0fcc1edc88b8eJ5MGAWHQubBVubZR7WXBUA+2xYWFOU6hh3fj9tVmk=', '', 'Agung Setiawan', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.006', '2c72bf7ed6c4a885bfe8645141b64c51b8079827951feb3c4bf8096829f7f2a57fbd84adc11d8a550c0056314fcabb36dcafe96649cdcb3014219254761c340bxR0a8PTN5d6k4juNoHgVpT7xz91x7WbfFxqw7etoiOY=', '', 'Akhmad Irvan Attorik ', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.007', 'f31d87c19b344978659efe8d2ad1ddbf10b93e723252332986f796be186ee163a4c77e43006fa1bcac3907fb14240a22ff203e425c05caef5ba75259582480c2mLSsx5khqhO5pX3FEDpCLgbbbSZPsp+VLibljknVWpo=', '', 'Alda Marliana', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.008', '077b65ddcb242bb321b124c25fdbe1a0ad14326e384af4b0ca17d8e99c2be81e061ae830e3f64cca6a11d34a5743f5702c93b650c223515c85bc2e1ab8bbbc90sA7qfNssY+L0Un1fvtFmyZL7c78ZNLaoZfeFgSLhAnw=', '', 'Alfa Ginting', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.009', 'f69f79e9afc5ddf24167436adfb247c26ae690783cedcdc6a059dcaabfc1c004d80ce936a0d316988bebd9966c098ecc11dd98337d8b1254e65963db0f4c1f9288rZSHPvfKHiLzCyenrjx1XTqrK8bi3JHeNNZH6gxNQ=', '', 'Alifianda Rifqy Rahmawan', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.010', 'e64b34c0b623c4a96dd90ca50ff6c56929a9635ff4b5ce656ae17612dc3b0f5eb2664741aa8726af26a8bf88613fdbc81be97d39e3a32c3da3affc01033d60ffaDk5jL2I2YdAfvfCNyb1euo9HEBQ6Fkv279iv/R6WOA=', '', 'Alverina Susan', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.011', '632a5069232eb78a92013a1418218d498036e83a01348f05abcbf5957a3688fb9d1c34ebaffe78a24a464143b0924ad9b7b37cfa2ede392473ba7621bbaed153AkNH9RL4LylNROgETby5aS+awWWJseivhBGyDUFd/WU=', '', 'Alvin Lukman Nulhakim', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.012', '68ba0d10980f8acd1383dd7c2ab196f9f81d2e19c26f39ec3b16e2d3e4b27bd8fb58c1cd075cbb64bd30cdb2d74677044e1ee7e16153271f92b91fdd7ff1d3554r2ygEsIPNkWvY3vrjQ6aEeb8t+C3nK+odaKjEYSm1A=', '', 'Alvira Nurul Azmi', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.013', '8833be0f9ed6436c7220ccf8f2127456fffeeb30c588302ba36ae9bb5fa0589274594c46dd370106524d7ebafe3bccf9869b17a122e63682ea31733c90b04bb1mTCj+wyDWh91TgFn5N3bHeeHSwze4j7SbsRz7ti30cQ=', '', 'Ananda Alifka Ramadhan', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.014', '40c2d32fbe7a6b1f8cfd4adfe315a5700f9ee7577c24e7cac6c851bd0c7c067b7c34fc117f6069e54e9da5e8932d6833f03768c9f6f5924001b76f339e0350d4c8bQVZssVc75RPCoPdNeM/5EypauHt/48d62v/uJz8Y=', '', 'Andhika Putra Bagaskara', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.015', '2c6cf2fef61966f4f8a605bb262b3f4f977fa8aaf92b8e55a6146a56dcd6e2f4e07bafee2b4b1b6ae39c4d50dab1730ea08166bb1d80dd59248f33b533837d92ZS78WVxzFmNs/iwbGhbA2IxoaxOXzU8DAWFd3HQHZX8=', '', 'Andi Maryadi ', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.016', 'a6a7b9b649559dc5d657d8143409a8bd47e8aa6cc6a850d2d36e888552d9f1eff40d427f82ad4389d91e08f0b850a6eacc47351b537d6f9333986dd071a111934RXL40e3RYqtg+IaWcPD50ecIycX26G/W854Ezikj9c=', '', 'Andi Muhammad Ivan Muftiyazid', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.017', '969dc3996ef139226aa262c09c4ec8a89996dd9a31e208e8c3c40d1657442ebecdeada4817ca5b4658d9e5d97854195cd8b14b2cde66d201d422418b02f0ffe6t+e5hd4K1M4mIi/DqXcMx8G64Zr6r9GTReFDvNDCoqc=', '', 'Andreas Ikun', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.018', '37061fbb9c01ceeb069e7835fad2b80878dbd00830434c1f8c178358df82578b035a653f265da97638f7c793d992fcc525b8ddfd5a65c6d3c72c135d2c70b865Lk7hwun4j2FErVwNEJ1RYm2oN9ixGyThLBfzj7t9zdc=', '', 'Andy Satria Jatmoko', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.019', 'daa1134b7193d54c9c3a53e569edbb628882eeb37aca80ed7bbef5f427a9317367fb2fa64f7e0ac03bae249ce43c74e87330d8dddfb207fe856483bd51a6011cjvhhZMZMbcPvLHzZ7GLB6SnMvuNrfFBcfCQ8fR/MwWo=', '', 'Angga Fajar Pratama', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.020', '40067acfa6112c8426ed8be3e050b01cd4f763069ae431daa2652928f5681db741103f03624a538b4319dc77783ddaa72954380e281f9570383ba186b140c6f2zUonyw3QIDEhXPqGcCIsdjQ26NlcrZ19mTJp/bK/xiQ=', '', 'Anisya Wahyuningtyas', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.021', 'afd9e3b2064637197a8705c2662b62312289ced6e86443f51c8de7e1a441916db4e9ebd6493944ca35a9cb3bb50507add37f367d31259ed38d5ee21d529a3681bg3cmdV0jFWk4aDk8LalejhJsMo4rybaJ2usF1sxoZw=', '', 'Annisya Nurhidayah Futri', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.022', 'ee6474624f3497a500aaecf45e57eb5b05295fccf287ba37c0cfaf532806ce6b5bc5db419afb5e164ec11ea39e69e459694455b697073502b010c4e7d5325318gy0nGhdyN0EkU36vKARaPcsu7ycxMpjWh/ecvohL9q4=', '', 'Ari Fahmi Wisuda Pratama', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.023', '69dcd845514e726213bcc279ead1ed970e38993de213297dd46a63ae76803d33ded3a25f1280f86f6c184f87a7e4cb446e8319e3da55bb985fd4a3c71c06ada2m7TY6oU/vV5CxWzBDppUMuZZdqnddrcSI6qZzzSPN04=', '', 'Aria Pratomo', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.024', '9cf2cee824d44363f0fdeb029a0877a3b9c886f28e5e75ce2e09457e72085aff62bb5d05764ee1942f90fc050162b33981484eeedc73fc98f23754def976337cCr+FIie5M6tc4yBVQrRsdzSuz+eYrLWuXTijP6ro/TQ=', '', 'Arkani Adila', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.025', 'd751c9e64b2692a0bf237f066c200a086c0188b652085c172c42e414c46fc250c666e20ad665b73c79664c7cf8e57f2db02e12cdcaee022a4a338594977e4bdfZBGWuMyxn9MpLnhuqIAlRJLYqSnD1k5R396+CDa4Wj4=', '', 'Asfrila Dewi Firlandia', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.026', '74263a46cd9ef156862f854421cfb162f5e81f70d75700e72d178ace59e274f29efdcc913c6e2073b6d86af2dc06573b05a8684044ef2debd1de781a7622bc75WNWPtlCwukDibWauFMIXU4GiglOXTvg1INHSxSjlp4M=', '', 'Astika Fujianti Rahayu', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.027', '255b50ad87ad7a78b0cae0aa2f4c2059d82b9805a5d3db16734ab29665d425f92c119933b07e1c0d3761ccdf02b62aebccac0efed692e1b1b35bcbfb571da5a6/Kc80jHFCl6DS/WRE/cdaA74b1IIjXZuMYRRYnMriIA=', '', 'Ayun Nafi Yatalathof', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.028', '01e8153b289e59e9fbebdeaee8dfccc37f0d8b7e1db366db8eeabe00412cc16cd64e59b7a9c05b014d53d0fe7ab9c552671a02d26121dceb80d5006d0a176882zaMhcJxgsSJIR/+uQRg/xzM838HdVQgc3cDyqHjVY7M=', '', 'Bayu Satya Pratama', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.029', '99d3ebf9bf4cf64234c74a00b92c956010585b58716a6f64349c34558a09c226c08a1ffba58eeab3b0d06adc4a66c4e2f667667ea7c66c681e0992000c8690201X1NYQT6xyLGvQmuOnJnAiAM+nF1WnMNcmEhUfGGR4A=', '', 'Bima Sapta Kusuma', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.030', 'f03ea7b7fc1eaf5a3d06f90141fedf35bbc20d669d8212468864dd2d44743edf120ba098c90b039bcb24d81620a31ac5e1187626d90ec7238cdbcd282bbc6d9dNtT0PZv6SBLxIxFov5M3mxVi6M74zADjpx3S9rmDCos=', '', 'Bryan Jefindo Sihite', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.031', '30c24400bd9c01d2731961cc88de2f4e89777786d43ff99eedf3f06a1c51f7fccf59d91ad992edd9cd744df60d565ce2abe795a9b6073960a24ef6630acb878coZ6RwMcfg87d+8+0O+iSyhmtcIg19ZpRKGO63rIHhJ4=', '', 'Citra Laksmi Kusuma', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.032', '87b52b19f0816b2e9dfeff9c0abc3334ade419abc15347d4e765de8c3bc93041a4f3c6fc67f0f283ed2bb1931638bd53ff54694ec1d44aae22236341ed1d5b5alyZFkVUkHCJAYllXlPp++K6cW6wuAtqqwrAXpe69tWw=', '', 'Defiar Rizqi Ramadhan', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.033', '206b8dd6267ae243c707e93a622fd83fb39f8fb07ff74c925f15f456c2bf6ceb9c1bb712cc7aeace734d69e82bbf09be90bdf2381dff994f437de25f385f263bLX8YmYMe8KoDjqSHWL6Q6ybPB8ZfeqAEAkkQIsUGyuQ=', '', 'Dehan Alfania Mariam', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.034', '73425c91a68fe073cb5be94aff01cb7a3cd57da1186747ee945cdde18cb04f74aae2bfb8ebd5ef898e85258b356cbdca11093781e5fe0a425974ae4c73e50ebat2EGNx00SABrpEZ4p/tSSGyo51l4Rcd+Ms5+j+nMFjk=', '', 'Denia Shavira', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.035', '1c874852c3a1b6612d6781f7ba0f0b118c81fb097a6727b9a104899c8b357bc474b9a36e95ea283fe251e434e0569f38a002fc123345d5e79a1320237249808bn+2pibKMGsQkjiPCvdRIHCuFUGe1Tot/5ab0+pqu2wI=', '', 'Desi Vania', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.036', '0ca9303c59d9f3af82d12c08ae2e70b77919e49c4f6f73490bafa1e3da63816aecad4650901bbbe32d8a004b5efd508febdaaa0eb86dd2942c7a9c505e724d69bVhHf+/eREucYvHyqUspVL1avHI93RY3HbBkOM8MYqg=', '', 'Desta Anggariani', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.037', 'e089fc7a978c24bc7f2720e1c763d0dbb544a7f4a96098f86998f723672f4f1137a8a15e7b20de0f0a780e05626707fb90d17bb1117786a1276eb24c0e1daa8al9VrAXOs08SnaADcEUwtZNEFDXmmfXf/cymG4TOlCW4=', '', 'Destriani Fuji Hapsari', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.038', 'fdefeb57ab93ea4de6c31cbde05be585cad500fbad8250e05875c77f9d0e88498e04fc4d1783d7cdd6f9acaef50f2e23db2f77cddd06b93b6cc80a0e088ff857JKwOshd2NhpBmfD/THGR1x+BUcfQsCj7N48GMsEz3d4=', '', 'Deva Astrianti Rahayu', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.039', 'ac7458863414a4e3cbce99a815460063e5bf7935cbed0d7db768596c63cf6d039332fc14bbcd7574be74afe17c8d132ef75dc0e55a4d78b9df4334a368a59c3c3gTem8P1wxUJ5jvOVZBmsejvz/8tYT3JkCB9hBd8UeY=', '', 'Devan rasendriya pradana', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.040', 'b1a2e9bf26dceb99f6b751a8d0145104cab47e0a3b0ddd6cc5ad5377417b33f8f14cbfc33785df0189411dd9dc602831569e90ff9ed71a0a20289a1aafaf8c2e+nSwqs7m3Qa+ohUkoX00hW4x3hZBwUsF879God7cK1E=', '', 'Dewi Lestari H', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.041', '0603d3501d200e5aeaf03242f2fc54ce955887a2d30c987b658c0c40ac85848a5f47a2805a10339a2828786e816ffff9b140d03c976868fb3f0aec714b5c486bSHcA3dUzJkoIaFPHrjby9Ag6dNCLPO4gyYEnzgj6q7I=', '', 'Dewi Mulyati', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.042', '10791ac38ac5736debcb83d43af3ebb7ab8328fdbe4f41ac3376690b902b09e61a04baa776e3d1b007b0c72d78e9f2bf6bd8fa58145a573fef232cef06aad6c7DssJS/YZDdOTNkbxx0vV4OaV64X8j2uykVchbLaUKN8=', '', 'Dewi Sartika', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.043', '3c00d472828a51a93920bafdd6841af942400e68edc95e25937dae3a74058ecc2d86a106e7fe4ea100fef080760411dfa7fbf8848fc6ef316abb631e9fe2fbdfw6Vbtp5484S/qzFqtiJ0FnU8HsW20xLfIbjZSPMuGY0=', '', 'Dimas Aditya Nugraha', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.044', '16193a2332159894bdf571abece9e74cf1e2f0d6665584f0cd317c6bf545f823f9ab5244a78586b10c6bb607adf0e75cb29bae66dd4887ac47987413ae911d27U+TFPUUgFHyX6s0cLgdbw6FhqjRb6m7Xi2laZ3Asjb8=', '', 'Dinan Syaeful U', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.045', 'a5f30aa7473ea7333c921e25fcfa104877e4b9214ae0a453474672552c12f5f6871d871034215f6f6f7d30bbfe5c8423d592a1285ad69598e3d67f4c3b169c28Fe8+b9ZtxdGQhwWP7nmDmO5fAc7Tp29w/kl1Ln6odDY=', '', 'Dio Ilham D', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.046', '70bb69b7750062a7d397db11ce0beb6cc7810e2f355c860fdd99b0bd44fbf1ec55c605cfe5e353d008e624622c44135a49e1811ef1bf1add63a9b79d10555960houuDIXff0FfVH62JhArxUW5N1ZgpDXK8m0n2RnoyWg=', '', 'Dwi Lestari', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.047', '7d2dd2b723d4d59a5a6a2217766011531e1219db75561971e89f143e1fdd44a17cb6055462bcb8123f996e7b63cada2ea1874993eb0243e29fa6c18e6e97e0abkYz2M6G79YhERsIt1VBSQuTeL5mGqYsVlBtf5MqgeN8=', '', 'Dzaariya Al Bayyinah', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.048', '7c5d28ffd3d0ede7f6a3ae242a65b480dbea4f541625e0118783c54956a0f963ca2418b7f63fd657e3330f3e016109d059debed87ac4e9c1bb66f841e02db9fbI83cMlxxMQ2gNcPqEMrt5vnILYVACWh00hZk0jO2VOg=', '', 'Elsa Savitri', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.049', 'a9f13a96415c07f7797885b8c1db02709448052ca6d8d0ee8cf00546800ea8823bcd181a3a390550460a7dbf24768800e451068d716b7f12838d21d93c6a5c19pRC36QBj9v97mh6AOqLXi0IqTggmGbtYxQvGFQr5cLM=', '', 'Erma Damayanti', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.050', '9d144d94ce2c8d123ea65a3ef1ca38113267d42f70079bbfda6344f1ab96811f64060b0f1c732b702630087e8990dd90c59e7354c539869e14abffc124e69c93MBnDxhE0lX86mW/lXdrwWETrivK2H6NTE1xOd/tTts0=', '', 'Erni Yuliani', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.051', '62e4c7478b83c3d266eaf14a768dc4a4f6d1b3523f46c4db1b0ddf253c25fe72ae0dd9fe480fc846a500ee5abc127f5e62591f700e6a347ba7ef843936e72bd4ieFSEEY7BZUp28SWTwwaBIcinw0dCGjFpWgtPzouqyQ=', '', 'Fahmi Salim', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.052', 'ff95113d9b68ec3bdf4cc176b6eb0734d2889e032a1d32e7f5d02cb93f63024e2cc03178811632860ee444a7ed69c3007d55985e6d5f2ba987657dac78b3b099AFSvuni7cNatVNQY0yU3ftQKMbiaVrGdwCDqD4QTG/4=', '', 'Fajar Ramadhan', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.053', 'b611d372fadd1907dab45250c33ac408f1aafecdbb9c7f359bab691bf88816934613034cc0d0945583675913747fe010ff78b3479c116daea00f85e5a5999079r8ip+ayRzfQgOwOfHEfmnqALK1MLnXNd2iYGpgCYkQg=', '', 'Fathur Rahman F', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.054', '92d41f3bf86a3cb6f75d8a1a6ff810b1d9e4603522b0e384e4b653b16da8b8198194c9120675050db8b9287b345fd4fec027971fdf289b7ddf9c69622b690fd9S3zx0VAF/hKo2jzfX4aATX2E0felH6BDDCM+Hx+ICLw=', '', 'Febrizia Ananda Putri', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.055', 'a35ba4d92ff8ea9fbd29ab4f32dc5b8e4b8fc41b94124b440ed6fdd1828d18d12008ece3bc34c88a3e9999e52fa1ed9d534801ff8e701e0a30490b899d1e47bbGOuCI0qUbzTQt0f9tiSNUcTKtMutANT2aMa5mEeOQCc=', '', 'Ferel Alvian', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.056', '32b85aedee0ac9f6f6ca275b56b27c8f84154bfaaff1ab5a95f052b426d34b613c149412bdad223d5ac910cffedd71e2e2285af16b5aeac9f9fdef4d7d231390ijMIiHOedkjTCJtCGYxyulUGJe7OPBDC9E+lNNhX3rY=', '', 'Firda Yustika', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.057', '14cf0cb3dccbcb29ac9df919bae883e2d8452441f151c1176c2faa869ce37a06212086fa711ee8f6ea2cbdce33c352db2ffeef6f9750f3ab3913cbafbb0762cb6fFCb+yswViU7uXJ+slOcJd7Yq1DqnO6PqcHYdimBo4=', '', 'Firman Mauludin', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.058', 'acf1b74c490552b968fb447b35a69035f49807daf525012acfd03c52b99348b6d23a5317bbfa65867bc4ba6b1096c640a10b2dc523aafd86d4744ff6ed6bcb4947R+S4aFd7gp1lOHxnJdSJQ7vRtsq3sgfsjW+oBxuLo=', '', 'Fitri Damayanti', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.059', '277c365230c207fbc79721a1bae8f3b14c81698a3dbddc13ad919663eddc59e1ec845d005312c46133d00441676650f296f6b071c1b3b7dad50274a3c631f62cSjSj3LjcfxTIuKhRIgMG45HPqtQFTBStC0t9HxMTyMM=', '', 'Gilang Ramadhani', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.060', '75b94382826cf3b77c168f4d659eb826bb49b46925e4179baa59872d276059ae4885ebd500475a3ede789f159b4be772dc59f02115668de14a1a6670502b1f7es1wXX7xvosNb/dsVAqu+ke4a/3TFEkWENGtXR86VJTc=', '', 'Gisca Liztresna Gheavilda', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.061', '700b9c9ee2b03a20b604ff016a03e417d99f8d818b48b297fa3b9b9baa473b444f3fa4f0d1f0f54e3ace6be3760490d48bef6782837032f679599c3f3c8e3eb7AAl0psZkRQV4X+rxTmT0VxXYhRmYsNIre2zMgGfPhKQ=', '', 'Gracia Sausan Shafa', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.062', 'f52a81ab57eaa76d525a7e9d2d1c6bfb3a9145997f41666cbdd1f4dd997fffc273f30bc0d5658e9fec811df87d4570cac0779b8332b9b57d5f77e7da2c3e4549Q3vWNVgoG7pk0a+e/9fL6mKBGh0FfCBeOXrG9dKeqxE=', '', 'Gustur Nurmansyah ', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.063', '729b25caa43c3ec2a0e5de780c16e0ef3f6a5051490fe95e5fbf9a4c07b80072207552fdbcd2f055ac027add4db10fb833baca09bfaad3b17346ee7b0632fda53eIJxBhiHPILYWSQfNkUq2giTDgLI4lYDv03mOD3WhE=', '', 'Hadad Fatur Maula', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.064', '8f5fa0115fd37c1d0f5dc583dcd3e0eb0425819b341f9cdeee21df257942a11b4bf0ce7218be7be03cd72216418c18c1c04013bc530452ee364e631085739982hMlIPNZt3ov8Z+WaEgncjG0mgtKJ6mgsoaLL6IJ0T3E=', '', 'Hana Fitri Anggraeni', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.065', '012c3be8dfcc7416eba8a20fc33275470192afc1cea26d3d6d2f97137fb1aa861e59562142b70e2388c40a9b1210a2ed20eb2630e9e237857541e9d60e3c4cfen4EdolCF+v0OsR5UKuwaySVp/BGZBRMSUABXi2Ef1SE=', '', 'Hanifatur Rafidah', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.066', '1c975aa5c5f12a47fd02dd8819f1a2b927f2063b000ed99b96f7235067ff6683df42c227a167a316d39100f84e17d94f669824b68723af51a91ef93c2db6fdf1t2CYxMvufrZwS+OJ7YSOjKxcADrqEHHFf0/Q9TlCoLQ=', '', 'Hilman Fahmy', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.067', 'a708fa8f7ce63a65edbd762558a909569076b56fae15eb0ae27aab70fd8d066815217594b9392fdc443fec35d45723c82045ba60c87227566ada886b0724f416MI51xxgQEJJiHCgiHbNxi0DDhbu0tgCHEnr3J4RdTKE=', '', 'Ikbal Padilah', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.068', '1a6e97fad5ce3e69e3ca5207790ba5d86116e49a2b666927a9e66def8d5f0b0d1b99ecd14a45f62fec88940a603a3b4ba9e4d5c0b6badefb7756e1e3280dde6b1ljmF3DVW8JGCKDyCcLlYlTgAmchDQ6w5iQQ5Nrzg30=', '', 'Imas Widianingsih', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.069', '39cfed5daa8b968074c5cd8e38fbc401e0f3f22aa10bdf0d5ec778a5f08df86746f5755035759c08d8fc44d5906a19b85e6dcf319520602610841d6c5d1e80d0SOA5zGPv/XgTf6qqihTSYC5UudcdbT3eBfJ5fdjDZm8=', '', 'Indri Yuliyanti', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.070', 'a10385602d64f09c1efb880c92a6e13cf7fcb26de6e0b74d677f1e8b4388b9bc7e49a878dd00c939de0b0ca4f59e2ad5ec272fe091f823b44e7e4a39963ff920EZZDttaCdvk+hEMZHp0iColI/GkkfC45TaRXoGgzuLY=', '', 'Irene Elizabeth Wessels', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.071', '7595a13eb61aafb4e905d455ae4e46f58d2d852c45ae02d95ed991ece317949c6d4ac97d8ab9ea0391370e2bb514657bf2f1fee63925b46122ed60a2d7e17d0ct3rvBEa+hkLTV2qDNvTZTCzf9z4TtWqBt+8cW/5imB8=', '', 'Irma Nurlianti', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.072', '9ade8f1812c97b204f4dc7a44323b48d7ada417164dcf6ccf94c9a5f000e6ebbed6ed765182b7e870f88c89f95a76ad77cf72ec325aed66e7e0eb944ded3b016cMIC78eFRAbdCCRNnwXLxEH8bpzdAgg7A2UfQzgOL+o=', '', 'J. Boy Darmawan', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.073', 'e5ab1c49f188cb1107ef00792bc1ee2e5c229567d6699e593fd02a151f01c542741b94074a7863e87290136a9c540faf53e10e58c9592f8f91ee555c7c69f8195mhYmIlKHAb12ucQXJchvlbufzVoqDqg23uism6ZJO4=', '', 'Jeremia welly juniro', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.074', 'f13eb9a0b63348d075a107c1ab8474e9db260e3688e3e78ba95a182af2e6fd47cf4cdd49617bbc4aae5cbc1aec5cad12748ed3c61bcac655d92aa8eba6ce85f6KILOfYjVUXuos1N3BFqcHTCKwhwOzvzVtvgCT65XSMc=', '', 'Josua Situmorang', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.075', 'f9fb2a09af8ebb688c8b789c741e3660ff7ced5beb0c5f4523220e59c4e95caf6c0bd28919c735e812a79b614f43fca2028871742e789b21d2b59113b088215bQ36Gtp8ke9TtjHsxJLhFHxaytEd+wycxgCdbKqs5Q3A=', '', 'Juryati zamzami', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.076', '1a1e423b864e925760fb7250a95ad63bc8d34881b1de0413cd4a16bfb098954ed40d485382e42b20bb2cbf687fc13ef4bbfea9822ec6ab34d87135c9b351e50eFcUguysoX3GsJZiVtSGtAIIAd7cfX/S2PtWyXB7EvYI=', '', 'Kiki Mulhaqi Faozan', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.077', '25b1edd2753633915a58b4157e88a45089c9945377909361d64d9ac089d0e167c025d43b06fae5c7e8550e2b0c7eb5b94cd981756d5f46cab6769642eca838418ZGzc0U2EdNHj6l/rDK6IWCZ2GKHirevGBjA2m3bvLI=', '', 'Kilsya Oktaviana', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.078', 'a5a65333d30328aa70013690ae2c0d1fe8b65e502354bbf6d44ac54bf12c6849a519d41e72dee01d65e7f17edf52d1e192e23e239b08df2c9d6055b6ce9e5e94jCpZpLip9u7l8C4tu79fg3uo+JG0A55WGyZ5fHqZ63Q=', '', 'Leviani Astrillia', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.079', '1924aaa64b5dcad892b1e682da362efac4674953472fea5e19ea431c1faa3346df95612404354da1f7d97b20524c9002b9978bcf892df4b1083a4ab9477800be+a5pVOXmC55/IpsQKwK2HMKFs2U84W5oDyW+ZP8QdyE=', '', 'Liani Luthfia Barlian', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.080', 'b0fe85fafc2d681f7a22ee9030bfe4b1d460cacdea574074e298bf6a5826804ce424341f80b814c9655d6c9cb452fc2e15aa56fc87019524718ce6f4efc7cfa97Cs9D3Ux4KJ/3YOCgtALQ5Lees++tYyR3un5aXWkNqc=', '', 'Lusi Sulistiawati', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.081', '99c7cd1a976344e19c1a3cc1686529aaf3c78f870439ce971382257edf77bbfea40c7cf4c045c4242c0a43e6486b750bbf849ed63b0bfa841e68405ce10e1772UsTmAUKz7MdGb7VLff3PPML89Ft5BcVntEEuMGVnCpU=', '', 'Mahardika Wahyu Dirgatama', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.082', 'a5e5c9057f5b295861fded34487d3a2f4bdb11598ef76b22c19b86089b1128d16c0ea3df69679f156fdc50b1763269f860c97938d9a9f349d42f5ce8ab3d0336MuoOi9T0NBMOrst3DVmWYITeB94uA14u+FcfwsRg5nY=', '', 'Maria Veimliani Magung', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.083', '9b1820e1719c0f1b5fb1a45d0a71ccd8f9ae2918287838597dff847dcccaa0e16ee6a7666d88ca78e328dc395990959a7ab4320a59eeb2f656361d172ff78223uJ+bGwB3VEev7QeuFB85Nd3GGWS8upgBPtR41E8dBlc=', '', 'Mario Tri Prasetya', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.084', '94f30cc68f69c3c8e9835418917225650213348cd0ab1ea3b3f6e83523c2cc385e79dea0d2dbaf316ed3bf89beeed463cb719d39ebfa61ff0d5e369ff575a042vIUN2cCrBPVwyCnkQ9cbGzFO7rLT5DmHdNkEbbfM+tc=', '', 'Maryam Amrin Rosyada', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.085', '6e4277f79c4c80e15d70e1cf8100e9954f0cc6d170fe430b796b9530f16143ced3415e6e75f52ad1ad4a54241f6c69cd6699cad1f047935d3c43fa22ebde40209HS/2ojuPLpDEr7JjSXjlLWkDnkOn+/jCHeb2kfeoSo=', '', 'Maulana Fauzan Nurum', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.086', '6edac3848a70b59c5b887bc3f054fe3e04e49f0b5b02f98ab969d8a99891cd497d75a2a787e944db3e66287a5a84e5d9a889f6e1977b4e09da7995b895fae4419glK7J0umvImD0D9T/GlmBTMz9u7R34uOAQgUuUvfmk=', '', 'Mega Jasmin', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.087', '6badee677b2b141a0f7760ea7743d19c844ea4c1dd392eac8f7f07d6ab45bc9592bfb7ee0509c0bc27ccc269ec9c0e1c7d68dd1eb671e6cf50d9504685da253fxCxVPdByGTYL4AHmulJu3PsXTdXqH16PXXRz5AlpF0o=', '', 'Miftah Fauzan Nuryawan', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.088', '0e507619486b77dab612793bd2de718ddb7e4957cc576cc52f000c870221b211bfe1a43f8736cd13ca8ca89c28d86d9057737716853e9c94d613d8d4b384be311QlrtA2Y35tg+Xuec0Bi2YfMhUUmcQVSwMEfwW/9fxk=', '', 'Miranti Verdiana', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.089', '291b3b32b43687283a22f319329714992cd86d64a7f855e816512bc67d7d6901ef9301daeb85f35da528d306e016dfb448207d6fb2a764f6ad4346ea4ad70c99hm5HEHpk01TVJ3mIFDVuFi2VX8DFx3JNdVdGCL84Yuo=', '', 'Moch Tegar Faiz Rizqy', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.090', 'd9c6d6ee04eb8a9886f693862dcb1c80355b80199ecaaf84d8565549e5f363e7247646f11f77624b9f3d3e96666b19dcabdc9f3e6afdc24263f0607c95be9839L+3g0q6hcaNf7twaPpY1sQUf14xaSmJpofwblXRIaY0=', '', 'Mochamad Iqbal Septyan', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.091', '9d006a9f8cb9b7523341453c9f7791c1deae4191f16f8575309aad2e4ec103be9b86f2fc6669a711136c0914f388f9e528703dccc7faeabf7015631153dff6cdIq2b2IHNRmnl3Bm4PJ7QOS5PiVL1jVTRZHQwiI4r0n8=', '', 'Mohammad Chaerull Febriansyah', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.092', 'b8a2b53a33fe3a4d65b3191ecadfb34f447ece9d22d38d6fd901d238c19449c795c2a07654b6fe0ec42151372acd2440349275df8f5d983e568afe5ddfa8ddb3RK3ASD1AEGXLzGLmhiAOQer4/dQOTHhB2kAMOOQLwks=', '', 'Muhamad Pani Rayadi', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.093', 'c87a1aa4cb932f9ed253ee3cd49749af76dffd6119f70b1c5936352ef1c75d4b9216a989bcb6e35aa2a636699cf1e0d36c22dc45de7dad342b5f47f40d03b49elS0LKINhp+k9sxYTDCPxV4JX7T+eXi+o1N2nRYBJua4=', '', 'Muhamad Reza Febriana', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.094', 'fc608a060e18744246acc572db9f30bd5e1defec17287a79b57badd215154488a2237a15909e1a22829008d97886bc1486833492c083d19ba5cc77b4e5b319386gUBiWJ893bKg4gtara1A/eJ8gl4XEXw6fyY3WPTaUI=', '', 'Muhammad bayu raudhlotul irfan', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.095', 'e7506909c8bea55a5f6ef1e6dfe08a4e0c74b0ee9805d010606992e4a4d0bbf882b11282d00fc56108e4be0f3a05b862053a9fe7617b82ed066413c37581b255WGlcxypog5BlAgoVYexjVaNtfFTB27s17zet9y2Q42o=', '', 'Muhammad Endang Supriyadi', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.096', 'df42f378d6ad9f547794550a2f875b5c7eedaeabdc5df0c25b28c21bce77171a63713b0e9df6740b08268410508a6d4a1d145b95afbaa7db9c818a486a7b65beMBIuuk62O2kVy2/pMQM3t2m8UBUBVwZ53W9X/EDtiTs=', '', 'Muhammad Ghazy Fauzan', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.097', '005a4359a19f3f5adc479ad0b97fb3d04390992bbc8bd571dc0655a9b1bd23cb21ab29a109bb4a8997a610b196a56441331890e546520e66c7f763a431f6d914m1A+jDFgdkYgKhETmAcOVeVcJIOfduOKAFinGATBfwg=', '', 'Muhammad Rizam Andrianto', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.098', '97ea1c117ae227519d3d3be3c1849cd3d69adac48e4de508a7fd56a426738b268cf9fd1438169a64ca689a0fb135fdd316aae2ec4cc21c06f4776b8f02e0d122ix4jOHqkY2zTsWuLN+gEFHaCDaDEesqwtwZup6R/q/8=', '', 'Muhammad Rizan ', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.099', 'e1424b873ff315a06dddf696e30ace656f004ad5349865a996b3fda8bb8148b334d58d29946745d0b84573df838ad18e02ad02b138e4ba5628e187cdb2a23ff1jdTAt7u8QFHm4HXveEW2461YIAba7gjmrNgWtD5gBmc=', '', 'Muhammad Rizdky Maulady', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.100', 'cbc6e747a42ab7f73a11b18965fb58a846746f60e3b4b97dc1e70d79378a78a46af1f98ba349321660c6a66c3b664d004fc0e23d19dbf1801af2d3b2ef626491FBGUkLJSaDxQohuTM4ScVL7Urn15z/8ZkYzfjos+jTE=', '', 'Muhammad Ruqiyaddin', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.101', 'd5376b740ce3936bb0e4eb4615ad0a6ef389d6066c1de6ffbe5aacf90b9d4417cba0ed568e10c6558bd5c6dcd6d632c145b4edd83c2c35854c9645d9746dde27oE8U5WEOiwg/Cf9qOogLBNpDXNNQceCh+lxKBB1KRwE=', '', 'Muhammad Yusup', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.102', '9a0c9dcd3d9e4a7cb0f9ba10bdf2e29dc2984a990874c8bf7f54ffc68d766b84e3ce5a34d42ed94f3e9c7e052af5f89ff30c5f03469dacd81f587907d3efddc7tS3gspUjKUICqiOLXinnkIpGPEwww1+BXLrKiNtL/j0=', '', 'Mundarwis Sari', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.103', 'e3457690a79ae9e7bf7b6184b744121992b9bc4d458a4230106d92d400de1ddb43a2109257196107d9acca65db9d2832d17ec064f47cc4d081597fbdf0b972f3YvUihhlmVBaIWr8GC6fCVyqc45SjNTrRw1seW+33Q/E=', '', 'Muthiarawaty', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.104', 'e1209d9a890fc461c56c23f2416ea91054bd916caad4c3074d9af887ad1d1b01ae5093e230fd4cb3ad2c48fb3e55db4b0d8b51215dd38bda0071d6c39bbb690fnZ12gbB+3wwc7z8Y0RXRtpaKXApjAB6T/GV7450bAgY=', '', 'Mutiara Zhahira', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.105', '52732db355c2dfb70105ed1b5b073c93f88c639a5c791d93be9d5a776348c9983fb471915aa05f80fced9072557d10b37ab765c0f4e8fe4e0a417000997a1f9eEA/8w0zcFXB8xbFgec2w8cqsxDIssZNtVT1COw6vSl0=', '', 'Mutihara Sagala Herang', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.106', 'fde5ad7fa0d3d1729293f48a05b1df806016c53ef69e2c3a8b89ec9f744a6ccbc3e91aab05ac36ec080f6c8233e0cd81f78009f2f1596409f467f8c0469d6cd4rC6L+DF2QNbkZl6TNvfKu+PtPzbbwJBwHmF+VBrN8bM=', '', 'Nadia Ana Sekarini', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.107', '06069c5749a29aff75a1e4d079d67f4fbaa55dd312839abae9d24c37b77c13b6c593d65ba11bd2dc03e9348b7b4dfedb4cae4289ded36c18918afcfc74f5e85dmMGaCywe0TbfuVHebm7Q/OZBhKtnzx3IjA5o5tUsKko=', '', 'Nadia Gameta Seba', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.108', '48b65cbc70a9fb757254309065d2ac4bc4d37d28645b65eec945a00d652060866eeddb9f0dcc2b90dbc1b6c0aa7d1bbca23e02af9080a6daadd4c9234e6536e19jg+GlxbpAD/FzC2s6CCBtk14phYyeOLUIVy7Q9W9E0=', '', 'Nanda Asyiva', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.109', 'f88ff6ce8a3c6b2cda5b7be5e94b96855ac5bd7bf0158347d755e8359bcc2520a90ee05565a17226ed8397177556c5146927f8b05da91eaf5c081aff067bfe2bknO2q/2jD2mJvNshthFO4MbigXB78+737u1hibGPnHg=', '', 'Nazar Rizkiani Nurfadilah', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.110', '510883929a9f0bd8dbd995a10d43db92e025011cbd4e68e8561bdcb22ffd6aabcd846f2d4b9f188c8eb7fbc3bb94aae33ee3cbf0784930dd51b3890b7bfd952fRR+ehoZ5lAUdV+h8D8YG3LUXJgnPRvSXC5gT0EBfgmk=', '', 'Nelly Armeida', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.111', '0e2ef7d87af837809791215813bd5c7c07c3b70f3264d66635d5ddfe25f2521bf0a6ef50d8932f543ce4a52dc5e0e382ae3d019e7c5180252bdb5cf1b67cd1ecosVCjQmEL7b9s344KxU/KOQlZCQOZg8wPKAq/wDIOpg=', '', 'Ni Komang Tri Purnama Dewanti', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.112', '91fc79b03cc638db97ea2c2d7e59f908d9644725583ad521674afd45ca69a39030ef2216785813f305c7bdedec1e0c8172c5b9f03eb76e20b35d24fbfb3e7c67PyoOxwS9Vf4aDKt4juiKOwusoygTOFhHKASON3t2V/8=', '', 'Nurghaida Azhar', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.113', '1f9cd7dcae11c5bb3422b56c4bbe017c994947c76c757262ec609c4688a73e507cac70329850335659bb8914a72c17a78cf8f6a98b2e0b2d37a6f4110e8be4d2Z0KkTLJPbvIGMRBw6x/X2DSG/n7SF791PzE1bzq/YqY=', '', 'Nurul\'aini Salsabila Kurnia', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.114', '585111841f59a093b64adf0249ccde0cfa718b469092cefa8d960cae748c3281dcfe253c811bc7d6d4d94d2f34175cb868a5fdca9f128b8fd08e2d344aa7c5f3pO3eAsG9tETTELRMOgMTUUamEzBrw+6P84wj0lNWpt0=', '', 'Olivia', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.115', 'b7ec1a1b506a1d735c2e7f0ef9c57e98db92c0702984cbfe91c61081f6b1f948fd46ea99fce9fce86b416c6e1bcf00a2b48df79b5cdc927872b2a6e8e993e81fLtcfgmYO91YdU7GxAksqJnMPOtT1eBUFUEeqf/9f3/M=', '', 'Poppy Damayanti Usmany', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.116', '615e8cf76cbe92bb9078e56ccc574e8973473f95c590432140f3cf194f3a899659a662b099e5cd7a989198b9aefc16fe5c4ad4828490b200b4a7ffd378141f53Rm+AfsjxBADmPlW2Y+0w6ChMJyS82+ebn4VaPMXVwxg=', '', 'Puja Despa Rama', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.117', '07e681d41509ca28097ca2fca3c486ee908e9f2f6f29086affbb65a52bac062d1b7410fd95ef46eba1b90a7625df4366f3770c7b2f304b95bf2ba399fe5898cb3zH0mY9oWxA722HWLFOuSQJ86oD3tTRhJ4NlgeA/tuM=', '', 'Purwasih Widyawati', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.118', 'b9a75eb469da7904219cc9fb72bf93f5d59da182083e7d00cbf5e9bd94531bbfee8b792a3aed89cb4124f3e7655c3aaabc0314105a1652cac9efd96a6c19adb4fdDnsPKEq+YHI7b7uzJXguiv2GTThCLesL8Ju+mxOyc=', '', 'Putra Army Yudha Septa Triyono', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.119', '816cba7c0a0b8a4225fb458b1329b93468f4f43ded7b548817f6de961bef077412aceca552b8bd826f5e0d4e12396f41193d75c7858a6bbf1bb5a06b72c6ef43VLdLuIIzUclzyHO9ytxvbkXRCXcOswhBNBGqem/AlkU=', '', 'Raegina Nafarsya Imanirofi', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.120', 'a8f760003beb33dc7aee489d4dfb71b7239f7954d009cea99e23ff0df301585f3ef71e15904488b706b62c1c48dd689e8a6891403dcd56dfeeec208a94387876Va2E9h/QN9RdALroz6lWGVfwcl1DV/GbCr6EwQZW45A=', '', 'Rafa Nafisah', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.121', 'ff87f8fb71f8cc13686edc473e174c91201c7e60f39d2c82e0ccbd5336c43eab1703df69f7ab620f23ef77ae7e2a62796f865baf9923a5ba65967ab61d02e9ceg4roevOgx+1tnJY3iHiXfOwJf0WQjRo3qtOh4MFlnRg=', '', 'Rahajeng Yasmin F', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.122', 'bafe8d3f1016d7fdce83ab800cb6fea0c85a628818ba3f7778d8dec2882219d80cf94f8542cb1c2562c86177429f933110c175d732183f1f35f99d34a5ce9c1e2zR3W555hcOqxBa1Zn873Bt/mOd17ZLHYg2t4Ye7c+E=', '', 'Rahman Hidayat', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.123', 'cbf68db381e8b49cb25ad4712024b4aba1e9334deb6486ea8c8afcbb79dc79e018be8e18a687d523662f862cbb42741c802160b048345e94b53531be6fba5e1eLG790EN20CdcLby87d4yW38aPQ8NcACD9feM7ZKIq1w=', '', 'Ramha Rimba', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.124', '38d68ae3925ed7d372aa29bc3163972ed52e32a6d85f7c332b543586b97ab27e775dde8f922a52713812e0d9423f7fdbc8d3b0cf4b53b64133e7d8ec9c905002y62eLcNKdqlH+IE7K1sTk/+muBQFB62puYpU3uOkfSw=', '', 'Rania Febiananda', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.125', 'bf07ed232e42ddaafb0b67dbaa6b04e9cdcae1b1ff6694049030ed9ab55e718e6b950940ce02c07b3e1ad0f78b369b7a4435697304e49d18eddb79f5b57b9e7aFf7HMnjsM/Bg5NSO96p3UZyspedE/XStgaVEaaZmC4g=', '', 'Refi bunga pratiwi', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.126', '2c9d5de4c99eefd73f19a0dee4b6759210304bd3a72cd209823e4eb6daa1b6876f9b7a9f45e60ee9c1b7762c3614f3ab4bf5c48cef49fc086c3ad21aa5cb2bdfdjZtmPs1GJZg7+OLg7J8XCvSzn13o5hwpdLgK2Syj/w=', '', 'Restu Sapta Safira', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.127', 'dd563a879228f6d5625549ea9a1fc712fc0fbb86249fe9622f16ffc17f69ce176fdabf0b09db7688ecc8fa2d67a0fe2b9fc3cfa64dd37fd3ed79abc26e95b19ewx1Hu7o2K7bEtJ/8HfeH2UdVuUGvESPmQEPC3dEPTnY=', '', 'Reyhan Fauzan Nugroho', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.128', '10a2f64f45398f527a47ccfef4800a192dcc69b480450324df97705a130d08daa5bd394fe73fd3aa9e49a88bf80d0d258593174ec208b85c5662efae35c8911alURm70Y+CcD9zdzxT22FEDniGAj4QWJmlO4FY0gKMCo=', '', 'Rian Aprian Ashari', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.129', '03dbf1f64c05752299d0852385b9dbea2e4c09b3c23b30afd77e1ba5648dcb50ddd7c23a3567b96f8ed0845f39991afa0eebb34a67ef334db970d924bb99dc90JG3ptdXfoyQp8xtWiySaJyR7mTHi9roh/jO0q5OGA1E=', '', 'Rifayani Nur Anisa', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.130', 'c2416d38c9d389507b8d453ce0aa432926a36cfa26c5c804a8b22d5da6eea51244c1d4f31cc219d8090ef52e9fa43aa792980f0513f207362c806fe90f6427661Fw8dCoffcKPH6BDWd3B5G4694nxEMggv3Oal+CLZbE=', '', 'Rifka Yovena', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.131', '101856d282dd964bc431a1b9a0de8d854b8e30fc00248f699b037882bafa69b8e914ac76eeba3017e33270a2606e2df64cea46793a6e88739292a7f9eaef4692sm7mqYPcOEkambnqVYKYztKKr68V99YUGnijCTdOl3I=', '', 'Rigil Makmun', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.132', '65ea78884d7a052a6457470bfa82192ebdb41098dec84ee80759f3281909a1c252cf80d1678051f58c78d79ce3147186f0925fd7aadd148a16eefe2248fd03e1addSyVczSw/tQWJCIa7fJL23bqXr2GHXUkULcQ0F8HA=', '', 'Riki Ahmad Fauzi', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.133', 'd9ed5d336f21e6c0201885cc19d23ccac0fd40d7ab8e0adf8b0f205f62cb1b757e7102ea5111b623bd884431acd71ada49bb0838e96834fc5547d7173f7f5992MOeFpKt6SWCUE472T2Vpzhq6Uugd2Za2b5f183TGx9w=', '', 'Riki Andrianzah', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.134', '94cbdef90cb3c0ae7a68e45cb783ef1995da8621a655f95736346dcfaa80414941340bbebb9c381206c5b06c871feddaf3850f0b5e5f696c39ed90271ab56491A2cayHV1zB+0ZYaV4Dp6R2EQ9WRkzzgtHC0rvaQdgg0=', '', 'Rita Sugiharti', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.135', 'aca70983075a6b29a973b25d1503a1f552a7ff8b146deb9959ef673ffb6b4c4224c153a8c0344b908dd630b5d07fb79bb8f69e858aefc8755ac88140ce9061e8mPBPPoXVTkX6pASZZxGdxv5DMrofg7EPLeVzSqHDlWw=', '', 'Riza Dwisyarifah', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.136', '08990f69020a65b2fc0296393a7f11ed0deaa8a7ec6408738a1a82756786fe42dfc3e1b76747f9d26f4b8d3f256f3de8d74f6f7fcdb23d62cbb43b88fe69a15cp5y1JP5mWdC4cizc4ir9dp/rKpo/YxJxcrb5+afc4ZY=', '', 'Rizal Mustari', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.137', 'e727054f39f167e69b4200fd283869b5a7dbb3e0890d090cae924643e29384ae9abfd5cc453aee659094fd716a760811d1ed581041e76c32c9fd79f0dfec680d8TySP1CANej1prLsvPKjgxD34EVXtbERnI+5E398r0E=', '', 'Rizki Maulana Ramdani', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.138', '5072b4d7cd8fbebb6cb921d0026ec99087851d8b42906b06fbed664a0c49f9ff2f2032882e79b770a8ca6747e4e46b1d80342cb2784a12993cf5247a2b58d2f4eSDfDU22+GNAx0MbuTeJOyDRtMjVs9HcGlsxu0OcMXs=', '', 'Rizky Novayandi', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.139', '6b3399b4b5cd26ef1a0252fb610394c3d1bd01297f616fe18fefbe22913e0f6d9b355e94c89fa0e739987ee10f38dc281639a44bb3173a1032f1725ab4873ff9MDMMT437tLtSXP3waIRlCzlg2yf+otpTlO/K3bY09Uc=', '', 'Robi Febriamsah', '', '', '', '', '0000-00-00', '', '', '', '', '', '');
INSERT INTO `tb_anggota` (`NTA`, `password`, `NIM`, `nama`, `jenis_kelamin`, `email`, `agama`, `tempat_lahir`, `tanggal_lahir`, `kontak`, `kontak_lain`, `alamat`, `sosial_media`, `jurusan`, `foto`) VALUES
('252.11.140', '4769194211c6ef2e4683ca93b5a62ef8403b29b0270772ab9e57c2459c3fcdf308b9a38686224b3f8c18899ef15fcc8559d52ae48651f449b69353efcb22a4efTWeAoAwQU5WykdJU+uufppDt3crJeSyoU5i+//xckgY=', '', 'Rodia Setia Sari Malau', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.141', '888cd4ae2a9817fa81c55cf41f674fd96475918b8049656955062682adbfda9e17318e1a0a62c4ddf6e658ea16f365983440a743c9f106e227a2c6c5585eb695gYHKt4bif0xJjaChojezIdhyABj2/uu9azmbXLkQeSo=', '', 'Sabadi Dwiputra', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.142', 'be091a296565a7ee062fb077d7ba9c69f5b10d622f016a567275973c14ea80ef270d8d62385033af502dcc828f3b322c772c9ce3cbe90557c4fdffcc90768d8faYvxoUcIK06X3kf+qmV1pN/j1Y1GkTF8Ulymj+zanK0=', '', 'Sabar Aris Munandar', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.143', '503617dec60a920e0e0ab56faaa7696eef57ab5f7e6586aa15ca93444aa51e8fc2e15cc9dbe49a108d59b226ac744a35ab988f265dc4e9fa2b23d7df04d48dd3oelM8REP35YkGjRRRVvLAxgEuQ/DqPtX7oCdjaFigjA=', '', 'Salma Virgita Basara Agung Putri', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.144', 'e360d0b006df5e97d545e6dfe8cca51e891f56b98c01c0f1ed5bb0feef0ca6328af479dbe0b41a7f591b5f3ea41e8b0d18c12f9d02c9c2dafb40d484f3b69cc6RsV+x9fSR7Fm3Lcnx4kZUynJlYZdEe3sAbC1S+8zdhk=', '', 'Salmaindhinu Khalda', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.145', '8ca28a806fe43402a10a28b06c76ebdbf04baeb0f0a331c39e4d5c58cdf90f41fc2f3e6585a20ac3c314c2734581899308212ccee383934f56a583fac0503bc6C2wjW9uVj/aKjj0MvodlnSBJCe54sgbVFiafCUeJcq4=', '', 'Seli Ambardianti', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.146', '41decc9bde0966cfb38fff5305f9b81ff01a9e72f403d4ccc80f8173986116f183f7c19feb06b1c03e339258ae34304be56356d740689388fdb22e771e6ea296LbfsJU3TUmI8likqIdBD3hUw98rl/K0P9qkzmt6jJU8=', '', 'Shenny Satrianti', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.147', '1f9e68596b0ff9167a03b53472e37caca3eb559d4eab668bd6c09ab0db9e489529e284903f7415d839141b98dee27a10214618f4bccc361b889244e6ddcd62ebFWZcPfa34QcZuAx8nrSnl9J9PH+k69LQx2c/+t3cKQo=', '', 'Shinta Dwi Puteri', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.148', 'b663a6cc613babaff83cd72ed1fd659412946d6e44443ccc91124e3d935808ad8406669b2e7249edbc3c9dd97f46bccde9b107685729c78db77c08080f2e3913+J/P1cStpUzVCrPMwUTdjVqrSwqn6rPM2g4Zw95CRNQ=', '', 'Sinthya Destri Haryati', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.149', 'dd67572346f261775b905b3d45f458449f5bf3d5a2c1bc13a2208f4c5cdaf17935d639cbf395be873fa5130d96529d376f4dbc5ec474a126ae1a3d342d852ea70upyTCPcfGI1HIteeHWbAdDJ4tF2gW/8sYIMukgP+yg=', '', 'Siska Dina Febriana', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.150', '671290d9f12c5247ee70cc3ed52c43ea4ef50b6b6b568fd40d8ce53622ba8b74f4269a62f2a693419f3d9cf3678a1c7171a2e9f914e5a56f2b6fe0e9d38d0b10807tu3rieWbo8iFKoxiRI0jVMMMrVs89Fa6Kj+7xVXw=', '', 'Siti Mulyani', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.151', '54090e120bcac30e213839863bd5ecdf223588dfbebc16fb543f405fe5bc0c17e895771f97440971c635a50d3a657a5ade66a3e48fa23e07299f768bdef93981kkPNGfnClR2T1tw79j4wR+GqKtY26BMPbMralHUMGxY=', '', 'Suci Nur Azizah', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.152', 'e3e8ac5bd6ab8a5a2b752b1e9b9e956924c331cf9c9081c3b01387eec7efa4f3f0a0af503974bef3371fa37d75e72c95066b251d201448ca497b88af81ab34bbgvPvuHtFawCsbnEeV68F08cpZuhr/ttIXjh6LOy01Us=', '', 'Syahrani Fhiara Pramadita', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.153', '5b516bce3f2af0e74f989ee713b40d5cb79fb5e75cbee54422567e16243d3a8f36549e442b4692bdcb026fe805db8f6f1aed5a0cbf0524a966c7efbd220a6cbb6JmQF8BujxYPH0uarSVEAl0ByXDWzauRy4y9ayO2+3I=', '', 'Threesha', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.154', 'b7afa62f5328ef52c83c7dea535e8b8cb0a0018bf4f50262b63832c404b966ed83397ef3d74f70b46b909064bdcd950d166a70bb8bfae372c383ebdb3f99ad9ew2AzwM5TDD19usuzJnnq/btLipDkQKFZPyMH51hLQzI=', '', 'Tina Rostiani', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.155', '286e7652736c207d9530b9548a747e8f00caf47fbf40aab7f3b3d28add1cb42414222d6a16651d282d9aacc3267108d2b56f750ff9154fb17e88e9c1a850a2d1mkHCSf8gsrUyIfssT1Jcvg3T8MWsmZ2E3wtZek8pfhM=', '', 'Ulfah Gusmira', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.156', 'b16e74611d4afc167efe34f4ba6f9dec42e89ad7dfa3a0c46de5e0ed99f235618f339019570eaa79de570308feaf99ee6d39ff583749345b0b73ea07261519bd9YJCK/Vd0RToiVIPA5T4BpqposKwrQPCE8OzNq6nQwA=', '', 'Vera Seftia Nufus', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.157', '462ebaadde35b602d62b85918fdb4ec3bad1d7b42d3404a6dadc414f61ccaf3f917fb4920f2e2975408d5aa35a05fff54b75702b2faaa9d1f26346b787c6242fVUSUXCOPsjmB2dmQSpnc60CDrS4VElvW1q6oxMnYuaA=', '', 'Vina Anggraeni', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.158', 'c4b749b45fb320b53cd109ab4add4e6e91fada78a4ff9075b2f835a2e15feaf762ca0f6d12558cdc97aa173f5fa1c79bb31589944d41884271a6b5bcfbb120bbrn490wzYjtQ/1TN5yXvTOgjOXSzM5BsXkHPQyKpqQR0=', '', 'Vira Alfauzia Juniar', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.159', '799227077c31326d2e78eb5e9d4b770e026888ee8dd4f10dbaf3208dc1b0b50da8a13b90e864434fa383288b86d43664957f3f048976806503fcceaa3bc4efbeke9fL+7uDRR0mJMJ4JTLbsk6kYGwChg8q1a/nffzy3o=', '', 'Wahid Herlambang Suroso', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.160', '9d8e2640a10fb608f94d7c90bc4c70951cb137c074ecf8663248794e22690a2be31e4a0ab72885912d3628cf72299fda1cff78b22aef57050e7bb13b3c6993b1zcQshbV7V8g/GmR6Lvomom7M/dBojCqmq/w0mhtlxKI=', '', 'Wandi Hernadi', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.161', '0330f078b2987bd895d96729a08ceb6d328dda75a0b19a3ab2a912e07e954f4ca288a2f1a6d6266018d914985752e917f59862c4067a9e541ef0f161e879ccb1hM359Z/jONE9j8TAoFt7lOKxUoy6XO28/mdgaU9fmSs=', '', 'Wanti Karwati Setiawan Putri', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.162', 'd0d53a5a8cc9222d9ff3e72c46cf962fbc878387912dd44a057a5511bb7072f5dce70d67dbc80c20e657bbbbb519c7d4eea098dfd7886e38b172c2787e41d485DU9DOMz8IjSjnDhk3qTDEM56bFdVeeHhGtUKpHCgI60=', '', 'Widianata Ramadan', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.163', '4afff333717dbd77d726348ef9fb994a69959fa54caf6fcd8b432c23c57d80f9b13de7dc030a87d6bd63870811ce67a28e99e9d8aaea41c34eeefcad61ef8af0uMd221r9jggnAwd+Ajloda4icij4PtAZvM7BLK8vD5w=', '', 'Widya Ashiilah Jasyr', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.164', '040f0d60a61abaa4ad47c1d4a0b9bc3e00e2271a10932ea63bd8925419ea762569580b1ef2c0e00a6ec795c5e2f9dfa66d423185f763da6cb2225a99c7cebc5aqAL7g3zb9GCi11l+PqJ5HayZpoLsdCF+0InVrX0ko5o=', '', 'Willa Dharma', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.165', '240bbd1334b0a1dadbb4d401d6ee4bf68879ce43a4d47f256961c19f952b24edbc7572c64507e378da3580d07b9581c7681004a5abbdaa1b721421237410dde0X3n9l+yVOtTgVP07zoegV6oqsu0K5pcBvK4Ba0DxwpA=', '', 'Yesaya Septian W', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.166', '6a82e72fa02a8e3f67466d2396a5d87400e25d1ddfe8a4712a83d37727b0d579950e222f9bd08e31c20f7cc9425d3c81d28535cea17821b10aa464b827a84aaal0mD/njxKHyoDsm/T9gOQ1w4xx+FCPEs5ER1tYQvKeA=', '', 'Yessy Ramdhayanti', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.167', '8e03ff9f50defc16cffcf16d92670cdfdf6fdb453d9aee185130cd596d4056bf16319d681c2305f6fff3a39ac6669031fcdf718f0b69ee1bab43e9c01364487dnSFjyd0664wArI2gwHBVvl078I74gZkzaIjmd4AV2Sk=', '', 'Yuke Anggraeni', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.168', '19bab3c2091444841e16eed1c8fb3df58739681211236233fea8f82ffa6a3e6748f6cc4b361db158b7c001a5e087786193dac5e7da1856597fd9313da81232fcbLI5zwjyKEXsdG5Z0cpb4ah0VbkZNlslv+iZfqAfXSU=', '', 'Yuken Dwi Fauzan', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.169', 'a32378e06f46e910af4d043b72afd2db869e1ec47b230f0d91130707caa9e497a121c78b5a22ad033a237f6645e233de4eb15ff2310e3e1c10a4de82edfc6726oyd8yxCikr16hGeUzn9mc7KUjPny8ez7nQWjZ9nVvlQ=', '', 'Yuli Yuliawati', '', '', '', '', '0000-00-00', '', '', '', '', '', ''),
('252.11.170', '1c3915fc12cb14a963eed997c7f93ce43c3f07a5e7882d0534460f5113973c6260fdb3d1d399745b8528806ab7631d1317400310b1ef36bc7fdd9e8fdb08c9f3eI5rP0r3qf+oacmQr9RISjq0wp52bbPS+5+LgRVhWDI=', '', 'Yussy Wulinda Tsani', '', '', '', '', '0000-00-00', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_fakultas`
--

CREATE TABLE `tb_fakultas` (
  `id_fakultas` int(11) NOT NULL,
  `nama_fakultas` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_fakultas`
--

INSERT INTO `tb_fakultas` (`id_fakultas`, `nama_fakultas`) VALUES
(1, 'Fakultas Teknik & Ilmu Komputer'),
(2, 'Ekonomi'),
(3, 'Ilmu Sosial & Politik'),
(4, 'Desain'),
(5, 'Sastra'),
(6, 'Hukum'),
(7, 'Pasca Sarjana');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_info`
--

CREATE TABLE `tb_info` (
  `id_info` int(11) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `tanggal_update` datetime NOT NULL,
  `tag` varchar(30) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_info`
--

INSERT INTO `tb_info` (`id_info`, `judul`, `foto`, `tanggal_update`, `tag`, `deleted`) VALUES
(1, '123456', '<p>You did not select a file to upload.</p>', '2018-09-27 22:28:22', '1123', 1),
(2, '23333', '<p>You did not select a file to upload.</p>', '2018-09-27 22:28:55', '333', 1),
(3, '12311232132AAAA', '908c9a564a86426585b29f5335b619bc3.jpg', '2018-09-27 22:30:01', '132', 1),
(4, '123123', '820a8f5c40c91fbd63f19519314ca2772.PNG', '2018-09-27 22:53:18', '1231232', 0),
(5, 'Ini adalah judul nyaa owowowowo', 'a8240cb8235e9c493a0c30607586166c3.png', '2018-09-28 20:11:57', '123123', 0),
(6, 'Ini adalah judul nyaa owowowowo', '3210ddbeaa16948a702b6049b8d9a2023.png', '2018-09-28 20:12:26', '123123', 0),
(7, 'Ini adalah judul nyaa owowowowo', 'e6d8545daa42d5ced125a4bf747b36884.png', '2018-09-28 20:12:47', '123123', 0),
(8, 'Selamat malam semua', '674bfc5f6b72706fb769f5e93667bd234.PNG', '2018-09-28 20:13:43', '#ucapanmalam', 0),
(9, 'Selamat malam semua', '3bbfdde8842a5c44a0323518eec97cbe4.PNG', '2018-09-28 20:14:54', '#ucapanmalam', 0),
(10, 'Selamat malam semua', '7b7a53e239400a13bd6be6c91c4f6c4e3.PNG', '2018-09-28 20:20:20', '#ucapanmalam', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_jabatan`
--

CREATE TABLE `tb_jabatan` (
  `id_jabatan` varchar(2) NOT NULL,
  `nama_jabatan` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_jabatan`
--

INSERT INTO `tb_jabatan` (`id_jabatan`, `nama_jabatan`) VALUES
('1', 'Ketua'),
('10', 'Biro Divisi'),
('2', 'Wakil Ketua'),
('3', 'Sekretaris'),
('4', 'Bendahara'),
('5', 'Biro Humas'),
('6', 'Biro Kajian'),
('7', 'Biro Peralatan'),
('8', 'Biro Pagelaran'),
('9', 'Biro Kewirausahaan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_jurusan`
--

CREATE TABLE `tb_jurusan` (
  `id_jur` int(11) NOT NULL,
  `id_fakultas` int(11) NOT NULL,
  `nama_jurusan` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_jurusan`
--

INSERT INTO `tb_jurusan` (`id_jur`, `id_fakultas`, `nama_jurusan`) VALUES
(1, 1, 'Teknik Informatika'),
(2, 1, 'Sistem Informasi'),
(3, 1, 'Teknik Komputer'),
(4, 1, 'Teknik Elektro'),
(5, 1, 'Teknik Arsitektur'),
(6, 1, 'Teknik Sipil'),
(7, 1, 'Teknik Industri'),
(8, 1, 'Perencanaan Wilayah & Kota'),
(9, 1, 'Akuntansi Komputerisasi'),
(10, 2, 'Akuntansi'),
(11, 2, 'Manajemen'),
(12, 2, 'Manajemen Pemasaran'),
(13, 2, 'Keuangan & Perbankan'),
(14, 3, 'Ilmu Komunikasi'),
(15, 3, 'Ilmu Pemerintahan'),
(16, 3, 'Hubungan Internasional'),
(17, 4, 'Desain Komunikasi Visual'),
(18, 4, 'Desain Interior'),
(19, 5, 'Sastra Inggris'),
(20, 5, 'Sastra Jepang'),
(21, 6, 'Ilmu Hukum'),
(22, 7, 'Manajemen'),
(23, 7, 'Sistem Operasi'),
(24, 7, 'Desain');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pab`
--

CREATE TABLE `tb_pab` (
  `id_PAB` varchar(4) NOT NULL,
  `nama` varchar(30) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_pab`
--

INSERT INTO `tb_pab` (`id_PAB`, `nama`, `username`, `password`) VALUES
('PAB1', 'AdminPAB', 'adminpab', 'ccefd949f07d2501f59a6cccf81d30fc801f982289c5dd63571b6333312b6bd393a95defeb83069c6a68956e86c28040060c1191181a160b0d041a2d43948937+i1rFjwXuptPU7aOImjFnLN8H7ADrkOjf+VoTF4m61A=');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pendaftar`
--

CREATE TABLE `tb_pendaftar` (
  `NIM` char(8) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `panggilan` varchar(10) NOT NULL,
  `jenis_kelamin` enum('L','P') DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `agama` varchar(12) DEFAULT NULL,
  `tempat_lahir` varchar(30) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `kontak` varchar(15) DEFAULT NULL,
  `kontak_lain` varchar(15) DEFAULT NULL,
  `alamat` varchar(130) DEFAULT NULL,
  `alamat_lain` varchar(130) DEFAULT NULL,
  `sosial_media` varchar(25) DEFAULT NULL,
  `sosial_media_lain` varchar(25) DEFAULT NULL,
  `jurusan` varchar(25) DEFAULT NULL,
  `bakat` varchar(50) DEFAULT NULL,
  `minat` varchar(50) DEFAULT NULL,
  `alasan` varchar(100) DEFAULT NULL,
  `foto` text NOT NULL,
  `tanggal_daftar` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_pendaftar`
--

INSERT INTO `tb_pendaftar` (`NIM`, `nama`, `panggilan`, `jenis_kelamin`, `email`, `agama`, `tempat_lahir`, `tanggal_lahir`, `kontak`, `kontak_lain`, `alamat`, `alamat_lain`, `sosial_media`, `sosial_media_lain`, `jurusan`, `bakat`, `minat`, `alasan`, `foto`, `tanggal_daftar`) VALUES
('10115021', 'Zaky Muhammad Yopi Rusyana', 'asep uhuy', 'L', 'zarfaf@g.com', 'islam', '1231321', '1998-02-12', '23132321321', '21321323122', '12222222222', '', 'qqqwwweee', '', 'Teknik Informatika', '', 'Tak da', 'UKM SADAYA', '148148d62be67e0916a833931bd32b262.jpg', '2018-09-19 18:59:05'),
('10115022', 'Asep Budi', '', 'L', 'zaky@blue-banana-bear.xyz', 'islam', 'Cimahi', '1998-02-12', '0892736366', '02773462712', 'Cimahi', '', 'zakyymuh', '', 'Teknik Informatika', '', 'banyak minatku', 'karena kamu dong:)', '8620005ac78d8257435d490058c643dd1.jpg', '2018-09-19 18:45:23'),
('10115024', 'Zaky Muhammad Yopi Rusyana', '', 'L', 'zaky@blue-banana-bear.xyz', 'islam', 'Cimahi', '1998-02-12', '085524421947', '08980896910', 'Ciimahi', '', 'zakyymuh', '', 'Teknik Informatika', '', 'zaky123123', 'gapapa pengen aja hehe', '2b0aa0d9e30ea3a55fc271ced83645363.jpg', '2018-09-19 16:56:03'),
('10115031', 'Arief Septian Wijayanto', 'James', 'L', 'arief.septian66@gmail.com', 'islam', 'Bandung', '1997-09-01', '085794063517', '022-5432093', 'Komp. Permata Kopo Blok A.154', 'Komp. Permata Kopo Blok A.154', 'septian.arief', 'septiaan.arief', 'Teknik Informatika', 'Mencuri hati kamu', 'Bermusik', 'Karena aku bisa membuat hidup kamu lebih berwarna', 'f39ae9ff3a81f499230c4126e01f421b2.jpg', '2018-09-18 12:18:00'),
('10115211', '123123123', '123', 'L', '123123@gmail.com', 'islam', 'cimahi', '1998-02-12', '973736365636535', '17263263263232', 'f2r1vdvdava', '', '12313213', '2313', 'Teknik Informatika', '12321321', '321321321', '321312321', 'd8074a35855a7f4935e3e19222d9a9eb2.PNG', '2018-09-20 11:46:53'),
('10115333', 'testing', 'tes', 'L', 'fjarnugraha763@gmail.com', 'islam', 'subang', '1997-10-22', '089687634901', '089687634901', 'subang', 'dago', 'nu_nugraha', 'fjarnugraha', 'Teknik Informatika', 'nari', 'ngegambar', 'karna aku berharga', 'c5efe10ef922d575908700ec15d7517f3.jpg', '2018-09-18 13:07:10'),
('10215061', 'Dimas Mohammad Makdus', 'Dims', 'L', 'dimasmakdus@gmail.com', 'islam', 'Rangkasbitung, Banten', '1997-06-13', '085219587662', '085123468756', 'Komplek BTN Palaton Blok A3 No. 08, Muara Ciujung Timur', '', 'dimasmakdus', '', 'Teknik Komputer', 'Main Suling', 'Musik', 'cari banyak teman dan gebetan, dan mengembangkan musik', '8e987cf1b2f1f6ffa6a43066798b4b7f2.jpg', '2018-09-18 12:28:14'),
('11111111', '11111111', '1111111111', 'L', '11@ga.m', 'islam', '212321321', '0000-00-00', '21313123232', '3213213213', '3232', '', '222222', '22', 'Teknik Informatika', '231321', '3131231', '3123131', 'f0204e1d3ee3e4b05de4e2ddbd39e0762.jpg', '2018-09-19 12:30:39'),
('12312321', '222222', '22', 'L', '2222@gmail.com', 'islam', '123213213', NULL, '23132131312', '2132132132132', '32131321', '', '31321321', '321321312', '3213213', '32132132', '3213123', '3131231231', 'f1b0775946bc0329b35b823b86eeb5f53.jpg', '2018-09-19 12:27:00'),
('12322222', '222', '2', 'L', '2@fdacpc.c', 'kristen prot', '2w2423232', '2018-09-24', '23232323232222', '2323232322222', '2323232', '32323', '323232', '', NULL, '', '323232', '323232', '97f081d3b1b352e9d1aaa2225dd6bb165.jpg', '2018-09-26 10:39:23'),
('12832713', 'hay y u iaa aaaa', '', 'L', '12322@gac', 'islam', '12321321', '2018-09-17', '1111111111111', '111111111111', '111111111', '11111', '111111', '111', 'Teknik Informatika', '23333333', '3333333333', '2323232323', '142c65e00f4f7cf2e6c4c996e34005df3.jpg', '2018-09-19 12:35:39'),
('41816269', 'Muhammad Hafiz Hidayatullah', 'Sayang', 'L', 'muhammadhhafiz11@gmail.com', 'islam', 'Jakarta', '1998-11-02', '081236355673', '081236355673', 'Perum Trias Estate Blok Jl. Gandaria II G3/9 RT03/014 Kel Wanasari Kec Cibitung Kab Bekasi', 'Jl Tubagus Ismail No.46', '081236355673', 'IG : @nobitafis, Twitter ', 'Ilmu Komunikasi', 'Selingkuh', 'Bermusik', 'sadaya butuh orang seperti saya', 'ad9be0b5d43f9e2aba895f3ede723aa13.jpg', '2018-09-18 12:06:11'),
('99828273', '#SiapPakEdy', '', 'L', 'siappakedy@gmail.com', 'kristen kato', 'cimm', '2018-09-03', '0893848384931', '0893848384931', '0893848384931', '', '#SiapPakEdy', '', '', '', 'Tidak mempunyai minat', 'Kepo ih', '0342c9a7b54450830e9727b98f8e3cb72.jpg', '2018-09-26 10:50:11');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pengembang`
--

CREATE TABLE `tb_pengembang` (
  `id_pengembang` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(80) NOT NULL,
  `fb` varchar(100) NOT NULL,
  `twitter` varchar(100) NOT NULL,
  `instagram` varchar(50) NOT NULL,
  `linkedin` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `bio` varchar(100) NOT NULL,
  `jobdesk` varchar(100) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_pengembang`
--

INSERT INTO `tb_pengembang` (`id_pengembang`, `nama`, `email`, `fb`, `twitter`, `instagram`, `linkedin`, `foto`, `bio`, `jobdesk`, `deleted`) VALUES
(4, 'Yuma Yusuf', 'Yumavol@gmail.com', 'yumavol', 'yumavops', 'yumavol', 'yumavol', 'edc27f139c3b4e4bb29d1cdbc45663f94.jpg', 'Seorang yang ambisius dan telaten juga baik & penyayang terhadap hewan.', 'Frontend, UI & UX design', 0),
(5, 'Zaky Muhammad Yopi Rusyana', 'zakyymuh123@gmail.com', 'zaqboedax', 'zakyymuh', 'zakyymuh', 'zaky-muhammad-yopi-rusyana-9727b4115', 'a9dd14d824822d6d78d0fe3e55dbd7fb1.jpg', 'Mimpi besar menciptakan lapangan kerja di Indonesia. Penyuka kucing tapi lebih suka kamu', 'Backend Programmer', 0),
(6, 'Irfan Rangga Gumilar123213', 'irfanrangga16@katapanda.com', 'irfanrangga16', 'irfanrangga16', 'irfanrangga16', 'irfan-rangga-gumilar-162372153', 'd90e5b6628b4291225cba0bdc643c2954.jpg', 'Usaha dan kemapuanmu akan membawamu ke dalam keberhasilan.', 'Android Programmer', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pengurus`
--

CREATE TABLE `tb_pengurus` (
  `NTP` varchar(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `password` varchar(204) NOT NULL,
  `jabatan` varchar(2) NOT NULL,
  `deleted` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_pengurus`
--

INSERT INTO `tb_pengurus` (`NTP`, `nama`, `password`, `jabatan`, `deleted`) VALUES
('11.1718.001', 'Des Nur Trianti', '015b4d4ccac94e78035061896c9ee28a17978bea4b13bae521968a639fb338cd5882af53d75d80e4decbc8964d9ffa7fac43126719aff836d00d75998c728bdf+hbPbosUcEVCEg+v5CiFkGAB+qArIonPht9yvndqnL8=', '1', 0),
('11.1718.002', 'Fajar Nugraha Sugiarto', '947e8d66be21a04a9ea80a18929e75509fa6a95c0559a9139a074190080c2545a5ccbfb09f9c435e4054c15cd168cf48ed76410c053f3cd79aff364de422685fqTYUaD8kxABwhS6pbZQ1I0+W60nLzi2fmLU7j4FlxJs=', '2', 0),
('11.1718.003', 'Nida Nurmalia', '138ae276c571ca37757ed8fdfec14feba25436fef29c2c513ac513a2dc80fdfab355183b5a773f080f8ba80d8e740b20744520f93fc57d139caba092041e78f3k7L1kJC8odrCrVCNngiASyNDetl/q6VdyOI1Wxn4o3I=', '3', 0),
('11.1718.004', 'Senia Sucipratiwi', '2cdc038e6de01a8a563ccf27237623eb31f9fb676cbd7d19c58427839b16f8fd1eb12c187dba1544eccd6f9696a8edc3602a174b86f14b040636bdd3e9751de16TycOig/po997Jvr3NS3nYkcyJ28Kj/ySlNFn/dlT/0=', '3', 0),
('11.1718.005', 'Deva Ayuni Akbari', '3b67ede924b4d2c760a7eef8728f538016add4db02f0ec3de1c604c2a6554f9c336c6396ac40d5e32a15b982d53d0131e3c88a48a7a2fc94032c73f8c016f8b1jNqK+3HNx2JJjk1GkgukkYlG2JU4dByq1dgzM0TBj8Q=', '4', 0),
('11.1718.006', 'Mitha Khoerunnissa', 'e9e7f4f8fca333c105cfb3bc91a76937cfad3631ad842767b6f55edd925f57aeb02b183993dc7510b14705c80577a5eea1941e67797b59a8d8c3a0f2f69ef21btKqW18d70hauY1fO2DztPUQEW3RgRdoYr6ITmyDAIbQ=', '4', 0),
('11.1718.007', 'Sekar Ayu Aprilyani', '7d2bb2de00f90e27b73fe2aeeaf81edbf6d56dd8f3034824e9e726a1a2229a80f26cfd8aa50f369dd16decbaacf96e9bfd4c958a799035a589ffc58621389d8dFNMjYugRXkTnPcP3ZJSnnASpvs3WzSDKg0/hc84uDg8=', '5', 0),
('11.1718.008', 'Negrita Tikasari Siti Aisyah', '313b25fb99348dc02b57afcc8a3247282926d4a278c9b5579164dde71458deb8a037fc0b5748c4e6c87c648986dd43d7887338503ccea913e753d0aa1390e8afINYCRJnZrK94I7phrJbD5yF9RDlC6bCoJkOL7aPAml4=', '5', 0),
('11.1718.009', 'Muchammad Yaasiin Asshobry', '337b6ed50319203abb713232351a23d037a583251b51813afe7a323c0cf7f786a1c6bfca80f71c6674a7036adf353b74fe2ddb3d1045ec2b105b9be73f0ffcb8JxYE4id/idkZeh27BASOssnkxphmdxt5gpKEQkdzCgA=', '6', 0),
('11.1718.010', 'Zaky Muhammad Yopi Rusyana', '41b181193ade2be8af8b0e33d0646d9d7bc54eee76b91926775dc6b24180a295229833ecfcdeb818bc49f6fb31888b1fcace6b95f566626ec03e86ffdd24ec66CiGuyDFAYnuiTmY/YKV8Ke+Su4yvZ9awpU+wcVOf5NY=', '6', 0),
('11.1718.011', 'Nadila Luthfiantini', 'bb281ea6cd196f2af81dfcc7bd37c4c9d6a570d833bfe24464045cf353a0e475c42328de4f725dd93c3cdfe3415c0ff41d5cbe8be7964c933ce272f218a8f30fwEg6s4Ho/723OBQHPYZLOhUWBDDcgnURJSlv3TAwn8k=', '7', 0),
('11.1718.012', 'Yulia Nuraini', 'a8a02a84bc81891edd5e149d9106d254324fbe1986617c60e914357e8770b82e1019c1798c11bc1798fa790b594aabb0c53c7d24a54fd14de8fb98f7adba8d45IaoU5U8cOlrIgOWBwQc437bjME+jepGSBzXyFdZ/JYc=', '7', 0),
('11.1718.013', 'Salsabila Syifa', '6981e962117c64e6d39eaadf639a540feb08ebe5d1550e3cfa8dacf7c3c74d6756c272f716de024a0b6057a0f904db67e9e9dcc8730f928ec1fc9e4e1967a637XMo7saUaJYgClCP/BzWvupSFZXrAJHGNwoI9XgmfQu8=', '9', 0),
('11.1718.014', 'Endang Retnani Arumsari', '2c36d336cebe5414a17a5493617ecaaeb2aa1284939cdba6d997c22b38784079b11d4499756dc32be911828107f9bf6b8d13613df4b48338053ff37696dbacf9zWrbjqgsxfKZfIzCEIVbgscwabry7BSujm8re1DaHXo=', '9', 0),
('11.1718.015', 'Arief Septian Wijayanto', '1fa0c369a612846798384d582da48839a92d17ac6433d599cd4dc08c2163b8734030575a0b40402ea940868e42e733f92e177dda2bd1e59fd29b3dbc67f7713ekBuNweXSZnb/dBQtzoEjD5zSDqNbnF4xQqXJGejRcL4=', '8', 0),
('11.1718.016', 'Muhammad Mugnisalam Magfira', '58c4a5c54e087aeec07e09e90330536be3312d256980b1f7bd9f1e6b6bfe2d8eae091806000da2031e5e0f4dd8807e0e50daf28fb10caa6f923cc7093c0144a0hTYwfl0SXoJev+ZhPKB5sccVjQph+YFAwaJp0XPVzho=', '8', 0),
('11.1718.017', 'Dwijo Topo Warsito', '994420bf9f9c93390ca485e6d74ad7d0be7f25a2291351721e2fc73a84fc7cda532e4b9aef815c8b478fdf79dd2f71790d3a1093a1a7b3a869374fcaae0472afvfxjChazfjRhvy0oKv7GZAb20kv89x1UqjSw10FFH+E=', '10', 0),
('11.1718.018', 'Dimas Mohammad Makdus', 'bc061507da7103db1845339ce2f116fddb1e39b7dc11b2211dc62bdbe4d8a0f692118c2641bf40a98169c25f2bb7d244f5560ec7772b641fbc16845bcc45854aKefrxXPIrPgnptfrSoDDdEjSuLobKMQmhV4fb6xuDbk=', '10', 0),
('11.1718.019', 'Dinna Fauziyyah', 'e86f4f212713977c51da9ec3b5019f10b066f30ce0df08f770c21cdd0b977bbb02fd2769eb1a5b28ae8419423c6687966f50c57515fb64a9f7ac060da2b73b5a1cfeOeDKjfI3yD+0Phgq4bfkbSqjtz91Km1CYvVzHP8=', '10', 0),
('11.1718.020', 'Muhamad Riyandi Ludyansyah', 'd04f69e9f2cf3f89258088d2f5873ca9cdd5ac78d91c94ff09c70bd4853f94f64aeaa04ada4712cf892a0d159eabffe654288a43ada2cb20618dd0a259ad593cq/AkWZEStRDK5d47UQmiQoSmBkQASEnKZQYNeUovNZc=', '10', 0),
('11.1718.021', 'Dalih Rusmana', 'f453276d1364dc27d61485417144c7b1263bb19da2b0536e9d6fa21f8aff45c7e5a7586d107b471fcf26beba9b0c35b8bd5956267cdda4700a296a5ff99eeeadBjwlyLEVnRRkodrbtvKkIMjmElBDXWtCMNeKr26Y32w=', '10', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pesan`
--

CREATE TABLE `tb_pesan` (
  `id_pesan` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(70) NOT NULL,
  `pesan` text NOT NULL,
  `tanggal_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_pesan`
--

INSERT INTO `tb_pesan` (`id_pesan`, `nama`, `email`, `pesan`, `tanggal_update`) VALUES
(1, '123123123213', '123213@gmail.com', '3213221', '2018-09-23 22:09:50');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indeks untuk tabel `tb_anggota`
--
ALTER TABLE `tb_anggota`
  ADD PRIMARY KEY (`NTA`);

--
-- Indeks untuk tabel `tb_fakultas`
--
ALTER TABLE `tb_fakultas`
  ADD PRIMARY KEY (`id_fakultas`);

--
-- Indeks untuk tabel `tb_info`
--
ALTER TABLE `tb_info`
  ADD PRIMARY KEY (`id_info`);

--
-- Indeks untuk tabel `tb_jabatan`
--
ALTER TABLE `tb_jabatan`
  ADD PRIMARY KEY (`id_jabatan`);

--
-- Indeks untuk tabel `tb_jurusan`
--
ALTER TABLE `tb_jurusan`
  ADD PRIMARY KEY (`id_jur`);

--
-- Indeks untuk tabel `tb_pab`
--
ALTER TABLE `tb_pab`
  ADD PRIMARY KEY (`id_PAB`);

--
-- Indeks untuk tabel `tb_pendaftar`
--
ALTER TABLE `tb_pendaftar`
  ADD PRIMARY KEY (`NIM`);

--
-- Indeks untuk tabel `tb_pengembang`
--
ALTER TABLE `tb_pengembang`
  ADD PRIMARY KEY (`id_pengembang`);

--
-- Indeks untuk tabel `tb_pengurus`
--
ALTER TABLE `tb_pengurus`
  ADD PRIMARY KEY (`NTP`);

--
-- Indeks untuk tabel `tb_pesan`
--
ALTER TABLE `tb_pesan`
  ADD PRIMARY KEY (`id_pesan`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_fakultas`
--
ALTER TABLE `tb_fakultas`
  MODIFY `id_fakultas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `tb_info`
--
ALTER TABLE `tb_info`
  MODIFY `id_info` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `tb_pengembang`
--
ALTER TABLE `tb_pengembang`
  MODIFY `id_pengembang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tb_pesan`
--
ALTER TABLE `tb_pesan`
  MODIFY `id_pesan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
