<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class firebase {

  public function __construct()
  {
    $this->ci =& get_instance();
  }

  public function send($params = ''){
    return $this->_call(FIREBASE_BASE_URL."/send",$params);
  }

  private function _call($url,$params){

    $ch = curl_init(); 
    $curl_options = array(
        CURLOPT_URL => $url,
        CURLOPT_HTTPHEADER => [
          'Authorization:key='.FIREBASE_API_KEY.":".FIREBASE_SECRET, 
          'Content-Type:application/json',
        ],
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_POST => 1,   
        );
    curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($params));

    curl_setopt_array($ch, $curl_options);
    
    $result = curl_exec($ch);
    $info = curl_getinfo($ch);

    if($result === FALSE){
      throw new Exception('CURL Error: ' . curl_error($ch), curl_errno($ch));
    }else{
      return $result_array = $result;
    }
  }

}


