<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class linenotify {

  	public static $curlOptions = array();	

  	const LINE_NOTIFY_BASE_URL = 'https://notify-api.line.me/api/notify';
   
   	public static function getBaseUrl()
  	{
    	return Linenotify::LINE_NOTIFY_BASE_URL;
  	}

    public static function post($url,$params){
      return self::call($url,$params);
    }

    public static function call($url,$data){
      $ch = curl_init();
      $curl_options = array(
        CURLOPT_URL => $url,
        CURLOPT_HTTPHEADER => array(
          'Content-Type: application/x-www-form-urlencoded',
          'Authorization: Bearer '.$data['token'],
        ),
        CURLOPT_RETURNTRANSFER => 1,

        );

      $curl_options[CURLOPT_POST] = 1;

      curl_setopt($ch, CURLOPT_POSTFIELDS,"message=".$data['message']);
      curl_setopt_array($ch, $curl_options);
      $result = curl_exec($ch);
      $info = curl_getinfo($ch);

      if($result === FALSE){
        throw new Exception('CURL Error: ' . curl_error($ch), curl_errno($ch));
      }else{
       return $result_array = $result;
      }
    }

    public static function send($params){
      if(!isset($params['token'])){
        return "Mohon isi token";
      }elseif(!isset($params['message'])){
        return "Mohon isi pesan";
      }else{
      return Linenotify::post(
        Linenotify::getBaseUrl(),
        $params
      );
      }
    } 

}


