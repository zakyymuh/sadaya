<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {
	public function __construct() {
		parent::__construct();
		if(!$this->session->NTA){
			redirect('anggota/auth');
		}
	}
	public function index(){
		$this->load->view('anggota/header',['title'=>'Beranda']);
		$this->load->view('anggota/sidebar');
		$this->load->view('anggota/index');
		$this->load->view('anggota/footer');
	}
	public function pengembangan(){
		$this->load->view('pab/header');
		$this->load->view('pab/sidebar');
		$this->load->view('404');
		$this->load->view('pab/footer');
	}
}