<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model("Model_anggota");
		$this->load->library("encryption");
	}

	public function index(){
		if($this->input->post('SUBMIT')){
			$NTA = $this->input->post('NTA');
			$password = $this->input->post('password');

			$data = array('NTA'=>$NTA);

    		$res = $this->Model_anggota->get_by($data);
    		if($res){
    			$pass_from_db = $this->encryption->decrypt($res->password);
    			if($password == $pass_from_db){
    				$session_data = array(
    					'NTA' => $NTA,
    					'nama' => $res->nama,
    				);
    				$this->session->set_userdata($session_data);
    				redirect(base_url('anggota/beranda'));
    			}
				else{
					$this->session->set_flashdata('fail','Username dan password anda
					tidak sesuai!');
				}
			}else{
				$this->session->set_flashdata('fail','Username dan password anda
					tidak sesuai!');
			}
			redirect(('anggota/auth'),'refresh');
		}else{
			$this->load->view('anggota/login');
		}
	}
	public function logout(){
		$this->session->unset_userdata(['NTA','nama']);
		$this->session->set_flashdata('info','Anda berhasil logout!');
		redirect(base_url('anggota/auth'));
	}
}
