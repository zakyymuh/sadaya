<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {
	public function __construct() {
		parent::__construct();
		if(!$this->session->id_admin){
			redirect('admin/auth');
		}
		$this->load->model("Model_admin");
	}
	public function index(){
		$data['menu'] = 'beranda';
		$data['admin'] = $this->Model_admin->count_all();
		
		$this->load->model("Model_pengurus");
		$data['pengurus'] = $this->Model_pengurus->count_all();

		$this->load->model("Model_PAB");
		$data['pab'] = $this->Model_PAB->count_all();

		$this->load->model("Model_pengembang");
		$data['pengembang'] = $this->Model_pengembang->count_all();

		$this->load->view('admin/header');
		$this->load->view('admin/sidebar',$data);
		$this->load->view('admin/index',$data);
		$this->load->view('admin/footer',['file'=>'']);
	}
	public function dev(){
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar',['menu'=>'']);
		$this->load->view('404');
		$this->load->view('admin/footer');
	}
	public function pab(){
		redirect(base_url('admin/pab'));
	}
}