<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PAB extends CI_Controller {
	public function __construct() {
		parent::__construct();
		if(!$this->session->id_admin){
			redirect('admin/auth');
		}
		$this->load->model("Model_PAB");
	}
	public function index(){
		$data['data'] = $this->Model_PAB->get_all();
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar',['menu'=>'pab']);
		$this->load->view('admin/index_PAB',$data);
		$this->load->view('admin/footer',['file'=>'']);
	}
	public function tambah(){
		if($this->input->post('submit')){
			$nama = $this->input->post('nama');
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			//encrypt
			$this->load->library('encryption');
			$password = $this->encryption->encrypt($password);

			//insert
			$data = array(
				'nama' => $nama,
				'username' => $username,
				'password' => $password
			);
			$res = $this->Model_PAB->insert($data);
					$this->session->set_flashdata('info','Berhasil menambah admin PAB baru');
			
			redirect(base_url('admin/pab/tambah'),'refresh');
		}
			$this->load->view('admin/header');
			$this->load->view('admin/sidebar');
			$this->load->view('admin/tambah_PAB');
			$this->load->view('admin/footer');

	}
}