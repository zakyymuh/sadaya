<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengembang extends CI_Controller {
	public function __construct() {
		parent::__construct();
		if(!$this->session->id_admin){
			redirect('admin/auth');
		}
		$this->load->model("Model_pengembang");
	}
	public function index(){
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar',['menu'=>'pengembang']);
		$this->load->view('admin/index_pengembang');
		$this->load->view('admin/footer',['file'=>'pengembang']);
	}

  public function edit($id = ''){

    $data['data'] = $this->Model_pengembang->get("$id");

    if(isset($data['data']->nama)){

      $this->load->view('admin/header');
      $this->load->view('admin/sidebar',['menu'=>'pengembang']);
      $this->load->view('admin/edit_pengembang',$data);
      $this->load->view('admin/footer',['file'=>'file_input']);

    }else{
        redirect(base_url('admin/pengembang'));
    }

  }

  public function submit_edit($id = ''){

    if($this->input->post('submit')){
      
      $filename = $this->_upload_file();
      
      if(!$filename){
        $filename = false;
      }

      $result = $this->Model_pengembang->update_data($filename,$id);

      if($result){
        $this->session->set_flashdata('sukses','Berhasil mengubah pengembang');
      }else{
        $this->session->set_flashdata('gagal','Gagal mengubah pengembang');
      }
      redirect(base_url('admin/pengembang'),'refresh');
    }else{
      redirect(base_url('admin/pengembang'));
    }
  }

	public function get_data_pengembang(){
		$draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $res = $this->Model_pengembang->order_by("id_pengembang","DESC")->get_all();
          
        $data = array();
        $no = $start+1;
        foreach($res as $r) {
            	$data[] = [
            	$no,
            	$r->nama,
            	$r->email,
            	"fb: ".$r->fb.
              "<br>twitter: ".$r->twitter.
              "<br>linkedin: ".$r->linkedin.
              "<br>instagram: ".$r->instagram,
            	"<img style='height:150px;width:150px;' src='".base_url()."assets/images/dev/".$r->foto."'>",
            	$r->bio,
              "<a href='".base_url()."admin/pengembang/edit/$r->id_pengembang'>
                <button class='btn btn-info btn-flat'>
                  <i class='fa fa-pencil'></i>
                </button>
              </a>&nbsp;".
              "<a href='".base_url()."admin/pengembang/hapus/$r->id_pengembang'>".
              "<button class='btn btn-danger btn-flat'><i class='fa fa-remove'></i></button></a>",
            ];
            $no++;
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $this->Model_pengembang->count_all(),
                 "recordsFiltered" => $this->Model_pengembang->count_all(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
    }

    public function hapus($id){
      $res = $this->Model_pengembang->delete($id);
      if($res){
        $this->session->set_flashdata('sukses','Berhasil menghapus pengembang');

      }else{
        $this->session->set_flashdata('gagal','Gagal menghapus pengembang');

      }
      redirect(base_url('admin/pengembang'),'refresh');
    }

    public function tambah(){
    	if($this->input->post('submit')){

        if($file_ext = $this->_upload_file()){
                  
          $result = $this->Model_pengembang->insert_data($file_ext);

          if($result){
            $this->session->set_flashdata('sukses','Berhasil menambah pengembang baru');
          }else{
            $this->session->set_flashdata('gagal','Gagal menambah pengembang baru');
          }

        }else{
          $this->session->set_flashdata('gagal','Gagal mengupload file baru');
           
        }
      
      redirect(base_url('admin/pengembang/tambah'),'refresh');

    	}else{

        $this->load->view('admin/header',['css'=>'file_input']);
  			$this->load->view('admin/sidebar',['menu'=>'pengembang']);
  			$this->load->view('admin/tambah_pengembang');
  			$this->load->view('admin/footer',['file'=>'file_input']);
	    }
    }

    private function _upload_file(){
      $this->load->library('upload');
        
      $filename = md5(date('is')).rand(1,5);
      $config['upload_path'] = './assets/images/dev/';
      $config['file_name'] = $filename;
      $config['allowed_types'] = 'jpg|png|jpeg';
      $config['max_size'] = '2048';
      
      $this->upload->initialize($config);

      if($data = $this->upload->do_upload('userfile')){
        $filename = $filename.$this->upload->data('file_ext');
        return $filename;
      }else{
        return false;
      }
    }
}