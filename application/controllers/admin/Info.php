<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Info extends CI_Controller {
	public function __construct() {
		parent::__construct();
		if(!$this->session->id_admin){
			redirect('admin/auth');
		}
		$this->load->model("Model_info");
	}
	public function index(){
		$data['menu'] = 'info';
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar',$data);
		$this->load->view('admin/index_info');
		$this->load->view('admin/footer',['file'=>'info']);
	}

	public function tambah(){
		if($this->input->post('submit')){
			if($file_ext = $this->_upload_file()){
				$result = $this->Model_info->insert_data($file_ext);
				if($result){
			    	//load library firebase
			    	$this->load->library('firebase');
			    	$params = [
			    		'condition' => "'info' in topics",
			    		'data' => [
			    			'message' => $this->input->post('judul'),
			    			'judul' => $this->input->post('judul'),
			    		],
			    	];
			    	$result = $this->firebase->send($params);
			    	
			    	$this->session->set_flashdata('info','Berhasil menambah info baru');
			    }else{
			        $this->session->set_flashdata('info','Gagal menambah info baru');
			    }
	        }else{
	        	$this->session->set_flashdata('info','Gagal mengupload file baru');
			    }

	    redirect(base_url('admin/info/tambah'),'refresh');
		}else{

			$data['menu'] = 'info';
			$this->load->view('admin/header',['css'=>'file_input']);
			$this->load->view('admin/sidebar',$data);
			$this->load->view('admin/tambah_info');
			$this->load->view('admin/footer',['file'=>'file_input']);
		}
	}

	public function edit($id){
		if($this->input->post('submit')){
			
			$filename = $this->_upload_file();
      
     		if(!$filename){
        		$filename = false;
      		}

      		$result = $this->Model_info->update_data($filename,$id);

     	if($result){
	        $this->session->set_flashdata('info','Berhasil mengedit info');
		}else{
	        $this->session->set_flashdata('info','Gagal mengedit info');
		}
		redirect(base_url('admin/info/'),'refresh');

		}else{
			$data['data'] = $this->Model_info->get($id);
			$this->load->view('admin/header',['css'=>'file_input']);
			$this->load->view('admin/sidebar',['menu'=>'info']);
			$this->load->view('admin/edit_info',$data);
			$this->load->view('admin/footer',['file'=>'file_input']);
		}
		}


	public function hapus($id){
		$res = $this->Model_info->delete($id);
		
		if($res){
	    	$this->session->set_flashdata('info','Berhasil menghapus info');
		}else{
	    	$this->session->set_flashdata('info','Gagal menghapus info');
		}
		redirect(base_url('admin/info'),'refresh');
	}
	
	public function get_data_info(){
		$draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $res = $this->Model_info->order_by("tanggal_update","DESC")->get_all();
          
        $data = array();
        $no = $start+1;
        foreach($res as $r) {
            	$data[] = [
            	$no,
            	$r->judul,
            	$r->foto,
            	$r->tag,
            	$r->tanggal_update, 
            	"<a href='".base_url()."admin/info/edit/$r->id_info'>
                	<button class='btn btn-info btn-flat'>
                  	<i class='fa fa-pencil'></i>
                	</button>
              		</a>&nbsp;".
              	"<a href='".base_url()."admin/info/hapus/$r->id_info'>".
            	"<button class='btn btn-danger btn-flat'><i class='fa fa-remove'></i></button></a>",
            	];
            $no++;
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $this->Model_info->count_all(),
                 "recordsFiltered" => $this->Model_info->count_all(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
    }

    private function _upload_file(){

    $this->load->library('upload');
        
      $filename = md5(date('is')).rand(1,5);
      $config['upload_path'] = './assets/uploads/';
      $config['file_name'] = $filename;
      $config['allowed_types'] = 'jpg|png|jpeg';
      $config['max_size'] = '2048';
      
      $this->upload->initialize($config);

      if($data = $this->upload->do_upload('userfile')){
        $filename = $filename.$this->upload->data('file_ext');
        return $filename;
      }else{
        return false;
      }
    }
}

// function send_notification ($tokens, $message) 
// // // { 
//  $url = 'https://fcm.googleapis.com/fcm/send'; 

// $isi = "Bismillah, Sadaya Juara. Yakin Pada jago main musik mah"; 
// $judul = "Sadaya Ajaib"; 

// $message = array( 
//     'message'  =>  $isi, 
//     'judul'  => $judul 
//    ); 
// $fields = array( 
//     'condition'  => "'info' in topics", 
//     'data' => $message 
//    ); 
// // echo json_encode($fields); 
// // exit(); 
// $headers = array( 
//   'Authorization:key=AAAAs3SXwAE:APA91bE13SMdhaDqc6pvZsD8rYR7iTmZkRaX0v6nscdRTzS7o-HE8ijzA158nG1IKyCmjBYoOKj1LdOUXjnFeKYSs-rTCy3RnS2fmzgUzLX01HUfeYDBX1PH-XoUW778Y0icR462ExDr', 
//   'Content-Type:application/json' 
// ); 
  
//  $ch = curl_init(); 
//  curl_setopt($ch, CURLOPT_URL, $url); 
//  curl_setopt($ch, CURLOPT_POST, true); 
//  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
//  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
//  curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);   
//  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
//  curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields)); 
//  $result = curl_exec($ch);            
//  if ($result === FALSE) { 
//  die('Curl failed: ' . curl_error($ch)); 
//  } 
//  curl_close($ch); 
//  echo $result; 
// // } 
// // echo send_notification("",""); 