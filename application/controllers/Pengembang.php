<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengembang extends CI_Controller {

	public function index() {   
		$this->load->model('Model_pengembang');
		$data['js'] = array(); 
		$css = array();  
		$data['data'] = $this->Model_pengembang->get_all();
		$this->load->view('beranda/layout/header', array('title' => 'Pengembang - Sadaya Unikom','secondary' => true, 'css' => $css));
		$this->load->view('beranda/pengembang', $data);
	}
	
}
