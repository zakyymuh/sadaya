<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class mobile extends CI_Controller {

	public function index() {   
		$data['js'] = array(); 
		$css = array();  

		$this->load->view('beranda/layout/header', array('title' => 'Sadaya Unikom', 'mobile'=> true, 'css' => $css));
		$this->load->view('mobile/home', $data);
	}

	
	public function divisi() {   
		$data['js'] = array(); 
		$css = array();  

		$this->load->view('beranda/layout/header', array('title' => 'Sadaya Unikom', 'mobile'=> true, 'css' => $css));
		$this->load->view('mobile/divisi', $data);
	}

	public function info() {   
		$data['js'] = array(); 
		$css = array();
		$this->load->model('Model_info');
		$data['data'] = $this->Model_info->get_all();

		$css = array();  

		$this->load->view('beranda/layout/header', array('title' => 'Sadaya Unikom', 'mobile'=> true, 'css' => $css));
		$this->load->view('mobile/info', $data);
	}

	public function about() {   
		$data['js'] = array(); 
		$css = array();  

		$this->load->view('beranda/layout/header', array('title' => 'Sadaya Unikom', 'mobile'=> true, 'css' => $css));
		$this->load->view('mobile/intro', $data);

	}
	
	public function dev(){
		$this->load->model('Model_pengembang');
		$data['data'] = $this->Model_pengembang->get_all();
		$css = array();  
		$this->load->view('beranda/layout/header', array('title' => 'Sadaya Unikom', 'mobile'=> true, 'css' => $css));
		$this->load->view('beranda/pengembang', $data);

	}

	public function kritik(){
		
		$data['js'] = array(); 
		$css = array();  

		$this->load->view('beranda/layout/header', array('title' => 'Sadaya Unikom', 'mobile'=> true, 'css' => $css));
		$this->load->view('mobile/home', $data);
	}
}
