<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model("Model_pengurus");
		$this->load->library("encryption");
	}

	public function index(){
		if($this->input->post('SUBMIT')){
			$NTP = $this->input->post('NTP');
			$password = $this->input->post('password');
			$data = array('NTP'=>"$NTP",'deleted'=>"0");
    		$res = $this->Model_pengurus->get_by($data);
    		if($res){
    			$pass_from_db = $this->encryption->decrypt($res->password);
    			if($password == $pass_from_db){
    				$session_data = array(
    					'NTP' => $NTP,
    					'nama' => $res->nama,
    					'jabatan' => $email
    				);
    				$this->session->set_userdata($session_data);
    				if($res->jabatan == 5){
						redirect('pengurus/humas/beranda');
    				}else
    					redirect(base_url('Admin/Beranda'));
    				}
				$this->session->set_flashdata('fail','Password anda tidak cocok '.$pass_from_db." ".$password);
			}
			else{
				$this->session->set_flashdata('fail','Username dan password anda
					tidak terdaftar!');
			}
			redirect(('pengurus/auth'),'refresh');
		}else{
			$this->load->view('pengurus/login');
		}
	}
	public function logout(){
		$this->session->unset_userdata('NTP','nama','jabatan');
		$this->session->set_flashdata('info','Anda berhasil logout!');
		redirect(base_url('pengurus/auth'));
	}
}
