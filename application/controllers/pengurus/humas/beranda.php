<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {
	public function __construct() {
		parent::__construct();
		if((!$this->session->NTP)&&($this->session->jabatan <> 5)){
			redirect('pengurus/auth');
		}
	}

	public function index(){
		$this->load->view('pengurus/humas/header');
		$this->load->view('pengurus/humas/sidebar');
		$this->load->view('pengurus/humas/index');
		$this->load->view('pengurus/humas/footer');
	}

	public function dev(){
		$this->load->view('pengurus/humas/header');
		$this->load->view('pengurus/humas/sidebar');
		$this->load->view('404');
		$this->load->view('pengurus/humas/footer');
	}
}