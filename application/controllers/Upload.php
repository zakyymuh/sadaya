<?php

class Upload extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->helper(array('form', 'url'));
                $this->load->library('upload');
        }

        public function index()
        {
                $this->load->view('upload', array('error' => ' ' ));
        }

        public function do_upload()
        {
                        $filename = md5(date('is')).rand(1,5);
            $config['upload_path'] = './assets/uploads/';
            $config['file_name'] = $filename;
                        $config['allowed_types'] = 'jpg|png';
                        $config['max_size'] = '2048';

                        $this->upload->initialize($config);

                if ( ! $this->upload->do_upload('userfile'))
                {
                        $error = array('error' => $this->upload->display_errors());

                        $this->load->view('upload', $error);
                }
                else
                {
                        $data = $this->upload->data();
                        echo $data['file_ext'];
                        // $data = array('upload_data' => $this->upload->data());
                        // echo $data['upload_data']['file_ext'];
                        // $this->load->view('upload_form', $data);
                }
        }
}
?>