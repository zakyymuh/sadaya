<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {

	public function index() {   
		$data['js'] = array(); 
		$css = array();  

		$this->load->view('beranda/layout/header', array('title' => 'SADAYA UNIKOM', 'css' => $css));
		$this->load->view('beranda/main', $data);
	}

	public function pesan(){
		$this->load->model('Model_pesan');
		$result = $this->Model_pesan->send();
		if($result){
			$this->session->set_flashdata('info','Pesan anda telah kami terima, terimakasih');
		}else{
			$this->session->set_flashdata('info','Whoops, pesan anda mengalami gangguan');
		}

		redirect(base_url('beranda'),'refresh');
	}
}
