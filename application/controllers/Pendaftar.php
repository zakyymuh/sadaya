<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendaftar extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model("Model_pendaftar");
	}
	public function test(){
		$this->load->view('pendaftar/test');
	}
	public function index()
	{
		$this->load->view('pendaftar/form_pendaftaran');
	}
	
	public function cekNIM(){
		$NIM = $this->input->post("NIM");
		$result = $this->Model_pendaftar->get($NIM);
		if(isset($result->NIM)){
			$data['msg'] = false;
		}else{
			$data['msg'] = true;
		}
		echo json_encode($data);
	}

	public function daftar(){
		$this->load->library('upload');
		if($this->input->post('submit')){
		
		$this->form_validation->set_rules('NIM', 'NIM', 'required|is_unique[tb_pendaftar.NIM]');

		if($this->form_validation->run() == FALSE){
			redirect(base_url('pendaftar'));
		}
			
			$tanggal = $this->input->post('tanggal_lahir');
			$d = substr($tanggal,0,2);
			$m = substr($tanggal,3,2);
			$y = substr($tanggal,-4);
			$tanggal = $y."-".$m."-".$d;

			$NIM = $this->input->post('NIM');
			$kode = substr($NIM, 1,2);
			$jurusan = $this->_jurusan($kode);
			

			$filename = md5(date('is')).rand(1,5);
            $config['upload_path'] = './assets/uploads/';
            $config['file_name'] = $filename;
			$config['allowed_types'] = 'jpg|png';
			$config['max_size'] = '2048';

			$this->upload->initialize($config);

			if(!$this->upload->do_upload('userfile')){
				$error = array('error' => $this->upload->display_errors());				
				redirect(base_url('pendaftar'));
			}else{
				$file_ext = $this->upload->data();
				$sosmed = $this->input->post('sosmed');
				$data = array(
				"NIM" => $NIM,
				"nama" => $this->input->post('nama_lengkap'),
				"panggilan" => $this->input->post('nama_panggilan'),
				"jenis_kelamin" => $this->input->post('jenis_kelamin'),
				"email" => $this->input->post('email'),
				"agama" => $this->input->post('agama'),
				"tempat_lahir" => $this->input->post('tempat_lahir'),
				"tanggal_lahir" => $tanggal,
				"kontak" => $this->input->post('kontak'),
				"kontak_lain" => $this->input->post('kontak_lain'),
				"alamat" => $this->input->post('alamat_rumah'),
				"alamat_lain" => $this->input->post('alamat_lain'),
				"sosial_media" => $sosmed,
				"sosial_media_lain" => $this->input->post('sosmed_lain'),
				"jurusan" => $this->input->post('jurusan'),
				"bakat" => $this->input->post('bakat'),
				"minat" => $this->input->post('minat'),
				"alasan" => $this->input->post('alasan'),
				"foto" => $filename.$file_ext['file_ext'],
				"jurusan" => $jurusan,
				);
				
				$this->Model_pendaftar->insert($data);
				$nama = (!empty($this->input->post('nama_panggilan')))?$this->input->post('nama_panggilan'):$this->input->post('nama_lengkap');
					
				$this->load->library('linenotify');
				$params = array(
					"message"=>" ".$nama." baru saja mendaftar dengan idline: ".$sosmed,
					"token"=>'sxNHLgwz7YbENdQOiAVvzhQB4HROy38FUby0opBwcuj',
				);
				$res = $this->linenotify->send($params);
				

				$this->load->view('pendaftar/berhasil',array('nama'=>$nama));	
				// exit();
			}
		}else{
			redirect(base_url('pendaftar'));
		}
	}

	private function _jurusan($kode){
		switch ($kode) {
				case '01':
					$jurusan = "Teknik Informatika";
					break;
				case '02':
				case '08':
					$jurusan = "Teknik Komputer";
					break;
				case '03':
					$jurusan = "Teknik Industri";
					break;
				case '31':
					$jurusan = "Teknik Elektro";
					break;
				case '04':
					$jurusan = "Teknik Arsitektur";
					break;
				case '05':
					$jurusan = "Sistem Informasi";
					break;
				case '06':
					$jurusan = "Perencanaan Wilayah dan Kota";
					break;
				case '09':
					$jurusan = "Manajemen Informatika";
					break;
				case '10':
					$jurusan = "Komputerisasi Akuntansi";
					break;
				case '11':
				case '13':
					$jurusan = "Akuntansi";
					break;
				case '12':
					$jurusan = "Manajemen";
					break;
				case '14':
					$jurusan = "Manajemen Pemasaran";
					break;
				case '15':
					$jurusan = "Keuangan dan Perbankan";
					break;
				case '16':
					$jurusan = "Ilmu Hukum";
					break;
				case '18':
					$jurusan = "Ilmu Komunikasi";
					break;
				case '43':
					$jurusan = "Hubungan Internasional";
					break;
				case '17':
					$jurusan = "Ilmu Pemerintahan";
					break;
				case '34':
					$jurusan = "Sekretaris Eksekutif";
					break;
				case '33':
					$jurusan = "Public Relations";
					break;
				case '19':
				case '21':
					$jurusan = "Desain Komunikasi Visual";
					break;
				case '20':
					$jurusan = "Desain Interior";
					break;
				case '37':
				case '39':
					$jurusan = "Sastra Inggris";
					break;
				case '38':
					$jurusan = "Sastra Jepang";
					break;
				case '30':
					$jurusan = "Teknik Sipil";
					break;
				default:
					$jurusan = "";
					break;
			}

		return $jurusan;
	}
}
