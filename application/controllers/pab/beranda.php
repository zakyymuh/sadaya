<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {
	public function __construct() {
		parent::__construct();
		if(!$this->session->id_PAB){
			redirect('pab/auth');
		}
	}
	public function index(){
		$this->load->model('Model_pendaftar');
		$data['pendaftar'] = $this->Model_pendaftar->count_all();
			
		$today = date('Y-m-d');
		$where = [
			'LEFT(tanggal_daftar,10)'=>$today
		];

		$data['today'] = $this->Model_pendaftar->count_by($where);

		$data['list'] = $this->Model_pendaftar->order_by("tanggal_daftar","DESC")->limit(10)->get_all();

		// $this->load->model('Model_absensi');
		// $data['absensi'] = $this->Model_absensi->count_all();
		// $data['absensi'] = 0;

		$this->load->model('Model_pelantikan');
		// $data['pelantikan'] = $this->Model_pelantikan->count_all();
		$data['pelantikan'] = 0;
		$this->load->view('pab/header',['title'=>'Beranda']);
		$this->load->view('pab/sidebar',['menu'=>'beranda']);
		$this->load->view('pab/index',$data);
		$this->load->view('pab/footer');
	}
	public function absensi(){
		$this->load->view('pab/header');
		$this->load->view('pab/sidebar');
		$this->load->view('404');
		$this->load->view('pab/footer');
	}
	public function password(){
		$this->load->view('pab/header');
		$this->load->view('pab/sidebar');
		$this->load->view('404');
		$this->load->view('pab/footer');
	}
	public function pelantikan(){
		$this->load->view('pab/header');
		$this->load->view('pab/sidebar');
		$this->load->view('404');
		$this->load->view('pab/footer');
	}
}