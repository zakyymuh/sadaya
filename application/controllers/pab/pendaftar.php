<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendaftar extends CI_Controller {
	public function __construct() {
		parent::__construct();
		if(!$this->session->id_PAB){
			redirect('pab/auth');
		}
		$this->load->model('Model_pendaftar');
	}
	public function index(){
		$this->load->view('pab/header');
		$this->load->view('pab/sidebar',['menu'=>'pendaftar']);
		$this->load->view('pab/index_pendaftar');
		$this->load->view('pab/footer',['address'=>'pendaftar']);
	}
	public function get_data_pendaftar(){
		$draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $res = $this->Model_pendaftar->order_by("tanggal_daftar","DESC")->get_all();
          
        $data = array();
        $no = $start+1;
        foreach($res as $r) {
            	$data[] = [
            	$no,
            	$r->NIM,
            	$r->nama,
            	$r->jurusan,
            	$r->sosial_media,
            	"<center><img class='gambar_pab' src='".base_url()."assets/uploads/$r->foto'></center>"
            	];
            $no++;
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $this->Model_pendaftar->count_all(),
                 "recordsFiltered" => $this->Model_pendaftar->count_all(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
    }
}