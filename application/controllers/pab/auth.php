<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model("Model_PAB");
		$this->load->library("encryption");
	}

	public function index(){
		if($this->input->post('SUBMIT')){
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			$data = array('username'=>"$username");

    		$res = $this->Model_PAB->get_by($data);
    		if($res){
    			$pass_from_db = $this->encryption->decrypt($res->password);
    			if($password == $pass_from_db){
    				$session_data = array(
    					'username' => $username,
    					'nama' => $res->nama,
    					'id_PAB' => $res->id_PAB
    				);
    				$this->session->set_userdata($session_data);
    				redirect(base_url('pab/beranda'));
    			}
				else{
					$this->session->set_flashdata('fail','Username dan password anda
					tidak sesuai!');
				}
			}
			redirect(('pab/auth'),'refresh');
		}else{
			$this->load->view('pab/login');
		}
	}
	public function logout(){
		$this->session->unset_userdata(['nama','username','id_PAB']);
		$this->session->set_flashdata('info','Anda berhasil logout!');
		redirect(base_url('pab/auth'),'refresh');
	}
}
