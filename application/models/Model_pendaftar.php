<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pendaftar extends MY_Model{
	public $_table = "tb_pendaftar";
	public $primary_key = "NIM";

	public $before_create = array( 'timestamps');

    protected function timestamps($data){
        $data['tanggal_daftar'] = date('Y-m-d H:i:s');
        return $data;
    }
}
?>