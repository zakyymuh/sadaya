<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pesan extends MY_Model{
	public $_table = "tb_pesan";
	public $primary_key = "id_pesan";

	public $before_create = array('timestamps');

    protected function timestamps($data){
        $data['tanggal_update'] = date('Y-m-d H:i:s');
        return $data;
    }

    public function send(){
    	$data = $this->_data();
    	if($this->insert($data)){
    		return true;
    	}else{
    		return false;
    	}
    }

    private function _data(){
    	
    	$name = $this->input->post('nama');
    	$email = $this->input->post('email');
    	$pesan = $this->input->post('pesan');

    	$data = [
    		'nama' => $name,
    		'email' => $email,
    		'pesan' => $pesan
    	];
    	return $data;
    }
}
?>