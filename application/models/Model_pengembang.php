<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pengembang extends MY_Model{
	public $_table = "tb_pengembang";
	public $primary_key = "id_pengembang";
    public $soft_delete = TRUE;

	public function insert_data($filename){
		
		$data = $this->_data($filename);

		if($this->insert($data)){
			return true; 
		}else{
			return false;
		}
	}

	public function update_data($filename,$id_pengembang){

		$data = $this->_data($filename);
		if($this->update($id_pengembang,$data)){
			return true;
		}else{
			return false;
		}
	}

	private function _data($filename){

		$nama = $this->input->post('nama');
		$email = $this->input->post('email');
		$fb = $this->input->post('fb');
		$tw = $this->input->post('tw');
		$ig = $this->input->post('ig');
		$li = $this->input->post('li');
		$bio = $this->input->post('bio');
		$jobdesk = $this->input->post('jobdesk');
		$foto = $this->input->post('foto');

		$data = [
			'nama' => $nama,
			'email' => $email,
			'fb' => $fb,
			'twitter' => $tw,
			'instagram' => $ig,
			'linkedin' => $li,
			'bio' => $bio,
			'jobdesk' => $jobdesk
		];

		if($filename){
			$data['foto'] = $filename;
		}

		return $data;
	}
	






}

?>