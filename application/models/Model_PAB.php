<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_PAB extends MY_Model{
	public $_table = "tb_pab";
	public $primary_key = "id_PAB";

	public $before_create = array('generate');

    protected function generate($data){
 		$max = $this->db->select('max(right(id_PAB,1)) as new')->get('tb_PAB')->result()[0]->new;
 		$data['id_PAB'] = "PAB".($max+1);
        return $data;
    }
}
?>