<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_info extends MY_Model{
	public $_table = "tb_info";
	public $primary_key = "id_info";

	public $soft_delete = true;
	public $before_create = array( 'timestamps');

    protected function timestamps($data){
        $data['tanggal_update'] = date('Y-m-d H:i:s');
        return $data;
    }

	public function insert_data($filename){		
		$data = $this->_data($filename);

		if($this->insert($data)){
			return true; 
		}else{
			return false;
		}
	}

	public function update_data($filename,$id){		
		$data = $this->_data($filename);

		if($this->update($id,$data)){
			return true; 
		}else{
			return false;
		}
	}

	private function _data($filename){

		$judul = $this->input->post('judul');
		$tag = $this->input->post('tag');
		$data = [
			'judul' => $judul,
			'tag' => $tag,
		];

		if($filename){
			$data['foto'] = $filename;
		}

		return $data;
	}
}
?>