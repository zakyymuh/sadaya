<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'beranda';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE; 
$route['daftar'] = 'pendaftar';

$route['pengurus'] = 'pengurus/auth';
$route['pab'] = 'pab/auth';
$route['admin'] = 'admin/auth';
$route['anggota'] = 'anggota/auth';