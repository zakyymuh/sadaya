<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function en_pass($pass) {
	return $this->encryption->encrypt($pass);
}
function ver_pass($pass,$data){
}
function transform_datetime($date){
	// 2018-06-02;
	$y = substr($date, 0,4);
	$m = substr($date, 5,2);
	$d = substr($date, 8,2);
	$h = substr($date, 11,2);
	$i = substr($date, 14,2);
	return $d."-".$m."-".$y." $h:$i";
}