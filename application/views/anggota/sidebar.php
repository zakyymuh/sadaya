      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?=base_url()?>assets/images/logo.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?=$this->session->nama;?></p><i class="fa fa-circle text-success"></i> Online
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li>
              <a href="<?=base_url('anggota/beranda')?>">
                <i class="fa fa-dashboard"></i> <span>Beranda</span></i>
              </a>
            </li>
            <li><li>
              <a href="<?=base_url('anggota/pengembangan')?>">
                <i class="fa fa-envelope"></i> <span>Pengajuan Surat</span></i>
              </a>
            </li>
            <li>
              <a href="<?=base_url('anggota/beranda/pengembangan')?>">
                <i class="fa fa-black-tie"></i> <span>JOB</span></i>
              </a>
            </li>
            <li>
              <a href="<?=base_url('anggota/beranda/pengembangan')?>">
                <i class="fa fa-check"></i> <span>Absensi Latihan</span></i>
              </a>
            </li>
            <li>
              <a href="<?=base_url('anggota/beranda/pengembangan')?>">
                <i class="fa fa-lock"></i> <span>Ubah password</span>
              </a>
            </li>
            <li>
              <a href="<?=base_url('anggota/beranda/pengembangan')?>">
                <i class="fa fa-lock"></i> <span>Profile</span>
              </a>
            </li>
            <li>
              <a href="<?=base_url('anggota/auth/logout')?>">
                <i class="fa fa-sign-out"></i> <span>Log Out</span>
              </a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>