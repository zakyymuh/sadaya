      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?=base_url()?>assets/images/logo.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?=$this->session->nama;?></p><i class="fa fa-circle text-success"></i> Online
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li>
              <a href="<?=base_url('pengurus/humas/beranda')?>">
                <i class="fa fa-dashboard"></i> <span>Beranda</span></i>
              </a>
            </li>
            <li><li>
              <a href="<?=base_url('pengurus/humas/beranda/dev')?>">
                <i class="fa fa-calendar"></i> <span>Event</span></i>
              </a>
            </li><li>
              <a href="<?=base_url('pengurus/humas/beranda/dev')?>">
                <i class="fa fa-image"></i> <span>Galeri</span></i>
              </a>
            </li>
            <li>
              <a href="<?=base_url('pengurus/humas/beranda/dev')?>">
                <i class="fa fa-image"></i> <span>Tampilan</span></i>
              </a>
            </li>
            <li>
              <a href="<?=base_url('pengurus/humas/beranda/dev')?>">
                <i class="fa fa-lock"></i> <span>Password</span></i>
              </a>
            </li>
            <li>
              <a href="<?=base_url('pengurus/auth/logout')?>">
                <i class="fa fa-sign-out"></i> <span>Log Out</span>
              </a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>