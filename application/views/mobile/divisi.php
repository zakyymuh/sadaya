 

    <!-- ========================== start section divisi ============================= -->
    <div class="section divisi" id="divisi">
      <div class="container">
        <div class="section-title">Divisi Sadaya</div> 
        <div class="section-sub-text">Berikut merupakan divisi sadaya unikom.</div>
        <div class="box-container">
   

          <div class="box content">
            <div class="logo"><img style="width: 150px;" src="<?php echo base_url() ?>assets/images/angklung.png"></div>
            <div class="title">Angklung</div>
            <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut.</div> 
          </div>  

          <div class="box content">
            <div class="logo"><img style="width: 150px;" src="<?php echo base_url() ?>assets/images/sadaya.png"></div>
            <div class="title">Rampak Kendang</div> <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut.</div> 
          </div> 

          <div class="box content">
            <div class="logo"><img style="width: 150px;" src="<?php echo base_url() ?>assets/images/tari.png"></div>
            <div class="title">Tari</div> <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut.</div> 
          </div>  

          <div class="box content">
            <div class="logo"><img style="width: 150px;" src="<?php echo base_url() ?>assets/images/sadaya.png"></div>
            <div class="title">Perkusi</div><div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut.</div>  
          </div> 

          <div class="box content">
            <div class="logo"><img style="width: 150px;" src="<?php echo base_url() ?>assets/images/gamelan.png"></div>
            <div class="title">Gamelan</div> <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut.</div> 
          </div>   

        </div><!-- end box-container --> 
        </div>
      </div><!-- en container -->
    </div><!-- end section divisi --> 