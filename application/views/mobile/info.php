<!--
@zaky
@update
  dyanmic data info from databse
@end
-->

<div class="section section-news"> 
  <div class="container">
    <div class="section-title">Info Sadaya</div> 
    <div class="section-sub-text">Berikut merupakan Informasi sadaya unikom.</div>
    
    <div class="main-content">
      
      <div class="row">
        <?php
        $no = 1;
          foreach ($data as $v) {
            if(($no % 5) == 0){
              echo "<div class='col-md-8'>";
              echo "<div class='box horizontal'>";
            }else{
              echo "<div class='col-md-4'>";
              echo "<div class='box small'>";
              
            }
            ?>
                <a  href="#" class="link-overlay"></a>
                <div class="banner-img"><img src="<?php echo base_url() ?>assets/uploads/<?=$v->foto;?>"></div>
                <div class="content">
                  <div class="title"><?=$v->judul;?></div>
                  <div class="info">
                    <span class="author"><i class="fas fa-info-circle "></i> <?=$v->tag;?></span>
                    <span class="date"><i class="fas fa-clock"></i> <?=$v->tanggal_update;?></span>
                  </div>
                </div> 
              </div>
            <?php
              echo "</div>";
              $no++;
            }
        ?>
    
      </div><!-- end row -->
    </div><!-- end box container --> 
  </div><!-- end container --> 
</div><!-- end secttion --> 