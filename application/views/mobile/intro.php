 

    <!-- ========================== start section introduction ============================= -->
    <div class="section intro" id="pengertian">
      <div class="container">
        <div class="box-container">
          <div class="box content">
            <div class="content-title">Perkenalkan Sadaya Unikom<span class="content-title-icon"><div><i class="fa fa-handshake"></i></div></span></div>
            <div class="content-body">
             Saung Budaya (SADAYA) UNIKOM merupakan salah satu Unit Kegiatan Mahasiswa yang ada di Universitas Komputer Indonesia yang bergerak di bidang pelestarian dan perkembangan seni budaya khususnya kesenian tradisional Indonesia.  Sejak berdiri pada tanggal 25 Februari 2008, SADAYA telah mengembangkan berbagai kesenian tradisional & kontemporer diantaranya seperti paduan alat musik angklung, tarian-tarian daerah, perkusi dan rampak kendang. SADAYA terletak di kampus 2 Lantai 5 UNIKOM.
            </div>
            <div class="content-footer">
               
            </div>
          </div>
          <div class="box illustration">
            <img src="<?php echo base_url() ?>assets/images/logo.png">
          </div>
        </div>
      </div><!-- en container -->
    </div><!-- end section introduction -->
 
  