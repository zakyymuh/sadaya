      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?=base_url()?>assets/images/logo.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?=$this->session->nama;?></p><i class="fa fa-circle text-success"></i> Online
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class='<?=($menu == "beranda")?"active":"";?>'>
              <a href="<?=base_url('pab/beranda')?>">
                <i class="fa fa-dashboard"></i> <span>Beranda</span></i>
              </a>
            </li>
            <li>
            <li class='<?=($menu == "pendaftar")?"active":"";?>'>
              <a href="<?=base_url('pab/pendaftar')?>">
                <i class="fa fa-clone"></i> <span>Pendaftar</span></i>
              </a>
            </li>
            <li>
              <a href="<?=base_url('pab/beranda/absensi')?>">
                <i class="fa fa-check"></i> <span>Absensi</span></i>
              </a>
            </li>
            <li>
              <a href="<?=base_url('pab/beranda/pelantikan')?>">
                <i class="fa fa-graduation-cap"></i> <span>Pelantikan</span></i>
              </a>
            </li>
            <li>
              <a href="<?=base_url('pab/beranda/password')?>">
                <i class="fa fa-lock"></i> <span>Ubah password</span>
              </a>
            </li>
            <li>
              <a href="<?=base_url('pab/auth/logout')?>">
                <i class="fa fa-sign-out"></i> <span>Log Out</span>
              </a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>