	<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Beranda
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?=base_url('beranda');?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Pendaftar</span>
                  <span class="info-box-number"><?=$pendaftar;?> Orang</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-check"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Pendaftar Hari ini</span>
                  <span class="info-box-number"><?=$today;?></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-graduation-cap"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Pelantikan</span>
                  <span class="info-box-number"><?=$pelantikan;?> Orang</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
          </div>
          <div class="row">
            <div class="col-md-6">
                  <!-- USERS LIST -->
                  <div class="box box-danger">
                    <div class="box-header with-border">
                      <h3 class="box-title">Pendaftar terbaru</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body no-padding">
                      <ul class="users-list clearfix">
                      <?php
                        foreach ($list as $l) {
                          ?>
                            <li>
                              <img src="<?=base_url();?>assets/uploads/<?=$l->foto;?>" style='width: 100px; height: 100px;' alt="User Image">
                              <a class="users-list-name" href="#"><?=$l->nama;?></a>
                              <span class="users-list-date"><?=$l->jurusan;?></span>
                            </li>  
                          <?php
                        }
                      ?>
                        
                      </ul><!-- /.users-list -->
                    </div><!-- /.box-body -->
                  </div><!--/.box -->
                </div>
          </div>
         

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

