
   
  
     <div class="section divisi light" id="divisi">
       <div class="container">
         <div class="section-title">Pengembang Sadaya</div> 
         <div class="section-sub-text">Pengembang Website Sadaya Unikom.</div>
         <div class="box-container">
         <?php
            foreach ($data as $val) {
              ?>

                <div class="box content">
                 <div class="logo rounded"><img src="<?php echo base_url();?>assets/images/dev/<?=$val->foto;?>"></div>
                 <div class="title"><?=$val->nama;?></div>
                 <div class="info">
                   <span class="sosmed-list">
                     <a class="" href="<?=$val->email;?>"><i class="fa fa-envelope"></i></a>
                     <a class="linkedin-color" href="https://id.linkedin.com/in/<?=$val->linkedin;?>"><i class="fab fa-linkedin"></i></a>
                     <a class="facebook-color" href="https://facebook.com/<?=$val->fb;?>"><i class="fab fa-facebook"></i></a>
                     <a class="twitter-color" href="https://twitter.com/<?=$val->twitter;?>"><i class="fab fa-twitter"></i></a>
                     <a class="instagram-color" href="https://www.instagram.com/<?=$val->instagram;?>"><i class="fab fa-instagram"></i></a>
                   </span>
                   <span class="jabatan"><?=$val->jobdesk;?></span>
                 </div>
                 <div class="text"><?=$val->bio;?></div> 
               </div>  

              <?php
            }
         ?>
         </div><!-- end box-container --> 
         </div>
       </div><!-- en container -->
     </div><!-- end section introduction -->


    <?php $this->load->view('beranda/layout/footer'); ?>