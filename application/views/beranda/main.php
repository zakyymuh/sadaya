
    <div class="banner" style="background-image: url('<?php echo base_url() ?>assets/images/pallet.jpg'); ">
      <div class="layer-overlay"></div>
      <div class="banner-container">
        <h1 class="text-banner">Saung Budaya Universitas Komputer Indonesia</h1>
        <span class="text-banner-detail">Juara 2 Angklung tingkat nasional di Lomba Musik Angklung Padaeng X tahun 2018.</span>
           
          <span class="text-banner-footer">
            <a href="<?php echo site_url('daftar') ?>" class="btn-md btn-primary with-border btn-href">Gabung Sekarang Juga <span class="btn-icon animate"><i class="fa fa-users"></i></span></a>
          </span> 
      </div><!-- end banner container -->
    </div><!-- end banner -->

    <!-- ========================== start section introduction ============================= -->
    <div class="section intro" id="pengertian">
      <div class="container">
        <div class="box-container">
          <div class="box content">
            <div class="content-title">Perkenalkan Sadaya Unikom<span class="content-title-icon"><div><i class="fa fa-handshake"></i></div></span></div>
            <div class="content-body">
             Saung Budaya (SADAYA) UNIKOM merupakan salah satu Unit Kegiatan Mahasiswa yang ada di Universitas Komputer Indonesia yang bergerak di bidang pelestarian dan perkembangan seni budaya khususnya kesenian tradisional Indonesia.  Sejak berdiri pada tanggal 25 Februari 2008, SADAYA telah mengembangkan berbagai kesenian tradisional & kontemporer diantaranya seperti paduan alat musik angklung, tarian-tarian daerah, perkusi dan rampak kendang. SADAYA terletak di kampus 2 Lantai 5 UNIKOM.
            </div>
            <div class="content-footer">
              <a href="<?php echo base_url() ?>" class="btn-md btn-primary btn-href"> 
                Pelajari Selengkapnya <span class="btn-icon animate"><i class="fa fa-long-arrow-alt-right"></i></span>
              </a>
            </div>
          </div>
          <div class="box illustration">
            <img src="<?php echo base_url() ?>assets/images/logo.png">
          </div>
        </div>
      </div><!-- en container -->
    </div><!-- end section introduction -->

    <!-- ========================== start section divisi ============================= -->
    <div class="section divisi" id="divisi">
      <div class="container">
        <div class="section-title">Divisi Sadaya</div> 
        <div class="section-sub-text">Berikut merupakan divisi sadaya unikom.</div>
        <div class="box-container">
   

          <div class="box content">
            <div class="logo"><img style="width: 150px;" src="<?php echo base_url() ?>assets/images/angklung.png"></div>
            <div class="title">Angklung</div>
            <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut.</div> 
          </div>  

          <div class="box content">
            <div class="logo"><img style="width: 150px;" src="<?php echo base_url() ?>assets/images/sadaya.png"></div>
            <div class="title">Rampak Kendang</div> <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut.</div> 
          </div> 

          <div class="box content">
            <div class="logo"><img style="width: 150px;" src="<?php echo base_url() ?>assets/images/tari.png"></div>
            <div class="title">Tari</div> <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut.</div> 
          </div>  

          <div class="box content">
            <div class="logo"><img style="width: 150px;" src="<?php echo base_url() ?>assets/images/sadaya.png"></div>
            <div class="title">Perkusi</div><div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut.</div>  
          </div> 

          <div class="box content">
            <div class="logo"><img style="width: 150px;" src="<?php echo base_url() ?>assets/images/gamelan.png"></div>
            <div class="title">Gamelan</div> <div class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut.</div> 
          </div>   

        </div><!-- end box-container --> 
        </div>
      </div><!-- en container -->
    </div><!-- end section divisi -->


    <!-- ==================  start section event ==================== -->
    <div class="section event" id="event">
      <div class="container">
        <div class="section-title">Event terbaru Sadaya</div> 
        <div class="section-sub-text">Persembahan terbaik di setiap penampilannya</div>
        <div class="box-container">

          <div class="box">
            <div class="date">27<small>September 2018</small></div>
            <div class="content">
              <div class="title">Mini Perkusi 2018</div>
              <div class="location"><i class="fas fa-map-marker-alt"></i> Auditorium miracle, UNIKOM, Bandung.</div>
            </div> 
          </div> 
          
          <div class="box">
            <div class="date">08<small>Oktober 2018</small></div>
            <div class="content">
              <div class="title">Resital Tari 2018</div>
              <div class="location"><i class="fas fa-map-marker-alt"></i> Auditorium miracle, UNIKOM, Bandung.</div>
            </div> 
          </div> 
        </div><!-- end box-container --> 
      </div><!-- end container -->
    </div><!-- end section --> 
  
    <!-- ==================  start section contact ==================== -->
    <div class="section contact" id="kontak">
      <div class="container">
        <div class="section-title">Kontak Sadaya</div> 
        <div class="box-container"> 

          <div class="box left"> 
            <div class="contact-list"> 
              <div class="logo"><i class="fa fa-phone"></i></div>
              <div class="title"><span>089-676-232-129 / 081-584-396-6243</span></div>
            </div> 
            <div class="contact-list"> 
              <div class="logo"><i class="fa fa-envelope"></i></div>
              <div class="title"><span>hello@sadaya-unikom.com</span></div>
            </div> 
            <div class="contact-list"> 
              <div class="logo"><i class="fa fa-map-marker-alt"></i></div>
              <div class="title"><span>Universitas Komputer Indonesia Lt 5, Jl Dipatiukur No. 22 <br>Coblong Bandung ID</span></div>
            </div> 
          </div> 
          <!-- @zaky -->
          <!-- tutup dulu aja belum jadi fiturnya -->
          <!-- end box -->
          
          <div class="box right">
            <div class="form-group dark">
            <?=form_open('beranda/pesan');?>
            <div class="input-cont">
              <label>Nama</label>
              <input minlength="3" type="text" name="nama" required="required">
            </div>
            <div class="input-cont">
              <label>Email</label>
              <input minlength="10" type="email" required="required" name="email">
            </div>
            <div class="input-cont">
              <label>Pesan</label>
              <textarea name="pesan" minlength="10" rows="5" required="required"></textarea>
            </div>
            <input class="btn-md btn-primary btn-href" type="submit" name="submit" value="Kirim">
            </div> <!-- end form -->
          </div> <!-- end box -->           
        </div><!-- end box-container --> 
      </div><!-- end container -->
    </div><!-- end section --> 

    <?php $this->load->view('beranda/layout/footer'); ?>