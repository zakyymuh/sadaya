<!DOCTYPE html> 
<html lang="id">
  <head>
      <meta charset="utf-8">
      <title><?php echo (isset($title)) ? $title : 'Untitled' ;?></title>
      <meta name="viewport" content="width=device-width">
      <meta name="google-site-verification" content="1eK7H9KY8Lxkdwu61IvRfr8ZC1b9OdyWVPKlhpXK2rE" />
      <meta name="description" content="BERSAMA SADAYA, KITA BANGKITKAN KESENIAN TRADISIONAL">
      <meta name="keywords" content="SADAYA UNIKOM KESENIAN TRADISIONAL INDONESIA SUNDA JAWA BALI ">
      <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'/>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <link rel="shortcut icon" href="<?=base_url();?>assets/images/logo.ico" type="image/x-icon">

      <link href="<?php  echo base_url() ?>assets/frontend/css/ext/normalize.css" rel="stylesheet" type="text/css" media="all"/>  
      <link href="<?php  echo base_url() ?>assets/frontend/css/ext/bootstrap-grid.min.css" rel="stylesheet" type="text/css" media="all"/> 
      <link href="<?php  echo base_url() ?>assets/frontend/plugins/Font-Awesome/css/fontawesome-all.min.css" rel="stylesheet" type="text/css" media="all"/>
      <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600,800" rel="stylesheet"> 
      <link href="<?php  echo base_url() ?>assets/frontend/css/main-style.css" rel="stylesheet" type="text/css" media="all"/>
 
      <?php if(isset($css)) {foreach($css as $c) { ?>
        <link rel="stylesheet" href="<?php echo base_url($c);?>">
      <?php }} ?>
      <?php if(isset($js)) {foreach($js as $j) { ?>
        <script src="<?php echo base_url($j);?>" type="text/javascript"></script>
      <?php }} ?>
      
      <script>
        var BASE_URL = '<?php echo site_url();?>';
      </script>
  </head>
  <body>
    <?php 
      if($this->session->flashdata('info')){
        ?>
        <script type="text/javascript">
          alert('<?=$this->session->flashdata("info");?>');
        </script>
        <?php
      }
    ?>
    <?php if(!isset($mobile)){ ?>
    <!-- main navigation  -->
    <div class="nav <?php echo (isset($secondary)) ? "fixed" : "" ; ?>">
      <div class="container">
          
          <div class="nav-top-container">
            <div class="nav-top">
              <div class="nav-top-wrapper">
                <a href="<?php echo base_url() ?>" class="nav-logo"> 
                    <div class="logo-img">
                      <img src="<?php echo base_url() ?>assets/images/logo.png"></div>
                    <div class="logo-text">SADAYA</div> 
                </a> 
                <div class="nav-menu">
                <?php if(!isset($secondary)){ ?>
                  <ul>  
                    <li><a href="#pengertian">Tentang</a></li>
                    <li><a href="#divisi">Divisi</a></li>
                    <li><a href="#event">Event</a></li> 
                    <li><a href="#kontak">Kontak</a></li> 
                    <li><a class="btn-href" href="<?=base_url('anggota');?>">Masuk</a></li> 
                    <li><a href="<?=base_url('daftar');?>" class="btn-sm btn-light  btn-href">Daftar<span class="btn-icon"><i class="fa fa-plus"></i></span></a></li>
                  </ul>
                  <?php } else { ?> 
                  <ul>  
                    <li><a class="btn-href" href="<?php echo base_url('') ?>#pengertian">Tentang</a></li>
                    <li><a class="btn-href" href="<?php echo base_url('') ?>#divisi">Divisi</a></li>
                    <li><a class="btn-href" href="<?php echo base_url('') ?>#event">Event</a></li> 
                    <li><a class="btn-href" href="<?php echo base_url('') ?>#kontak">Kontak</a></li> 
                    <li><a class="btn-href" href="<?=base_url('anggota');?>">Masuk</a></li> 
                    <li><a href="<?=base_url('daftar');?>" class="btn-sm btn-light btn-href">Daftar<span class="btn-icon"><i class="fa fa-plus"></i></span></a></li>
                  </ul>
                  <?php } ?>
                </div>
              </div>

            </div><!-- end nav top -->

            <div class="menu-toggle">
              <a class="btn-toggle" href="#"><i class="fa fa-bars"></i></a>
            </div>
          </div><!-- end nav top container-->

      </div>   <!-- end container -->
    </div><!-- end nav -->

    <?php } //end if ?>

