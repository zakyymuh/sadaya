
    <!-- ==================  start foooter ==================== -->
    <div class="footer">
      <div class="container">
        <div class="footer-box-container"> 
          
          <section class="back-top"><i class="fa fa-chevron-up"></i></section>
          <div class="box sosmed"> 
            <div class="box-list"><a href="https://www.facebook.com/saung.unikom" target="_blank"><i class="fab fa-facebook-f"></i></a></div>
            <div class="box-list"><a href="https://twitter.com/Sadaya_"><i class="fab fa-twitter"></i></a></div>
            <div class="box-list"><a href="https://www.instagram.com/sadaya_unikom/"><i class="fab fa-instagram"></i></a></div>
            <div class="box-list"><a href="https://www.youtube.com/channel/UC9YzLtaaS5mZpXqWMOJaGLw"><i class="fab fa-youtube"></i></a></div>
            <!-- <div class="box-list"><a href="#"><i class="fab fa-google-plus"></i></a></div> -->
          </div>
          <div class="box right">
            <div class="logo">
              <div class="logo-img"><img src="<?php echo base_url() ?>assets/images/logo.png"></div>
              <div class="logo-text">SADAYA</div>
            </div> 
            <!-- <div class="box-address">
              Jl Sekeloa no 202 Batujajar <br>
              Coblong, Bandung ID
            </div> -->
            <div class="box-copy">
              <?php 
                if(isset($mobile)){
                  $link_dev = base_url().'mobile/dev';
                }else{
                  $link_dev = base_url().'pengembang';
                }
              ?>
              Made with <i class="fa fa-heart"></i> and <i class="fa fa-coffee"></i> by <a href="<?=$link_dev?>">Sadaya Dev</a>
            </div>
          </div>
        </div><!-- footer box-contaier --> 
      </div><!-- end container -->
    </div><!-- end footer -->
 

    <!-- javascript -->
    <script src="<?php  echo base_url() ?>assets/frontend/js/jquery.min.js"></script>  
    <!-- <script src="<?php  echo base_url() ?>assets/plugins/flexslider/jquery.flexslider-min.js"></script>  -->
    <script src="<?php  echo base_url() ?>assets/frontend/js/main.js"></script>

    <?php if(isset($js)) {foreach($js as $j) { ?>
    <script src="<?php echo base_url($j);?>" type="text/javascript"></script>
    <?php }}
      $file = (isset($file))?$file:"";
      if($file == "tambah_pengembang"){
        ?>
            <script src="<?=base_url();?>assets/js/fileinput.js"></script>
    <script type="text/javascript">
            $("#input-id").fileinput(
              {
                'showUpload':false, 
                'previewFileType':'any',
                'allowedFileExtensions': ['jpg', 'png', 'gif','jpeg'],
                maxFileSize: 2048,
                maxFileNum: 1
              }
              );
    </script>
        <?php
      }
    ?>

  </body>
</html>