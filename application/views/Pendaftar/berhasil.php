<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="google-site-verification" content="M2_bYecNNLBYJlvxcf2j8mIKkShnFgXGJLH6UP8F3cY" />
    <title>SADAYA UNIKOM</title>
    <link rel="shortcut icon" href="<?=base_url();?>assets/images/logo.ico" type="image/x-icon">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?=base_url()?>assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/source/font-awesome.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url()?>assets/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600,800" rel="stylesheet">
    
<style type="text/css">
body {
    font-family: 'Nunito', sans-serif !important;
    font-size: 14px;
    line-height: 1.5;
    color: #555;
}
  .register-page{
    background-color: #d22f30 !important;
  }
  .register-logo{
    background: url('<?=base_url();?>assets/images/cover.jpg') white;
    margin-bottom:0;
    min-height: 150px;
  }
   .register-logo a {
    color:white !important;
   }
        .footer{
          margin:15px;
        }
.btn-primary{
  background-color: #af191a ;
}
      </style>

  </head>
  <body class="hold-transition register-page">
    <div class="register-box">
      <div class="register-logo"><a href="<?=base_url();?>"><img style="margin-top: -50px; height: 30%; width: 30%;" src="<?=base_url();?>assets/images/logo-mini.png"></a><br><br>
        <a href="<?=base_url();?>daftar/">SAUNG <b>BUDAYA</b></a>
      </div>

      <div class="register-box-body">
          <center>
        <?php 
          if(!isset($nama)){
            ?>
              <h3>Silahkan klik tombol di bawah ini untuk daftar</h3>
              <a href="<?=base_url('pendaftar/daftar/');?>"><button class="btn btn-primary">Daftar</button></a>
            <?php
          }else{

         ?>
        <h3 class="login-box-msg">Selamat datang <strong><?=$nama?></strong>.</h3>
        <p>Silahkan masuk ke dalam grup baru untuk pengelompokan dan informasi lebih lanjut</p><br>Scan barcode di bawah<br>
            
         <img src="<?=base_url();?>assets/images/1536854335802.jpg"><br>Atau 
         <span><a href='https://line.me/R/ti/g/MbQcQtD5RS'>klik disini</a></span><br>
          Atau <a href="<?=base_url('pendaftar/daftar');?>">kembali ke pendaftaran </a><br><br>
          </center>
            <small>nb: Hanya yang telah terdaftar di website official SADAYA yang akan dilantik menjadi anggota</small>
            <br><br>
          <?php } ?>
       <style type="text/css">
        .footer{
          margin:15px;
        }
      </style>
      <div class="footer text-center" style="background: white; width: 100%; margin:0; padding:15px 0px;">
  <div class="row">
    <div class="copyright">MADE WITH <i style="color: #bda168" class="fa fa-coffee"></i> &amp; <i style="color: #a24848" class="fa fa-heart"></i> BY <a style="font-weight:bold" href="#">SADAYA DEV TEAM</a></div>
  </div>
</div>


    </div>
    </div><!-- /.register-box -->
    <!-- jQuery 2.1.4 -->
    <script src="<?=base_url();?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?=base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url();?>assets/js/typeahead.bundle.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip(); 
    });

    var substringMatcher = function(strs) {
  return function findMatches(q, cb) {
    var matches, substringRegex;

    // an array that will be populated with substring matches
    matches = [];

    // regex used to determine if a string contains the substring `q`
    substrRegex = new RegExp(q, 'i');

    // iterate through the pool of strings and for any string that
    // contains the substring `q`, add it to the `matches` array
    $.each(strs, function(i, str) {
      if (substrRegex.test(str)) {
        matches.push(str);
      }
    });

    cb(matches);
  };
};

var states = ["Teknik Informatika","Sistem Informasi","Teknik Komputer","Teknik Elektro","Teknik Arsitektur","Teknik Sipil","Teknik Industri","Perencanaan Wilayah & Kota","Akuntansi Komputerisasi","Akuntansi","Manajemen","Manajemen Pemasaran","Keuangan & Perbankan","Ilmu Komunikasi","Ilmu Pemerintahan","Hubungan Internasional","Desain Komunikasi Visual","Desain Interior","Sastra Inggris","Sastra Jepang","Ilmu Hukum","Manajemen","Sistem Operasi","Desain"];

$('#the-basics .typeahead').typeahead({
  hint: true,
  highlight: true,
  minLength: 1
},
{
  name: 'states',
  source: substringMatcher(states)
});
    </script>
   </body>
</html>
