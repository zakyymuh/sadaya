<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="google-site-verification" content="M2_bYecNNLBYJlvxcf2j8mIKkShnFgXGJLH6UP8F3cY" />
    <title>SADAYA UNIKOM</title>
    <link rel="shortcut icon" href="<?=base_url();?>assets/images/logo.ico" type="image/x-icon">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
    
    <link rel="stylesheet" href="<?=base_url()?>assets/source/font-awesome.min.css">


    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600,800" rel="stylesheet">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url()?>assets/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
    


<style type="text/css">
.register-box{
  margin-top: 15vh;
}
body {
    font-family: 'Nunito', sans-serif !important;
    font-size: 14px;
    line-height: 1.5;
    color: #555;
}

  .register-page{
    background-color: #d22f30 !important;
  }
  .register-logo{
    background: url('<?=base_url();?>assets/images/cover.jpg') white;
    margin-bottom:0;
    min-height: 150px;
  }
   .register-logo a {
    color:white !important;
   }

   //autocomplete
.autocomplete {
  /*the container must be positioned relative:*/
  position: relative;
  display: inline-block;
  width: 100%;
}
  .tt-menu{
    padding: 5px;
    background-color:white;
 }
 .tt-suggestion:hover{
    background-color: #e9e9e9;
    cursor: pointer;
  }
  .tt-suggestion{
    padding: 5px;
    width: 100%;
  }
  .twitter-typeahead{
    width: 100%;
  }
  .opt{
    font-weight: 400 !important;
  }
</style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition register-page">
    <div class="register-box">
      <div class="register-logo">
          <a href="<?=base_url();?>"><img style="margin-top: -50px; height: 30%; width: 30%;" src="<?=base_url();?>assets/images/logo-mini.png"></a><br>
        <br><a href="<?=base_url();?>beranda/">SADAYA <b>
          <?php 
            $thn = date('Y') - 2008 + 2; echo $thn;
           ?>

        </b></a>
      </div>
      
      <div class="register-box-body">
        <p class="login-box-msg">Daftar keanggotaan baru</p>
        <?=form_open_multipart("pendaftar/daftar")?>
          <div class="form-group">
                      <label id="labelNIM">NIM
                      
                      </label>
                      <input class="form-control NIM" onchange="cekNIM()" required="required" name="NIM" placeholder="" minlength="8" maxlength="8" type="text">
          </div>
          <div class="form-group">
            <label>Nama lengkap
            </label>
            <input class="form-control" required="required" placeholder="" type="text" name="nama_lengkap">
          </div>
          <div class="form-group">
            <label  class="opt">Nama panggilan (opsional)
            </label>
            <input class="form-control" placeholder="" name="nama_panggilan" type="text">
          </div>
          <div class="form-group">
            <label>Jenis Kelamin</label>
            <div class="radio">
                        <label>
                          <input name="jenis_kelamin" id="optionsRadios1" value="L" checked="checked" type="radio">
                          Laki-laki
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          <input name="jenis_kelamin" id="optionsRadios1" value="P" type="radio">
                          Perempuan
                        </label>
                      </div>
          </div>
          <div class="form-group">
            <label>Email</label>
            <input class="form-control" name="email" placeholder="" type="email" required="required">
          </div>
          <div class="form-group">
            <label>Agama</label>
            <select class="form-control" name="agama">
                        <option value="islam" selected="selected"> -- Pilih Agama -- </option>
                        <option value="islam">Islam</option>
                        <option value="kristen katolik">Kristen Katolik</option>
                        <option value="kristen protestan">Kristen Protestan</option>
                        <option value="hindu">Hindu</option>
                        <option value="budha">Budha</option>
                      </select>
          </div>
          <div class="form-group">
            <label>Tempat lahir</label>
            <input class="form-control" minlength="4" placeholder="" type="text" required="required" name="tempat_lahir">
          </div>
          <div class="form-group" id="dpct">
            <label >Tanggal Lahir</label>
            <input class="form-control" placeholder="dd/mm/yyyy" id="date" required="required" type="text" name="tanggal_lahir">
          </div>

          <div class="form-group">
            <label>No handphone</label>
            <input class="form-control" minlength="10" maxlength="15" placeholder="" required="required" type="text" name="kontak">
          </div>
          <div class="form-group">
            <label>No handphone orangtua/telp rumah
            </label>
            <input class="form-control" minlength="10" maxlength="15" placeholder="" type="text" required="required" name="kontak_lain">
          </div>
          <div class="form-group">
            <label>Alamat rumah</label>
            <textarea class="form-control" minlength="4" name="alamat_rumah" required="required" rows="2" placeholder=""></textarea>
          </div>
          <div class="form-group">
            <label class="opt">Alamat tinggal di Bandung (opsional)</label>
            <textarea class="form-control" name="alamat_lain" rows="2" placeholder=""></textarea>
          </div>
          <div class="form-group">
            <label>ID Line <sup>*</sup></label>
            <input class="form-control" minlength="4" placeholder="" required="required" type="text" name="sosmed">
          </div>
          <div class="form-group">
            <label class="opt">Sosial media lain (opsional)</label>
            <input class="form-control" name="sosmed_lain" placeholder="" type="text">
          </div>
          <div class="form-group">
            <label class="opt">Bakat (opsional)</label>
            <textarea class="form-control" rows="2" name="bakat" placeholder=""></textarea>
          </div>


          <div class="form-group">
            <label>Minat
             <i class="fa fa-info-circle" 
                          data-toggle="tooltip" data-placement="right" 
                          data-title="Apasih yang jadi minat kamu, kami kepo hehe"
                          data-widget="chat-pane-toggle">
                </i>
            </label>
            <textarea class="form-control" rows="2" name="minat" placeholder="" required="required"></textarea>
          </div>
          <div class="form-group">
            <label>Mengapa kamu ingin masuk ke UKM SADAYA?
              <i class="fa fa-info-circle" 
                          data-toggle="tooltip" data-placement="right" 
                          data-title="Seberapa tangguh kah kamu? -spongebob universe"
                          data-widget="chat-pane-toggle">
                </i></label>
            <textarea class="form-control" rows="3" name="alasan" placeholder=""></textarea>
          </div>
          <div class="form-group">
            <label>Foto unik kamu
              </label>
            <input id="input-id" type="file" data-show-upload="false" name="userfile" class="file" data-preview-file-type="text" required="required">
          </div>
          <div class="row">
            
            <style type="text/css">
            .btn-primary{
              background-color: #af191a;
              color:white;
              border: white;
            }
            .btn-primary:hover{
              background-color:#d22f30;
            }
            .btn-primary:focus{
              background-color:#d22f30;
            }
            .btn-primary:active:focus{
              background-color:#d22f30;

            }
            .btn-sm{
              font-size: 16px;
              padding: 5px 25px;
              font-weight: 800;
            }
            </style>
            <br>
            <div class="col-xs-12 text-center">
              <button type="submit" name="submit" value="Daftar" class="btn btn-sm btn-primary btn-block">
                <b>Daftar</b><span class="fa fa-plus-round"></span>
              </button>
            </div><!-- /.col -->
           
          </div>
        </form>

       
      </div><!-- /.form-box -->
      <style type="text/css">
        .footer{
          margin:15px;
        }
      </style>
      <div class="footer text-center" style="background: white; width: 100%; margin:0; padding:15px 0px;">
  <div class="row">
    <div class="copyright">MADE WITH <i style="color: #bda168" class="fa fa-coffee"></i> &amp; <i style="color: #a24848" class="fa fa-heart"></i> BY <a style="font-weight:bold" href="<?=base_url('pengembang')?>">SADAYA DEV TEAM</a></div>
     
  </div>
</div>


    </div><!-- /.register-box -->
    <!-- jQuery 2.1.4 -->
    <script src="<?=base_url();?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    
    <!-- Bootstrap 3.3.5 -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>

    <script>
      $(document).ready(function(){
        var date_input=$('input[name="tanggal_lahir"]'); //our date input has the name "date"
        // var container= '#tanggal_lahir';
        date_input.datepicker({
          format: 'dd/mm/yyyy',
          orientation: 'top left',
          container: $('.register-page').parent(),
          todayHighlight: true,
          autoclose: true
        })
      })
    </script>

    <script src="<?=base_url();?>assets/js/fileinput.js"></script>
    <script type="text/javascript">
            $("#input-id").fileinput(
              {
                'showUpload':false, 
                'previewFileType':'any',
                'allowedFileExtensions': ['jpg', 'png', 'gif','jpeg'],
                maxFileSize: 2048,
                maxFileNum: 1
              }
              );

    function cekNIM(){
        NIM = $(".NIM").val();
        label = $("#labelNIM").html();
        labelCheck = label+" (Mengecek)";
        $("#labelNIM").html("NIM (Mengecek)"); 
        $.ajax({
            type:'POST',
            data: 'NIM='+NIM,
            url :'<?=base_url();?>pendaftar/cekNIM',
            success: function(result){
                var arr = JSON.parse(result);
                console.log(arr);
                if(arr['msg'] == true){
                  $("#labelNIM").html("NIM <i class='text-success fa fa-check'></i>");
                }else{
                  $("#labelNIM").html("NIM <i class='text-danger fa fa-remove'></i><span class='opt'> NIM telah digunakan </span>");
                }
              }
            });
            };
        </script>
        
   </body>
</html>
