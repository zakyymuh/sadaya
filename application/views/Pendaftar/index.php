	<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Beranda
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?=base_url('beranda');?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <?php
              foreach ($data as $res) {
                # code...
            ?>
            <div class="col-md-4">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title"><?=$res->judul;?></h3><br><strong><?=$res->username;?></strong><br>
                  <span class="text-muted"><?=transform_datetime($res->tanggal_upload);?>
                </div><!-- /.box-header -->
              </div>
            </div>
            <?php
              }
            ?>
          </div>
         

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

