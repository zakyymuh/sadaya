      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?=base_url()?>assets/dist/images/user8-128x128.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?=$this->session->username;?></p><i class="fa fa-circle text-success"></i> Online
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li>
              <a href="<?=base_url('Admin/beranda')?>">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
              </a>
            </li>
            <li><li>
              <a href="<?=base_url('Admin/Transaksi')?>">
                <i class="fa fa-dollar"></i> <span>Transaksi</span></i>
              </a>
            </li>
            <li>
              <a href="<?=base_url('Admin/berita')?>">
                <i class="fa fa-newspaper-o"></i> <span>Berita</span></i>
              </a>
            </li>
            <?php if($this->session->id_user == 1){
              ?>
            <li>
              <a href="<?=base_url('Admin/user')?>">
                <i class="fa fa-user"></i> <span>User</span></i>
              </a>
            </li>
              <?php
            }?>
            <li>
              <a href="<?=base_url('Admin/profile')?>">
                <i class="fa fa-picture-o"></i> <span>Profile</span></i>
              </a>
            </li>
            <li>
              <a href="<?=base_url('Admin/Password')?>">
                <i class="fa fa-lock"></i> <span>Password</span></i>
              </a>
            </li>
            <li>
              <a href="<?=base_url('Auth/logout')?>">
                <i class="fa fa-sign-out"></i> <span>Log Out</span>
              </a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>