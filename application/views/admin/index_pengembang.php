	<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Halaman pengembang
            <small>kelola daftar pengembang</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
            <li class="active">Pengembang</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          
          <div class="row">
            <!-- Left col -->
            <section class="col-lg-12">
              <!-- Table box -->
        <?php
        if($this->session->flashdata("sukses")){
          ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>  <i class="icon fa fa-check"></i> Berhasil!</h4>
                    <?=$this->session->flashdata("sukses")?>
                  </div>
          
          <?php
        }
        ?>
             <div class="box box-primary">
             	<div class="box-header">
             		<i class="fa fa-table"></i>
                  <h3 class="box-title">Tabel pengembang</h3>
             	</div>
             	<div class="box-body">
             	<a href="<?=base_url()?>admin/pengembang/tambah">
          <button class="btn btn-primary btn-flat"><i class='fa fa-plus'></i> Tambah</button>&nbsp;</a>

          
             		<table id="tb" border="1" class="table table-bordered">
						<thead>
	                      <tr>
                         <th>No</th>
                         <th>Nama</th>
                         <th>Email</th>
                         <th>Sosial Media</th>
                         <th>Foto</th>
                         <th>Bio</th>
                         <th>Aksi</th>
                        </tr>
	                    </thead>
	                    <tbody>
                        
	                    </tbody>
					</table>
					
             	</div>
             </div>
            </section><!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

