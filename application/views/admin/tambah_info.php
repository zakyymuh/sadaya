<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Tambah info
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?=base_url('admin/beranda/');?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
            <li class="<?=base_url('admin/info');?>">Info</li>
            <li class="active">Tambah</li>
          </ol>

        </section>

        <!-- Main content -->
        <section class="content">
          
          <div class="row">
            <!-- Left col -->
            <section class="col-lg-4">
              <?php
        if($this->session->flashdata("info")){
          ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>  <i class="icon fa fa-check"></i> Informasi!</h4>
                    <?=$this->session->flashdata('info');?>
                  </div>
          
          <?php
        }
        ?>
              <!-- Table box -->
             <div class="box box-primary">

                <div class="box-header with-border">
             		<i class="fa fa-plus"></i>
                  <h3 class="box-title">Form Tambah Info</h3>
                </div><!-- /.box-header -->
                <div class="box-body pad">
				<span class="text-danger"><?=validation_errors();?></span>
				<?=form_open_multipart("admin/info/tambah");?>
                    <!-- text input -->
                    <div class="form-group">
                      <label>Judul</label>
                      <input class="form-control" minlength="3" name="judul" type="text" required="required">
                    </div>
                    <div class="form-group">
                      <label>TAG</label>
                      <input class="form-control" minlength="3" name="tag" type="text" required="required
                      ">
                    </div>
                     <div class="form-group">
                      <label>Foto</label>
                      <input id="input-id" type="file" data-show-upload="false" name="userfile" class="file" data-preview-file-type="text" required="required">
                  </div>
                    
                   
                </div><!-- /.box-body -->
             	<div class="box-footer">
             		<input type="submit" class="btn btn-info btn-flat" name="submit" value="Tambah">
             	</div></form>
                </div><!-- /.box-body -->
              
           
            </section>
          </div>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

