<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Tambah pengembang
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?=base_url('admin/beranda/');?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
            <li class="<?=base_url('admin/pengembang/');?>">Pengembang</li>
            <li class="active">Tambah</li>
          </ol>

        </section>

        <!-- Main content -->
        <section class="content">
          
          <div class="row">
            <!-- Left col -->
            <section class="col-lg-6">
              <?php
        if($this->session->flashdata("sukses")){
          ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>  <i class="icon fa fa-check"></i> Berhasil!</h4>
                    <?=$this->session->flashdata("sukses");?>        
                  </div>
          
          <?php
        }elseif($this->session->flashdata("gagal")){
          ?>
           <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>  <i class="icon fa fa-remove"></i> Gagal!</h4>
                    <?=$this->session->flashdata("gagal");?>        
                  </div>

          <?php
        }
        ?>
            <style type="">
              .col-md-6{
                margin-bottom: 10px;
              }
            </style>
              <!-- Table box -->
             <div class="box box-primary">

                <div class="box-header with-border">
             		<i class="fa fa-plus"></i>
                  <h3 class="box-title">Form Tambah Pengembang</h3>
                </div><!-- /.box-header -->
                <div class="box-body pad">
				<span class="text-danger"><?=validation_errors();?></span>
				<?=form_open_multipart("admin/pengembang/tambah");?>
                    <!-- text input -->
                    <div class="form-group">
                      <label>Nama</label>
                      <input class="form-control" minlength="3" name="nama" type="text" required="required">
                    </div>
                    <div class="form-group">
                      <label>Email</label>
                      <input class="form-control" minlength="6" name="email" type="text" required="required">
                    </div>
                      <label>Sosial Media</label>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-facebook"></i></span>
                          <input class="form-control" placeholder="sadaya_unikom" type="text" name="fb">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-linkedin"></i></span>
                    <input class="form-control" placeholder="sadaya_unikom" type="text" name="li">
                  </div>
                        </div><br>
                        <div class="col-md-6">
                           <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-twitter"></i></span>
                            <input class="form-control" placeholder="sadaya_unikom" type="text" name="tw">
                        </div>
                        </div>
                        <div class="col-md-6">
                          <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-instagram"></i></span>
                          <input class="form-control" placeholder="sadaya_unikom" type="text" name="ig">
                        </div>
                        </div>
                      </div>
                   
                  <div class="form-group">
                      <label>Bio</label>
                      <textarea class="form-control" name="bio" rows="2" required="required"></textarea>
                    </div>
                  <div class="form-group">
                      <label>Jobdesk</label>
                      <textarea class="form-control" name="jobdesk" rows="2" required="required"></textarea>
                    </div>
                  <div class="form-group">
                      <label>Foto</label>
                      <input id="input-id" type="file" data-show-upload="false" name="userfile" class="file" data-preview-file-type="text" required="required">
                  </div>
                </div><!-- /.box-body -->
             	
              <div class="box-footer">
             		<input type="submit" class="btn btn-info btn-flat" name="submit" value="Tambah">
             	</div></form>
                </div><!-- /.box-body -->
              
           
            </section>
          </div>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

