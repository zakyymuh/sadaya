<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Tambah admin PAB
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?=base_url('admin/beranda/');?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
            <li class="">PAB</li>
            <li class="active">Tambah</li>
          </ol>

        </section>

        <!-- Main content -->
        <section class="content">
          
          <div class="row">
            <!-- Left col -->
            <section class="col-lg-4">
              <?php
        if($this->session->flashdata("info")){
          ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>  <i class="icon fa fa-check"></i> Berhasil!</h4>
                    Admin telah ditambah .
                  </div>
          
          <?php
        }
        ?>
              <!-- Table box -->
             <div class="box box-primary">

                <div class="box-header with-border">
             		<i class="fa fa-plus"></i>
                  <h3 class="box-title">Form Tambah Admin</h3>
                </div><!-- /.box-header -->
                <div class="box-body pad">
				<span class="text-danger"><?=validation_errors();?></span>
				<?=form_open("admin/pab/tambah");?>
                    <!-- text input -->
                    <div class="form-group">
                      <label>Nama</label>
                      <input class="form-control" minlength="3" name="nama" type="text" required="required">
                    </div>
                    <div class="form-group">
                      <label>Username</label>
                      <input class="form-control" minlength="6" name="username" type="text" required="reqw
                      ">
                    </div>
                    <div class="form-group">
                      <label>Password</label>
                      <input class="form-control" minlength="6" required="required" name="password" type="Password">
                    </div>
                   
                </div><!-- /.box-body -->
             	<div class="box-footer">
             		<input type="submit" class="btn btn-info btn-flat" name="submit" value="Tambah">
             	</div></form>
                </div><!-- /.box-body -->
              
           
            </section>
          </div>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

