      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?=base_url()?>assets/images/logo.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?=$this->session->nama;?></p><i class="fa fa-circle text-success"></i> Online
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class=" <?php 
                  echo ($menu == "beranda")?"active":"";
                  ?>">
              <a href="<?=base_url('admin/beranda')?>">
                <i class="fa fa-dashboard 

                "></i> <span>Beranda</span></i>
              </a>
            </li>
            <li class="<?=($menu == 'info')?'active':'';?>">
              <a href="<?=base_url('admin/info')?>">
                <i class="fa fa-info"></i> <span>Info</span></i>
              </a>
            </li><li class="<?=($menu == 'pab')?'active':'';?>">
              <a href="<?=base_url('admin/beranda/pab')?>">
                <i class="fa fa-users"></i> <span>PAB</span></i>
              </a>
            </li>
            <li>
              <a href="<?=base_url('admin/beranda/dev')?>">
                <i class="fa fa-users"></i> <span>Pengurus</span></i>
              </a>
            </li>
            <li class='<?=($menu == "pengembang")?"active":"";?>'>
              <a href="<?=base_url('admin/pengembang/')?>">
                <i class="fa fa-users"></i> <span>Pengembang</span></i>
              </a>
            </li>
            <?php if($this->session->id_admin == 'AD1'){
              ?>
            <li class="<?=($menu == 'admin')?'active':'';?>">
              <a href="<?=base_url('admin/beranda/dev')?>">
                <i class="fa fa-users"></i> <span>Admin</span></i>
              </a>
            </li>
              <?php
            }?>
            <li class="<?=($menu == 'profile')?'active':'';?>">
              <a href="<?=base_url('admin/beranda/dev')?>">
                <i class="fa fa-user"></i> <span>Profile</span>
              </a>
            </li>
            <li>
              <a href="<?=base_url('admin/auth/logout')?>">
                <i class="fa fa-sign-out"></i> <span>Log Out</span>
              </a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>