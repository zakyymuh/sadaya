<!DOCTYPE html>
<html lang="id">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?=base_url()?>assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/source/font-awesome.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/dist/css/AdminLTE.min.css">
    <link rel="shortcut icon" href="<?=base_url();?>assets/images/logo.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?=base_url()?>assets/dist/css/skins/skin-red.min.css">

    <link rel="stylesheet" href="<?=base_url()?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/dataTables.bootstrap.css"> 
    <?php   
      $css = (isset($css))?$css:"";
      switch ($css) {
        case 'file_input':
          ?>
          <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />    
          <?php
          break;
      }
    ?>

<head>
	<title>Beranda</title>
</head>
<body class="hold-transition skin-red sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="<?=base_url();?>admin/beranda/" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>SDY</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>SADAYA</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
        </nav>
      </header>