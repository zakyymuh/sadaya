      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.3.0
        </div>
        <strong>Copyright &copy; 2014-2015 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights reserved.
      </footer>
</body>
<script src="<?=base_url()?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="<?=base_url()?>assets/source/jquery-ui.min.js"></script>
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <script src="<?=base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/plugins/fastclick/fastclick.min.js"></script>
    <script src="<?=base_url()?>assets/dist/js/app.min.js"></script>
    <script src="<?=base_url()?>assets/dist/js/pages/dashboard.js"></script>
    <script src="<?=base_url()?>assets/dist/js/demo.js"></script>
    <script src="<?=base_url()?>assets/js/jquery.dataTables.min.js"></script>
    <script src="<?=base_url()?>assets/js/dataTables.bootstrap.min.js"></script>
     
    <?php 
      $file = (isset($file))?$file:"";
      switch ($file) {
        case 'pengembang':
          $url = base_url()."admin/pengembang/get_data_pengembang";
          break;
        case 'file_input':
          ?>
            <script src="<?=base_url();?>assets/js/fileinput.js"></script>
            <script type="text/javascript">
            $("#input-id").fileinput(
              {
                'showUpload':false, 
                'previewFileType':'any',
                'allowedFileExtensions': ['jpg', 'png', 'gif','jpeg'],
                maxFileSize: 2048,
                maxFileNum: 1
              }
              );
            </script>
          <?php
          break;
        case 'info':
          $url = base_url()."admin/info/get_data_info";
          break;
        }
    ?>
    <script type="text/javascript">
                $('#tb').DataTable({
                      "paging": true,
                      "lengthChange": false,
                       "searching": true,
                      "ordering": true,
                      "info": true,
                      "autoWidth": false,
                      "ajax":{
                        url: '<?=$url;?>',
                        type: "GET"
                      }
                    });
              
    </script>
      
      
    <script src="<?=base_url()?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <script type="text/javascript">
        // $(".textarea").wysihtml5();
      </script>

</html>