	<style type="text/css">
   .gambar_pab{
    max-width: 300px;
    max-height: 300px;
   } 
  </style>
  <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Data pendaftar
            <small>Kelola data pendaftar</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Pendaftar</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          
          <div class="row">
            <!-- Left col -->
            <section class="col-lg-12">
              <!-- Table box -->
        <?php
        if($this->session->flashdata("info")){
          ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>  <i class="icon fa fa-check"></i> Berhasil!</h4>
                    <?=$this->session->flashdata("info")?>
                  </div>
          
          <?php
        }
        ?>
             <div class="box box-primary">
             	<div class="box-header">
             		<i class="fa fa-table"></i>
                  <h3 class="box-title">Table Pendaftar</h3>
             	</div>
             	<div class="box-body">
          
             		<table id="tb" border="1" class="table table-bordered">
						<thead>
	                      <tr>
	                        <th style="">NO</th>
                          <th style="">NIM</th>
	                        <th style="">NAMA</th>
	                        <th style="">JURUSAN</th>
	                        <th style="">LINE</th>
	                        <th class="">FOTO</th>
	                      </tr>
	                    </thead>
	                    <tbody>
	                    </tbody>
					</table>
					
             	</div>
             </div>
            </section><!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

